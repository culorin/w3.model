//
// w3 data model
//
#[macro_use]
extern crate log;
extern crate indexmap;
extern crate regex;
extern crate uuid;

#[macro_use]
extern crate w3model_derive;
// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub type Result<T> = result::Result<T, std::string::String>;
// ----------------------------------------------------------------------------
pub trait ValidatableElement {
    fn validate(&self, id: &str) -> Result<()>;
}
// ----------------------------------------------------------------------------
#[macro_use]
pub mod validator;

pub mod community;
pub mod entity;
pub mod item;
pub mod journal;
pub mod layer;
pub mod navdata;
pub mod questgraph;
pub mod resources;
pub mod shadowlayer;

pub mod settings;

pub mod primitives;
pub mod shared;

pub mod debug;

pub mod rtti;

pub use quest::QuestDefinition;
// ----------------------------------------------------------------------------
use std::result;
// ----------------------------------------------------------------------------
mod quest;
// ----------------------------------------------------------------------------

//
// settings::quest
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Debug, Model)]
#[model(set, getr)]
pub struct Settings {
    dlcid: String,
    caption: String,
    description: Option<String>,
    menuvisibile: bool,
    enabled: bool,
    storesdata: bool,
    mounters: Option<String>,
    idspace: u32,
    idstart: u32,
    dirs: Directories,
    version: u32,
}
// ----------------------------------------------------------------------------
#[derive(Debug, Model)]
pub struct Directories {
    #[model(set, getr)]
    pref_base: Option<String>,
    #[model(set, getr)]
    pref_layer: Option<String>,
    dlcid: String,
    base: String,
    #[model(set, getr)]
    quest: String,
    #[model(set, getr)]
    phase: String,
    #[model(set, getr)]
    scenes: String,
    #[model(set, getr)]
    spawnsets: String,
    #[model(set, getr)]
    entities: String,
    journals: String,
    items: String,
    loot: String,
    rewards: String,
    layer: String,
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use super::IDSPACE_BASE as STRING_IDSPACE_BASE;
use {Result, ValidatableElement};
// ----------------------------------------------------------------------------
impl Settings {
    // ------------------------------------------------------------------------
    pub fn new(id: &str, caption: &str) -> Settings {
        Settings {
            dlcid: id.to_owned(),
            caption: caption.to_owned(),
            description: None,
            menuvisibile: true,
            enabled: true,
            storesdata: true,
            mounters: None,
            idspace: 9999,
            idstart: 0,
            dirs: Directories::default(),
            version: 0,
        }
    }
    // ------------------------------------------------------------------------
    pub fn init(&mut self) -> Result<()> {
        // set questid
        self.dirs.init_for_dlcid(&self.dlcid);
        Ok(())
    }
    // ------------------------------------------------------------------------
    pub fn get_strid_start(&self) -> u32 {
        // must be exactly defined as IDSPACE in w3strings encoder
        STRING_IDSPACE_BASE + self.idspace * 1000 + self.idstart
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Default for Settings {
    // ------------------------------------------------------------------------
    fn default() -> Settings {
        Settings {
            dlcid: String::default(),
            caption: String::default(),
            description: None,
            menuvisibile: true,
            enabled: true,
            storesdata: true,
            mounters: None,
            idspace: 9999,
            idstart: 0,
            dirs: Directories::default(),
            version: 0,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Default for Directories {
    // ------------------------------------------------------------------------
    fn default() -> Directories {
        Directories {
            pref_base: None,
            pref_layer: None,

            dlcid: String::from("undefined"),
            base: String::default(),
            quest: String::default(),
            phase: String::default(),
            scenes: "scenes".to_string(),
            spawnsets: "spawnsets".to_string(),
            journals: "journals".to_string(),
            entities: "entities".to_string(),
            items: "gameplay/items".to_string(),
            loot: "gameplay/items".to_string(),
            rewards: "gameplay/rewards".to_string(),
            layer: String::default(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Directories {
    // ------------------------------------------------------------------------
    fn normalize_path(path: &str) -> String {
        path.replace('\\', "/")
            .replace("//", "/")
            .trim_start_matches('/')
            .trim_end_matches('/')
            .to_lowercase()
    }
    // ------------------------------------------------------------------------
    fn init_for_dlcid(&mut self, id: &str) {
        self.base = match self.pref_base {
            Some(ref base) => Self::normalize_path(base),
            None => Self::normalize_path(&format!("dlc/dlc{}", id)),
        };
        self.quest = Self::normalize_path(&format!(
            "{}/data/{}",
            self.base,
            Self::normalize_path(&self.quest)
        ));
        self.phase = Self::normalize_path(&format!(
            "{}/{}",
            self.quest,
            Self::normalize_path(&self.phase)
        ));
        self.scenes = Self::normalize_path(&format!(
            "{}/data/{}",
            self.base,
            Self::normalize_path(&self.scenes)
        ));
        self.spawnsets = Self::normalize_path(&format!(
            "{}/data/{}",
            self.base,
            Self::normalize_path(&self.spawnsets)
        ));
        self.journals = format!("{}/journal", &self.base);
        self.entities = Self::normalize_path(&format!(
            "{}/data/{}",
            self.base,
            Self::normalize_path(&self.entities)
        ));
        self.items = Self::normalize_path(&format!(
            "{}/data/{}",
            self.base,
            Self::normalize_path(&self.items)
        ));
        self.loot = Self::normalize_path(&format!(
            "{}/data/{}",
            self.base,
            Self::normalize_path(&self.loot)
        ));
        self.rewards = Self::normalize_path(&format!(
            "{}/data/{}",
            self.base,
            Self::normalize_path(&self.rewards)
        ));

        self.layer = match self.pref_layer {
            Some(ref layer) => layer.to_string(),
            None => id.to_string(),
        };
        self.dlcid = id.to_string();
    }
    // ------------------------------------------------------------------------
    pub fn reddlc_path(&self) -> String {
        Self::normalize_path(&format!("{}/dlc{}.reddlc", self.base, self.dlcid))
    }
    // ------------------------------------------------------------------------
    pub fn journals_path(&self) -> String {
        self.journals.replace('/', "\\")
    }
    // ------------------------------------------------------------------------
    pub fn entities_path(&self) -> String {
        self.entities.replace('/', "\\")
    }
    // ------------------------------------------------------------------------
    pub fn items_path(&self) -> String {
        self.items.replace('/', "\\")
    }
    // ------------------------------------------------------------------------
    pub fn navdata_path(&self, worldpath: &str) -> String {
        // skip first two dirs for a dlc world and remove w2w part
        let skip_dirs = if worldpath.starts_with("dlc") { 2 } else { 0 };
        let parts: Vec<_> = worldpath
            .split('\\')
            .skip(skip_dirs)
            .take_while(|p| !p.ends_with("w2w"))
            .collect();
        Self::normalize_path(&format!("{}/{}/navi", self.base, parts.join("/")))
    }
    // ------------------------------------------------------------------------
    pub fn shadowmesh_path(&self) -> String {
        Self::normalize_path(&format!("{}/merged_content", self.base))
    }
    // ------------------------------------------------------------------------
    pub fn questlevels_filepath(&self) -> String {
        format!("{}/data/quest_levels.csv", self.base).replace('/', "\\")
    }
    // ------------------------------------------------------------------------
    pub fn quest_filepath(&self) -> String {
        Self::normalize_path(&format!("{}/{}.w2quest", self.quest, self.dlcid))
    }
    // ------------------------------------------------------------------------
    pub fn segment_filepath(&self, context: &str, segmentid: &str) -> String {
        Self::normalize_path(&format!("{}/{}/{}.w2phase", self.phase, context, segmentid))
    }
    // ------------------------------------------------------------------------
    pub fn character_journal_filepath(&self, journalid: &str) -> String {
        Self::normalize_path(&format!(
            "{}/characters/{}.journal",
            self.journals, journalid
        ))
    }
    // ------------------------------------------------------------------------
    pub fn creature_journal_filepath(&self, journalid: &str) -> String {
        Self::normalize_path(&format!("{}/bestiary/{}.journal", self.journals, journalid))
    }
    // ------------------------------------------------------------------------
    pub fn quest_journal_filepath(&self, journalid: &str) -> String {
        Self::normalize_path(&format!("{}/quests/{}.journal", self.journals, journalid))
    }
    // ------------------------------------------------------------------------
    pub fn community_filepath(&self, context: &str, communityid: &str) -> String {
        Self::normalize_path(&format!(
            "{}/{}/{}.w2comm",
            self.spawnsets, context, communityid
        ))
    }
    // ------------------------------------------------------------------------
    pub fn entity_filepath(&self, context: &str, entityid: &str) -> String {
        Self::normalize_path(&format!("{}/{}/{}.w2ent", self.entities, context, entityid))
    }
    // ------------------------------------------------------------------------
    pub fn resource_filepath(&self, context: &str, targetname: &str) -> String {
        if context.starts_with('/') {
            Self::normalize_path(&format!(
                "{}/{}/{}",
                self.base, context, targetname
            ))
        } else {
            Self::normalize_path(&format!(
                "{}/data/{}/{}",
                self.base, context, targetname
            ))
        }
    }
    // ------------------------------------------------------------------------
    pub fn layer_filepath_parts(
        &self,
        context: Option<&String>,
        layername: &str,
    ) -> (String, String) {
        // cannot be assembled at this point as the (sub)path for the world levels
        // is not available at this point (defined in repository)
        // -> split into prefix/<world levels>/suffix
        // Note: since only directories with layer files can be shown/hidden in quests the
        // layername is used as last subdirectory, too
        (
            self.base.clone(),
            Self::normalize_path(&format!(
                "{}/{}/{}/",
                self.layer,
                context.map(String::as_str).unwrap_or(""),
                layername
            )),
        )
    }
    // ------------------------------------------------------------------------
    pub fn layer_reference(&self, context: Option<&String>, layername: &str) -> String {
        Self::normalize_path(&format!(
            "{}/{}/{}/{}",
            self.base,
            self.layer,
            context.map(String::as_str).unwrap_or(""),
            layername
        ))
    }
    // ------------------------------------------------------------------------
    pub fn scene_reference_filepath(&self) -> String {
        self.scenes.clone()
    }
    // ------------------------------------------------------------------------
    pub fn items_filepath(&self, itemtype: &str) -> String {
        Self::normalize_path(&format!("{}/def_item_{}.xml", self.items, itemtype))
    }
    // ------------------------------------------------------------------------
    pub fn loot_filepath(&self, loottype: &str) -> String {
        Self::normalize_path(&format!("{}/def_loot_{}.xml", self.loot, loottype))
    }
    // ------------------------------------------------------------------------
    pub fn rewards_filepath(&self, rewardtype: &str) -> String {
        Self::normalize_path(&format!("{}/def_reward_{}.xml", self.rewards, rewardtype))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Validation
// ----------------------------------------------------------------------------
impl ValidatableElement for Settings {
    // ------------------------------------------------------------------------
    fn validate(&self, _: &str) -> Result<()> {
        if self.idspace > 9999 {
            return Err(format!(
                "strings-idspace must be within [0;9999]. found: {}",
                self.idspace
            ));
        }
        if self.idstart > 999 {
            warn!(
                "strings-idstart {} is bleeding into next idspace!",
                self.idstart
            );
        }

        if self.dlcid.len() < 3 {
            return Err("dlcid must be a string with at least 3 characters".into());
        }

        match self.caption.len() {
            0..=2 => Err("caption must be a string with at least 3 characters".into()),
            _ => Ok(()),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for Directories {
    // ------------------------------------------------------------------------
    fn validate(&self, _: &str) -> Result<()> {
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------

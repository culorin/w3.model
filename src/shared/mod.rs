//
// w3 data model structures shared in multiple contexts
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[allow(clippy::upper_case_acronyms)]
#[derive(Clone, Debug, PartialEq)]
pub enum CompareFunction {
    EQ,
    NEQ,
    LT,
    LTE,
    GT,
    GTE,
}
// ----------------------------------------------------------------------------
#[allow(clippy::upper_case_acronyms)]
#[derive(Clone, Debug, PartialEq)]
pub enum LogicOperation {
    AND,
    OR,
    XOR,
    NAND,
    NOR,
    NXOR,
}
// ----------------------------------------------------------------------------
#[derive(Clone, Model)]
pub struct FactsDbCondition {
    #[model(getr, set)]
    name: String,

    #[model(getr)]
    fact: String,
    #[model(getr)]
    comparefunction: CompareFunction,
    #[model(getr)]
    value: i32,

    #[model(getr)]
    displayname: String,
}
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub enum ScriptParameter {
    String(String, String),
    CName(String, String),
    Float(String, f32),
    Bool(String, bool),
    Int32(String, i32),
}
// ----------------------------------------------------------------------------
#[derive(Default, Clone, Model)]
pub struct ScriptCall {
    #[model(getr, set)]
    function: String,
    #[model(getr, add)]
    params: Vec<ScriptParameter>,
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use {Result, ValidatableElement};
// ----------------------------------------------------------------------------
impl FactsDbCondition {
    // ------------------------------------------------------------------------
    pub fn new(factid: String, operator: CompareFunction, value: i32) -> FactsDbCondition {
        let displayname = format!("{} {} {}?", factid, operator, value);

        FactsDbCondition {
            // modelid: 0,
            name: "".to_string(),
            fact: factid,
            comparefunction: operator,
            value,
            displayname,
        }
    }
    // ------------------------------------------------------------------------
    pub fn condition(&self) -> (&str, &CompareFunction, i32) {
        (&self.fact, &self.comparefunction, self.value)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl CompareFunction {
    // ------------------------------------------------------------------------
    pub fn as_redid(&self) -> &'static str {
        match *self {
            CompareFunction::EQ => "CF_Equal",
            CompareFunction::NEQ => "CF_NotEqual",
            CompareFunction::LT => "CF_Less",
            CompareFunction::LTE => "CF_LessEqual",
            CompareFunction::GT => "CF_Greater",
            CompareFunction::GTE => "CF_GreaterEqual",
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl LogicOperation {
    // ------------------------------------------------------------------------
    pub fn as_redid(&self) -> &'static str {
        match *self {
            LogicOperation::AND => "LO_And",
            LogicOperation::OR => "LO_Or",
            LogicOperation::XOR => "LO_Xor",
            LogicOperation::NAND => "LO_Nand",
            LogicOperation::NOR => "LO_Nor",
            LogicOperation::NXOR => "LO_Nxor",
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ScriptCall {
    // ------------------------------------------------------------------------
    pub fn new(name: &str) -> ScriptCall {
        ScriptCall {
            function: name.to_string(),
            params: Vec::new(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Validation
// ----------------------------------------------------------------------------
use validator;
// ----------------------------------------------------------------------------
impl ValidatableElement for FactsDbCondition {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        validator::validate_str_default(&self.fact, id, "^[0-9_a-zA-Z]*$", "[a-zA-Z_0-9]")
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for ScriptCall {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        validator::validate_str_default(
            &self.function,
            &format!("{} function", id),
            "^[0-9_a-zA-Z]*$",
            "[a-z_0-9]",
        )?;
        for (i, param) in self.params.iter().enumerate() {
            param.validate(&format!("{} #{} parameter", id, i + 1))?
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for ScriptParameter {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        let validators = [
            validator::is_nonempty(),
            validator::chars("^[0-9_a-zA-Z]*$", "[a-z_0-9]"),
            validator::min_length(1),
            validator::max_length(100),
        ];
        match self {
            ScriptParameter::String(name, _)
            | ScriptParameter::Bool(name, _)
            | ScriptParameter::Float(name, _)
            | ScriptParameter::Int32(name, _) => validator::validate_str(name, id, &validators),
            ScriptParameter::CName(name, value) => {
                validator::validate_str(name, id, &validators)?;
                validator::validate_str(value, id, &validators)
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Default
// ----------------------------------------------------------------------------
impl Default for FactsDbCondition {
    // ------------------------------------------------------------------------
    fn default() -> FactsDbCondition {
        FactsDbCondition {
            // modelid: 0,
            name: "".to_string(),
            fact: "".to_string(),
            comparefunction: CompareFunction::EQ,
            value: 0,
            displayname: "".to_string(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Converter
// ----------------------------------------------------------------------------
use std::convert::TryFrom;

impl<'a> TryFrom<&'a str> for CompareFunction {
    type Error = String;
    // ------------------------------------------------------------------------
    fn try_from(operator: &str) -> Result<Self> {
        match operator {
            "=" | "==" => Ok(CompareFunction::EQ),
            "!=" => Ok(CompareFunction::NEQ),
            "<" => Ok(CompareFunction::LT),
            "<=" => Ok(CompareFunction::LTE),
            ">" => Ok(CompareFunction::GT),
            ">=" => Ok(CompareFunction::GTE),
            invalid => Err(format!(
                "valid comparison operators [=, !=, <, <=, >, >=]. found: {}",
                invalid
            )),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
use std::fmt;
// ----------------------------------------------------------------------------
impl fmt::Display for LogicOperation {
    // ------------------------------------------------------------------------
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            LogicOperation::AND => write!(f, "and"),
            LogicOperation::OR => write!(f, "or"),
            LogicOperation::XOR => write!(f, "xor"),
            LogicOperation::NAND => write!(f, "nand"),
            LogicOperation::NOR => write!(f, "nor"),
            LogicOperation::NXOR => write!(f, "nxor"),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl fmt::Display for CompareFunction {
    // ------------------------------------------------------------------------
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            CompareFunction::EQ => write!(f, "=="),
            CompareFunction::NEQ => write!(f, "!="),
            CompareFunction::LT => write!(f, "<"),
            CompareFunction::LTE => write!(f, "<="),
            CompareFunction::GT => write!(f, ">"),
            CompareFunction::GTE => write!(f, ">="),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------

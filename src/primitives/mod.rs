//
// w3 data model primitives
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub const TILE_SIZE: f32 = 64.0;
// ----------------------------------------------------------------------------
#[derive(Eq, PartialEq, Hash, Debug, Ord, PartialOrd, Clone, Copy)]
pub struct TileId(pub i32, pub i32);
// ----------------------------------------------------------------------------
// helper struct to map valid textureslots
#[derive(Eq, PartialEq, Debug, Ord, PartialOrd, Clone, Copy)]
pub struct TextureSlot<const N: u8>(pub usize);
// ----------------------------------------------------------------------------
#[allow(clippy::upper_case_acronyms)]
#[derive(Clone)]
pub struct GUID {
    pub uuid: Uuid,
}
// ----------------------------------------------------------------------------
#[derive(Default, Clone, Copy, Ord, Eq, PartialOrd, PartialEq)]
pub struct Time(pub u8, pub u8);
#[derive(Default, Clone, Copy, Ord, Eq, PartialOrd, PartialEq)]
pub struct HiResTime(pub u8, pub u8, pub u8);
#[derive(Debug, Default, Clone, Copy)]
pub struct DateTime {
    pub date: (u16, u8, u8),
    pub time: (u8, u8, u8, u32),
}
// ----------------------------------------------------------------------------
#[derive(Default, Clone, Copy, PartialEq)]
pub struct Vector2D(pub f32, pub f32);
#[derive(Default, Clone, Copy, PartialEq, Debug)]
pub struct Vector3D(pub f32, pub f32, pub f32);
#[derive(Default, Clone, Copy, PartialEq)]
pub struct Vector4D(pub f32, pub f32, pub f32, pub f32);
#[derive(Default, Clone, Copy, PartialEq, Debug)]
pub struct Rotation(pub f32, pub f32, pub f32);
#[derive(Clone, Copy)]
pub struct RotatedPosition(pub Vector3D, pub Rotation);
// ----------------------------------------------------------------------------
#[derive(Default, Clone, Copy, Debug)]
pub struct Transform3D {
    pub pos: Vector3D,
    pub rot: Rotation,
    pub scale: Vector3D,
}
// ----------------------------------------------------------------------------
#[derive(Default)]
pub struct MinMaxBox {
    pub min: Vector3D,
    pub max: Vector3D,
}
// ----------------------------------------------------------------------------
pub mod enums;
pub mod references;
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::ops;
use uuid::{Uuid, UuidVersion};
use Result;
// ----------------------------------------------------------------------------
impl GUID {
    pub fn new() -> GUID {
        GUID {
            uuid: Uuid::new(UuidVersion::Random).unwrap_or_else(|| panic!("uuid generator error")),
        }
    }
}
// ----------------------------------------------------------------------------
impl Time {
    // ------------------------------------------------------------------------
    pub fn new(hour: u8, min: u8) -> Time {
        Time(hour, min)
    }
    // ------------------------------------------------------------------------
    pub fn as_int(self) -> u32 {
        u32::from(self.0) * 3600 + u32::from(self.1) * 60
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl HiResTime {
    // ------------------------------------------------------------------------
    pub fn new(hour: u8, min: u8, sec: u8) -> HiResTime {
        HiResTime(hour, min, sec)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ops::Sub for Vector3D {
    type Output = Vector3D;
    // ------------------------------------------------------------------------
    fn sub(self, point: Vector3D) -> Self {
        Vector3D(self.0 - point.0, self.1 - point.1, self.2 - point.2)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ops::Add for Vector3D {
    type Output = Vector3D;
    // ------------------------------------------------------------------------
    fn add(self, point: Vector3D) -> Self {
        Vector3D(self.0 + point.0, self.1 + point.1, self.2 + point.2)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ops::Mul<Vector3D> for f32 {
    type Output = Vector3D;
    // ------------------------------------------------------------------------
    fn mul(self, point: Vector3D) -> Vector3D {
        Vector3D(point.0 * self, point.1 * self, point.2 * self)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl MinMaxBox {
    // ------------------------------------------------------------------------
    pub fn extend(&mut self, point: &Vector3D) {
        self.min = Vector3D(
            if point.0 < self.min.0 {
                point.0
            } else {
                self.min.0
            },
            if point.1 < self.min.1 {
                point.1
            } else {
                self.min.1
            },
            if point.2 < self.min.2 {
                point.2
            } else {
                self.min.2
            },
        );
        self.max = Vector3D(
            if point.0 > self.max.0 {
                point.0
            } else {
                self.max.0
            },
            if point.1 > self.max.1 {
                point.1
            } else {
                self.max.1
            },
            if point.2 > self.max.2 {
                point.2
            } else {
                self.max.2
            },
        );
    }
    // ------------------------------------------------------------------------
    pub fn merge(&mut self, other: &MinMaxBox) {
        self.extend(&other.min);
        self.extend(&other.max);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Transform3D {
    // ------------------------------------------------------------------------
    pub fn is_zero(&self) -> bool {
        self.pos == Vector3D(0.0, 0.0, 0.0)
            && self.rot == Rotation(0.0, 0.0, 0.0)
            && self.scale == Vector3D(0.0, 0.0, 0.0)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// default
// ----------------------------------------------------------------------------
impl Default for GUID {
    // ------------------------------------------------------------------------
    fn default() -> GUID {
        GUID {
            uuid: Uuid::new(UuidVersion::Random).unwrap_or_else(|| panic!("uuid generator error")),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Converter
// ----------------------------------------------------------------------------
use std::convert::TryFrom;

impl<'a> TryFrom<&'a str> for Time {
    type Error = String;
    // ------------------------------------------------------------------------
    fn try_from(src: &str) -> Result<Self> {
        if src.len() == 5 {
            let (hour, minutes) = src.split_at(2);
            let hour: u8 = hour
                .parse()
                .map_err(|_| "could not parse hour".to_string())?;
            let minutes: u8 = minutes
                .split_at(1)
                .1
                .parse()
                .map_err(|_| "could not parse minutes".to_string())?;

            if (hour < 24 && minutes < 60) || (hour == 24 && minutes == 0) {
                return Ok(Time::new(hour, minutes));
            }
        }
        Err(format!("expected HH:mm. found: {}", src))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFrom<&'a str> for HiResTime {
    type Error = String;
    // ------------------------------------------------------------------------
    fn try_from(src: &str) -> Result<Self> {
        if src.len() == 8 {
            let (hour, min_sec) = src.split_at(2);
            let (minutes, seconds) = min_sec.split_at(3);

            let hour: u8 = hour
                .parse()
                .map_err(|_| "could not parse hour".to_string())?;
            let minutes: u8 = minutes
                .split_at(1)
                .1
                .parse()
                .map_err(|_| "could not parse minutes".to_string())?;
            let seconds: u8 = seconds
                .split_at(1)
                .1
                .parse()
                .map_err(|_| "could not parse seconds".to_string())?;

            if minutes < 60 && seconds < 60 {
                return Ok(HiResTime::new(hour, minutes, seconds));
            }
        }
        Err(format!("expected HH:mm:ss. found: {}", src))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl TryFrom<(f32, f32)> for TileId {
    type Error = String;
    // ------------------------------------------------------------------------
    fn try_from(pos: (f32, f32)) -> Result<Self> {
        let x = if pos.0 < 0.0 {
            f32::ceil(pos.0 / -TILE_SIZE) * -TILE_SIZE
        } else {
            f32::floor(pos.0 / TILE_SIZE) * TILE_SIZE
        };
        let y = if pos.1 < 0.0 {
            f32::ceil(pos.1 / -TILE_SIZE) * -TILE_SIZE
        } else {
            f32::floor(pos.1 / TILE_SIZE) * TILE_SIZE
        };

        if x > i32::max_value() as f32
            || x < i32::min_value() as f32
            || y > i32::max_value() as f32
            || y < i32::min_value() as f32
        {
            Err(format!("out of range tile id: {}x{}", x, y))
        } else {
            Ok(TileId(x as i32, y as i32))
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a, const N: u8> TryFrom<&'a str> for TextureSlot<N> {
    type Error = String;
    // ------------------------------------------------------------------------
    fn try_from(src: &str) -> Result<TextureSlot<N>> {
        if src.len() == 10 && src.starts_with("texture_") {
            let (_, slotid) = src.split_at(8);

            match slotid.parse::<u8>() {
                Ok(v) if (1..=N).contains(&v) => return Ok(TextureSlot(v as usize - 1)),
                _ => {}
            }
        }
        Err(format!(
            "expected texture_xx with xx in [01..{}]. found: {}",
            N, src
        ))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl From<Vector2D> for (f32, f32) {
    fn from(v: Vector2D) -> (f32, f32) {
        (v.0, v.1)
    }
}
// ----------------------------------------------------------------------------
impl From<Vector3D> for (f32, f32, f32) {
    fn from(v: Vector3D) -> (f32, f32, f32) {
        (v.0, v.1, v.2)
    }
}
// ----------------------------------------------------------------------------
impl From<Vector4D> for (f32, f32, f32, f32) {
    fn from(v: Vector4D) -> (f32, f32, f32, f32) {
        (v.0, v.1, v.2, v.3)
    }
}
// ----------------------------------------------------------------------------
impl From<Rotation> for (f32, f32, f32) {
    fn from(v: Rotation) -> (f32, f32, f32) {
        (v.0, v.1, v.2)
    }
}
// ----------------------------------------------------------------------------
impl From<(f32, f32)> for Vector2D {
    fn from(v: (f32, f32)) -> Vector2D {
        Vector2D(v.0, v.1)
    }
}
// ----------------------------------------------------------------------------
impl From<(f32, f32, f32)> for Vector3D {
    fn from(v: (f32, f32, f32)) -> Vector3D {
        Vector3D(v.0, v.1, v.2)
    }
}
// ----------------------------------------------------------------------------
impl From<(f32, f32, f32, f32)> for Vector4D {
    fn from(v: (f32, f32, f32, f32)) -> Vector4D {
        Vector4D(v.0, v.1, v.2, v.3)
    }
}
// ----------------------------------------------------------------------------
impl From<(f32, f32, f32)> for Rotation {
    fn from(v: (f32, f32, f32)) -> Rotation {
        Rotation(v.0, v.1, v.2)
    }
}
// ----------------------------------------------------------------------------
impl From<RotatedPosition> for Transform3D {
    // ------------------------------------------------------------------------
    fn from(rp: RotatedPosition) -> Transform3D {
        Transform3D {
            pos: rp.0,
            rot: rp.1,
            scale: Vector3D::default(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl From<TileId> for Vector3D {
    // ------------------------------------------------------------------------
    fn from(id: TileId) -> Vector3D {
        Vector3D(id.0 as f32, id.1 as f32, 0.0)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> From<&'a Uuid> for GUID {
    // ------------------------------------------------------------------------
    fn from(uuid: &Uuid) -> GUID {
        GUID { uuid: *uuid }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Display trait impl
// ----------------------------------------------------------------------------
use std::fmt;
// ----------------------------------------------------------------------------
impl fmt::Display for Time {
    // ------------------------------------------------------------------------
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:0>2}:{:0>2}", self.0, self.1)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl fmt::Display for HiResTime {
    // ------------------------------------------------------------------------
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:0>2}:{:0>2}:{:0>2}", self.0, self.1, self.2)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl fmt::Display for Transform3D {
    // ------------------------------------------------------------------------
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "({},{},{}) ({},{},{}) ({},{},{})",
            self.pos.0,
            self.pos.1,
            self.pos.2,
            self.rot.0,
            self.rot.1,
            self.rot.2,
            self.scale.0,
            self.scale.1,
            self.scale.2
        )
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl fmt::Display for GUID {
    // ------------------------------------------------------------------------
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.uuid)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
#[cfg(test)]
mod test {
    use super::TileId;
    use std::convert::TryFrom;

    #[test]
    fn test_tileid() {
        assert_eq!(Ok(TileId(0, 0)), TileId::try_from((0.0, 0.0)));
        assert_eq!(Ok(TileId(0, 0)), TileId::try_from((1.0, 0.0)));
        assert_eq!(Ok(TileId(0, 0)), TileId::try_from((1.0, 1.0)));
        assert_eq!(Ok(TileId(0, 0)), TileId::try_from((63.5, 63.5)));
        assert_eq!(Ok(TileId(64, 0)), TileId::try_from((64.0, 63.5)));
        assert_eq!(Ok(TileId(0, 64)), TileId::try_from((63.5, 64.0)));
        assert_eq!(Ok(TileId(0, 64)), TileId::try_from((63.5, 127.9)));
        assert_eq!(Ok(TileId(0, 128)), TileId::try_from((63.5, 128.0)));
        assert_eq!(Ok(TileId(-64, 0)), TileId::try_from((-64.0, 0.0)));
        assert_eq!(Ok(TileId(-128, 0)), TileId::try_from((-64.1, 0.0)));
        assert_eq!(Ok(TileId(-128, 0)), TileId::try_from((-128.0, 0.0)));
        assert_eq!(Ok(TileId(-64, 64)), TileId::try_from((-64.0, 64.0)));
        assert_eq!(Ok(TileId(0, -64)), TileId::try_from((63.9, -64.0)));
        assert_eq!(Ok(TileId(0, -64)), TileId::try_from((63.9, -1.0)));
    }
}
// ----------------------------------------------------------------------------

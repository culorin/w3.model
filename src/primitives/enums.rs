//
// model::primitives::enums
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub enum MoveType {
    Walk,
    Run,
    FastRun,
    Sprint,
    AbsSpeed,
    Unchecked(String),
}
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub enum ActorImmortality {
    None,
    Immortal,
    Invulnerable,
    Unconscious,
    Unchecked(String),
}
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub enum SpawnVisibility {
    Hidden,
    Always,
    Visible,
    Unchecked(String),
}
// ----------------------------------------------------------------------------
pub enum LayerVariant {
    Static,
    NonStatic,
}
// ----------------------------------------------------------------------------
pub enum LayerSpecialization {
    None,
    Ignored,
    EnvOutdoor,
    EnvIndoor,
    EnvUnderground,
    Quest,
    Communities,
    Audio,
    Nav,
    Gameplay,
    Dlc,
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::convert::TryFrom;

use Result;
// ----------------------------------------------------------------------------
// str -> enum
// ----------------------------------------------------------------------------
impl<'a> TryFrom<&'a str> for MoveType {
    type Error = String;
    // ------------------------------------------------------------------------
    fn try_from(s: &str) -> Result<MoveType> {
        use self::MoveType::*;

        Ok(match s.replace('_', "").to_lowercase().as_str() {
            "walk" => Walk,
            "run" => Run,
            "fastrun" => FastRun,
            "sprint" => Sprint,
            "absspeed" => AbsSpeed,
            _ if s.starts_with("MT_") => {
                warn!("using unchecked enum value for MoveType: {}", s);
                Unchecked(s.to_owned())
            }
            _ => return Err(format!("invalid value for MoveType: {}", s)),
        })
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFrom<&'a str> for ActorImmortality {
    type Error = String;
    // ------------------------------------------------------------------------
    fn try_from(s: &str) -> Result<ActorImmortality> {
        use self::ActorImmortality::*;

        Ok(match s.replace('_', "").to_lowercase().as_str() {
            "none" => None,
            "immortal" => Immortal,
            "invulnerable" => Invulnerable,
            "unconscious" => Unconscious,
            _ if s.starts_with("AIM_") => {
                warn!("using unchecked enum value for ActorImmortalityMode: {}", s);
                Unchecked(s.to_owned())
            }
            _ => return Err(format!("invalid value for ActorImmortalityMode: {}", s)),
        })
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFrom<&'a str> for SpawnVisibility {
    type Error = String;
    // ------------------------------------------------------------------------
    fn try_from(s: &str) -> Result<SpawnVisibility> {
        use self::SpawnVisibility::*;

        Ok(match s.replace('_', "").to_lowercase().as_str() {
            "hidden" => Hidden,
            "always" => Always,
            "visible" => Visible,
            _ if s.starts_with("STSV_") => {
                warn!(
                    "using unchecked enum value for SpawnTreeSpawnVisibility: {}",
                    s
                );
                Unchecked(s.to_owned())
            }
            _ => return Err(format!("invalid value for SpawnTreeSpawnVisibility: {}", s)),
        })
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// enum -> str
// ----------------------------------------------------------------------------
impl<'a> From<&'a MoveType> for String {
    // ------------------------------------------------------------------------
    fn from(e: &MoveType) -> String {
        use self::MoveType::*;

        match e {
            Walk => "MT_Walk",
            Run => "MT_Run",
            FastRun => "MT_FastRun",
            Sprint => "MT_Sprint",
            AbsSpeed => "MT_AbsSpeed",
            Unchecked(v) => return v.to_string(),
        }
        .to_string()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> From<&'a ActorImmortality> for String {
    // ------------------------------------------------------------------------
    fn from(e: &ActorImmortality) -> String {
        use self::ActorImmortality::*;

        match e {
            None => "AIM_None",
            Immortal => "AIM_Immortal",
            Invulnerable => "AIM_Invulnerable",
            Unconscious => "AIM_Unconscious",
            Unchecked(v) => return v.to_string(),
        }
        .to_string()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> From<&'a SpawnVisibility> for String {
    // ------------------------------------------------------------------------
    fn from(e: &SpawnVisibility) -> String {
        use self::SpawnVisibility::*;

        match e {
            Hidden => "STSV_SPAWN_HIDEN",
            Always => "STSV_SPAWN_ALWAYS",
            Visible => "STSV_SPAWN_ONLY_VISIBLE",
            Unchecked(v) => return v.to_string(),
        }
        .to_string()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> From<&'a LayerVariant> for String {
    // ------------------------------------------------------------------------
    fn from(e: &LayerVariant) -> String {
        use self::LayerVariant::*;

        match e {
            Static => "LT_AutoStatic",
            NonStatic => "LT_NonStatic",
        }
        .to_string()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> From<&'a LayerSpecialization> for String {
    // ------------------------------------------------------------------------
    fn from(e: &LayerSpecialization) -> String {
        use self::LayerSpecialization::*;

        match e {
            None => "LBT_None",
            Ignored => "LBT_Ignored",
            EnvOutdoor => "LBT_EnvOutdoor",
            EnvIndoor => "LBT_EnvIndoor",
            EnvUnderground => "LBT_EnvUnderground",
            Quest => "LBT_Quest",
            Communities => "LBT_Communities",
            Audio => "LBT_Audio",
            Nav => "LBT_Nav",
            Gameplay => "LBT_Gameplay",
            Dlc => "LBT_DLC",
        }
        .to_string()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------

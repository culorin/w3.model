//
// journal::creature
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Model)]
pub struct CreatureJournal {
    #[model(getr)]
    id: String,
    #[model(getr)]
    guid: GUID,
    #[model(getr, set)]
    name: String,
    #[model(getr, set)]
    image: Option<String>,
    #[model(getr, set)]
    group: Option<String>, // id of creaturegroup
    #[model(getr)]
    description: JournalEntryGroup,
    #[model(set, iter)]
    items: Vec<String>,
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use primitives::GUID;

use super::JournalEntryGroup;
// ----------------------------------------------------------------------------
impl CreatureJournal {
    // ------------------------------------------------------------------------
    pub fn new(id: &str) -> CreatureJournal {
        CreatureJournal {
            id: id.to_string(),
            guid: GUID::new(),
            name: String::default(),
            image: None,
            group: None,
            description: JournalEntryGroup::default(),
            items: Vec::new(),
        }
    }
    // ------------------------------------------------------------------------
    pub fn add_description(&mut self, id: &str, text: &str) {
        self.description.add(id, text)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Validation
// ----------------------------------------------------------------------------
use {Result, ValidatableElement};
// ----------------------------------------------------------------------------
impl ValidatableElement for CreatureJournal {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if self.name.is_empty() {
            validate_err_missing!(id, "name")
        } else if self.image.is_none() {
            validate_err_missing!(id, "image")
        } else if self.group.is_none() {
            validate_err_missing!(id, "group")
        } else if self.description.is_empty() {
            validate_err_missing!(id, "description")
        } else {
            Ok(())
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------

//
// model::journal structures
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Default, Model, ModelRepo)]
pub struct QuestJournals {
    #[modelrepo(getr, add)]
    characters: IndexMap<String, CharacterJournal>,
    #[modelrepo(getr, add)]
    creatures: IndexMap<String, CreatureJournal>,
    #[modelrepo(getr, add)]
    quests: IndexMap<String, QuestJournal>,
}
// ----------------------------------------------------------------------------
#[derive(Default, Model, ModelRepo)]
pub struct JournalEntryGroup {
    #[model(getr)]
    guid: GUID,
    #[modelrepo(getr)]
    entries: BTreeMap<String, JournalEntry>,
}
// ----------------------------------------------------------------------------
#[derive(Model)]
pub struct JournalEntry {
    #[model(getr)]
    pos: u8,
    #[model(getr)]
    id: String,
    #[model(getr)]
    parent_guid: Option<GUID>,
    #[model(getr)]
    guid: GUID,
    #[model(getr)]
    text: String,
}
// ----------------------------------------------------------------------------
pub use self::character::CharacterJournal;
pub use self::creature::CreatureJournal;
pub use self::quest::{
    QuestGroupJournal, QuestJournal, QuestJournalMapPin, QuestJournalObjective, QuestJournalPhase,
};
// ----------------------------------------------------------------------------
mod character;
mod creature;
mod quest;
// ----------------------------------------------------------------------------
use std::collections::BTreeMap;

use indexmap::map::{Drain, IndexMap};

use primitives::GUID;
// ----------------------------------------------------------------------------
impl QuestJournals {
    // ------------------------------------------------------------------------
    pub fn has_journals(&self) -> bool {
        !self.characters.is_empty() || !self.creatures.is_empty() || !self.quests.is_empty()
    }
    // ------------------------------------------------------------------------
    pub fn drain_characters(&mut self) -> Drain<String, CharacterJournal> {
        self.characters.drain(..)
    }
    // ------------------------------------------------------------------------
    pub fn drain_creatures(&mut self) -> Drain<String, CreatureJournal> {
        self.creatures.drain(..)
    }
    // ------------------------------------------------------------------------
    pub fn drain_quests(&mut self) -> Drain<String, QuestJournal> {
        self.quests.drain(..)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl JournalEntry {
    // ------------------------------------------------------------------------
    fn new(pos: u8, id: &str, text: &str) -> JournalEntry {
        JournalEntry {
            pos,
            id: id.to_string(),
            parent_guid: None,
            guid: GUID::new(),
            text: text.to_string(),
        }
    }
    // ------------------------------------------------------------------------
    fn new_with_parent(parent: &JournalEntryGroup, pos: u8, id: &str, text: &str) -> JournalEntry {
        JournalEntry {
            pos,
            id: id.to_string(),
            parent_guid: Some(parent.guid.clone()),
            guid: GUID::new(),
            text: text.to_string(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl JournalEntryGroup {
    // ------------------------------------------------------------------------
    pub fn add(&mut self, id: &str, text: &str) {
        let ord = self.entries.len() as u8;
        self.entries.insert(
            id.to_lowercase(),
            JournalEntry::new_with_parent(self, ord, id, text),
        );
    }
    // ------------------------------------------------------------------------
    pub fn is_empty(&self) -> bool {
        self.entries.is_empty()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Validation
// ----------------------------------------------------------------------------
use {Result, ValidatableElement};
// ----------------------------------------------------------------------------
impl ValidatableElement for QuestJournals {
    // ------------------------------------------------------------------------
    fn validate(&self, _id: &str) -> Result<()> {
        // TODO validate some settings
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------

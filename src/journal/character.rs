//
// journal::character
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Model, ModelRepo)]
pub struct CharacterJournal {
    #[model(getr)]
    id: String,
    #[model(getr)]
    guid: GUID,
    #[model(getr, set)]
    name: String,
    #[model(getr, set)]
    image: Option<String>,
    #[model(getr, set)]
    group: Option<String>, // id of charactergroup
    #[modelrepo(getr)]
    entries: BTreeMap<String, JournalEntry>,
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::collections::BTreeMap;

use primitives::GUID;

use super::JournalEntry;
// ----------------------------------------------------------------------------
impl CharacterJournal {
    // ------------------------------------------------------------------------
    pub fn new(id: &str) -> CharacterJournal {
        CharacterJournal {
            id: id.to_string(),
            guid: GUID::new(),
            name: String::default(),
            image: None,
            group: None,
            entries: BTreeMap::new(),
        }
    }
    // ------------------------------------------------------------------------
    pub fn add_description(&mut self, id: &str, text: &str) {
        let ord = self.entries.len() as u8;
        self.entries
            .insert(id.to_lowercase(), JournalEntry::new(ord, id, text));
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Validation
// ----------------------------------------------------------------------------
use {Result, ValidatableElement};
// ----------------------------------------------------------------------------
impl ValidatableElement for CharacterJournal {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if self.name.is_empty() {
            validate_err_missing!(id, "name")
        } else if self.image.is_none() {
            validate_err_missing!(id, "image")
        } else if self.group.is_none() {
            validate_err_missing!(id, "group")
        } else if self.entries.is_empty() {
            validate_err_missing!(id, "description")
        } else {
            Ok(())
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------

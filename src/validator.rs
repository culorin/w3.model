//
// validators
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
// macros
// ----------------------------------------------------------------------------
macro_rules! validate_err {
    ($id: expr, $msg: expr) => ({
        return Err(format!("{}: {}", $id, $msg))
    });
}
macro_rules! validate_err_missing {
    ($id: expr, $element: expr) => ({
        return Err(format!("{}: required \"{}\" missing.", $id, $element))
    });
}
// ----------------------------------------------------------------------------
pub type StringValidator = Box<dyn Fn(&str) -> Result<()>>;
pub type IntValidator = Box<dyn Fn(i32) -> Result<()>>;
pub type FloatValidator = Box<dyn Fn(f32) -> Result<()>>;
// ----------------------------------------------------------------------------
// float
// ----------------------------------------------------------------------------
pub fn float_is_between(min: f32, max: f32) -> FloatValidator {
    Box::new(move |input: f32| {
        if input < min || input > max {
            Err(format!("value must be >= {} and <= {}", min, max))
        } else {
            Ok(())
        }
    })
}
// ----------------------------------------------------------------------------
// int
// ----------------------------------------------------------------------------
pub fn int_is_in(valid: &'static [i32]) -> IntValidator {
    Box::new(move |input: i32| {
        for v in valid {
            if *v == input {
                return Ok(())
            }
        }
        Err(format!("value must be one of the following: {:?}", valid))
    })
}
// ----------------------------------------------------------------------------
pub fn int_is_between(min: i32, max: i32) -> IntValidator {
    Box::new(move |input: i32| {
        if input < min || input > max {
            Err(format!("value must be >= {} and <= {}", min, max))
        } else {
            Ok(())
        }
    })
}
// ----------------------------------------------------------------------------
// string
// ----------------------------------------------------------------------------
pub fn is_nonempty() -> StringValidator {
    Box::new(|input: &str| {
        if input.is_empty() {
            Err(String::from("must not be empty"))
        } else {
            Ok(())
        }
    })
}
// ----------------------------------------------------------------------------
pub fn is_ascii() -> StringValidator {
    Box::new(|input: &str| {
        if input.is_ascii() {
            Ok(())
        } else {
            Err(String::from("must not contain non-ascii characters"))
        }
    })
}
// ----------------------------------------------------------------------------
pub fn length(len: usize) -> StringValidator {
    Box::new(move |input: &str| {
        if input.chars().count() != len {
            Err(format!("must be {} characters long", len))
        } else {
            Ok(())
        }
    })
}
// ----------------------------------------------------------------------------
pub fn min_length(len: usize) -> StringValidator {
    Box::new(move |input: &str| {
        let chars = input.chars().count();
        if chars >= 1 && chars < len {
            Err(format!("must be at least {} characters long", len))
        } else {
            Ok(())
        }
    })
}
// ----------------------------------------------------------------------------
pub fn max_length(len: usize) -> StringValidator {
    Box::new(move |input: &str| {
        if input.chars().count() > len {
            Err(format!("must be at most {} characters long", len))
        } else {
            Ok(())
        }
    })
}
// ----------------------------------------------------------------------------
pub fn chars(regexpr: &'static str, valid_characters_msg: &'static str) -> StringValidator {
    use regex::Regex;
    let re = Regex::new(regexpr).unwrap();

    Box::new(move |input: &str| {
        if re.is_match(input) {
            Ok(())
        } else {
            Err(format!(
                "must contain only following characters: {}",
                valid_characters_msg
            ))
        }
    })
}
// ----------------------------------------------------------------------------
pub fn is_time() -> StringValidator {
    Box::new(move |input: &str| {
        if input.chars().count() == 5 {
            let (hour, minutes) = input.split_at(2);
            let (separator, minutes) = minutes.split_at(1);

            if let Ok(hour) = hour.parse::<u8>() {
                if let Ok(minutes) = minutes.parse::<u8>() {
                    if separator == ":" && hour < 23 && minutes < 59 {
                        return Ok(());
                    }
                }
            }
        }
        Err(String::from("time must be given as HH:mm"))
    })
}
// ----------------------------------------------------------------------------
pub fn validate_float(input: f32, id: &str, validator: &[FloatValidator]) -> Result<()> {
    let mut result = Ok(());
    for v in validator {
        if let Err(msg) = v(input) {
            result = Err(format!("{}: {}", id, msg));
            break;
        }
    }
    result
}
// ----------------------------------------------------------------------------
pub fn validate_int(input: i32, id: &str, validator: &[IntValidator]) -> Result<()> {
    let mut result = Ok(());
    for v in validator {
        if let Err(msg) = v(input) {
            result = Err(format!("{}: {}", id, msg));
            break;
        }
    }
    result
}
// ----------------------------------------------------------------------------
pub fn validate_str(input: &str, id: &str, validator: &[StringValidator]) -> Result<()> {
    let mut result = Ok(());
    for v in validator {
        if let Err(msg) = v(input) {
            result = Err(format!("{}: {}", id, msg));
            break;
        }
    }
    result
}
// ----------------------------------------------------------------------------
pub fn validate_str_default(
    input: &str,
    id: &str,
    regexp: &'static str,
    charset: &'static str,
) -> Result<()> {
    validate_str(input, id, &[
        is_nonempty(),
        chars(regexp, charset),
        min_length(2),
        max_length(250),
    ])
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use super::Result;
// ----------------------------------------------------------------------------

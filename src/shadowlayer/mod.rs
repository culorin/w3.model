//
// model::shadowmesh
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Default, Model, ModelRepo, Debug)]
pub struct ShadowLayersDefinition {
    #[model(getr, set)]
    id: String,
    #[modelrepo(add)]
    #[model(getr)]
    hublayers: BTreeMap<String, ShadowLayers>, // world -> layers
}
// ----------------------------------------------------------------------------
#[derive(Default, Model, ModelRepo, Debug)]
pub struct ShadowLayers {
    #[modelrepo(add)]
    #[model(getr)]
    layers: BTreeMap<String, ShadowLayer>, // merged_grid_1x1
}
// ----------------------------------------------------------------------------
#[derive(Model, ModelRepo, Default, Debug)]
pub struct ShadowLayer {
    #[model(getr)]
    overwrite: bool,
    #[modelrepo(add)]
    #[model(getr)]
    entities: BTreeMap<String, ShadowMeshEntity>,
}
// ----------------------------------------------------------------------------
#[derive(Model, Debug)]
pub struct ShadowMeshEntity {
    #[model(getr)]
    name: String,
    #[model(getr, set)]
    transform: Transform3D,
    #[model(getr, add)]
    meshes: Vec<ShadowMeshInfo>,
}
// ----------------------------------------------------------------------------
#[derive(Model, Debug)]
pub struct ShadowMeshInfo {
    #[model(getr, set)]
    srcmesh: String,
    #[model(getr, set)]
    mesh: String,
    #[model(getr, set)]
    transform: Transform3D,
}
// ----------------------------------------------------------------------------
use std::collections::BTreeMap;
use std::convert::TryFrom;

use primitives::{Rotation, TileId, Transform3D, Vector3D};

use {Result, ValidatableElement};
// ----------------------------------------------------------------------------
impl ShadowLayersDefinition {
    // ------------------------------------------------------------------------
    pub fn new(id: &str) -> ShadowLayersDefinition {
        ShadowLayersDefinition {
            id: id.to_lowercase(),
            hublayers: BTreeMap::default(),
        }
    }
    // ------------------------------------------------------------------------
    pub fn is_empty(&self) -> bool {
        self.hublayers.is_empty()
    }
    // ------------------------------------------------------------------------
    pub fn mesh_mapping(&self) -> BTreeMap<&String, &String> {
        self.hublayers
            .values()
            .flat_map(|hub| hub.layers.values())
            .flat_map(|layer| layer.entities.values())
            .flat_map(|entity| entity.meshes.iter())
            .map(|mesh| (&mesh.srcmesh, &mesh.mesh))
            .collect()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ShadowLayers {
    // ------------------------------------------------------------------------
    pub fn add_partitioned(&mut self, entities: Vec<ShadowMeshEntity>) -> Result<()> {
        for entity in entities {
            let tileid = TileId::try_from((entity.transform.pos.0, entity.transform.pos.1))?;

            let tile = self
                .layers
                .entry(format!("merged_grid_{}x{}", tileid.0, tileid.1))
                .or_insert_with(ShadowLayer::default);

            if tile.entities.contains_key(&entity.name) {
                let mut i = 1;
                let mut uniq = format!("{}_{}", entity.name, i);
                while tile.entities.contains_key(&uniq) {
                    i += 1;
                    uniq = format!("{}_{}", entity.name, i);
                }
                tile.entities.insert(uniq, entity);
            } else {
                tile.entities.insert(entity.name.to_owned(), entity);
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    pub fn is_empty(&self) -> bool {
        self.layers.is_empty()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ShadowMeshEntity {
    // ------------------------------------------------------------------------
    pub fn new(name: &str, pos: Vector3D, rot: Rotation, scale: Vector3D) -> ShadowMeshEntity {
        ShadowMeshEntity {
            name: name.to_owned(),
            transform: Transform3D { pos, rot, scale },
            meshes: Vec::default(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ShadowMeshInfo {
    // ------------------------------------------------------------------------
    pub fn new(
        srcmesh: &str,
        shadowmesh: &str,
        pos: Vector3D,
        rot: Rotation,
        scale: Vector3D,
    ) -> Self {
        ShadowMeshInfo {
            srcmesh: srcmesh.to_owned(),
            mesh: shadowmesh.to_owned(),
            transform: Transform3D { pos, rot, scale },
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Default for ShadowMeshInfo {
    // ------------------------------------------------------------------------
    fn default() -> Self {
        ShadowMeshInfo {
            srcmesh: String::default(),
            mesh: String::default(),
            transform: Transform3D {
                pos: Vector3D::default(),
                rot: Rotation::default(),
                scale: Vector3D::from((1.0, 1.0, 1.0)),
            },
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for ShadowLayer {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if self.entities.is_empty() {
            return Err(format!("{}: mesh layer does not contain any entities.", id));
        }
        Ok(())
    }
}
// ----------------------------------------------------------------------------
impl ValidatableElement for ShadowMeshEntity {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if self.meshes.is_empty() {
            return Err(format!("{}: mesh entity does not contain any meshes.", id));
        }
        Ok(())
    }
}
// ----------------------------------------------------------------------------
impl ValidatableElement for ShadowMeshInfo {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if self.srcmesh.is_empty() {
            validate_err_missing!(id, "src");
        }
        if self.mesh.is_empty() {
            validate_err_missing!(id, "shadowmesh");
        }

        if !self.srcmesh.ends_with(".w2mesh") {
            return Err(format!(
                "{}: expected src mesh reference file extension \".w2mesh\". found: {}",
                id, self.srcmesh
            ));
        }
        if !self.mesh.ends_with(".w2mesh") {
            return Err(format!(
                "{}: expected shadowmesh reference file extension \".w2mesh\". found: {}",
                id, self.mesh
            ));
        }
        Ok(())
    }
}
// ----------------------------------------------------------------------------

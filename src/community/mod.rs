//
// model::community
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Default, ModelRepo)]
pub struct QuestCommunities {
    #[modelrepo(getr, add, has)]
    communities: IndexMap<String, Community>,
}
// ----------------------------------------------------------------------------
#[derive(Model)]
pub struct Community {
    #[model(getr)]
    id: String,
    #[model(set)]
    context: Option<String>,
    #[model(getr)]
    actors: BTreeMap<String, Actor>,
    #[model(getr, getmr)]
    phases: BTreeMap<String, Phase>,
}
// ----------------------------------------------------------------------------
#[derive(Model)]
pub struct Actor {
    #[model(getr)]
    name: String,
    #[model(getr)]
    template: EntityReference,
    #[model(iter, set)]
    appearances: Vec<String>,
    #[model(iter, set)]
    tags: Vec<String>,
    #[model(getr)]
    initializer: Vec<spawntree::SpawnDecoration>,
}
// ----------------------------------------------------------------------------
#[derive(Default)]
pub struct Phase(BTreeMap<String, ActorPhase>);
// ----------------------------------------------------------------------------
#[derive(Model)]
pub struct ActorPhase {
    #[model(getr)]
    actor: String,
    #[model(getr, set)]
    start_in_ap: Option<bool>,
    #[model(getr, set)]
    use_last_ap: Option<bool>,
    #[model(getr, set)]
    spawn_hidden: Option<bool>,
    #[model(getr)]
    spawntimes: BTreeMap<Time, SpawnSettings>,
    #[model(getr)]
    actionpoints: BTreeMap<Time, Vec<ActionPoint>>,
    #[model(iter, set, add)]
    spawnpoints: Vec<LayerTagReference>,
    #[model(iter, set, add)]
    despawnpoints: Vec<LayerTagReference>,
    #[model(getr)]
    decorator: Vec<spawntree::SpawnDecoration>,
}
// ----------------------------------------------------------------------------
#[derive(Model)]
pub struct SpawnSettings {
    #[model(getr)]
    quantity: u8,
    #[model(getr)]
    respawn: bool,
    #[model(getr)]
    respawn_delay: Option<u32>,
}
// ----------------------------------------------------------------------------
#[derive(Model)]
pub struct ActionPoint {
    #[model(getr)]
    actionpoint: LayerTagReference,
    #[model(getr)]
    weight: Option<f32>,
}
// ----------------------------------------------------------------------------
pub mod spawntree;
// ----------------------------------------------------------------------------
use std::ops::{Deref, DerefMut};

use std::collections::BTreeMap;
use std::iter::Iterator;

use indexmap::map::{Drain, IndexMap};

use primitives::references::{EntityReference, LayerTagReference};
use primitives::Time;
use {Result, ValidatableElement};
// ----------------------------------------------------------------------------
impl QuestCommunities {
    // ------------------------------------------------------------------------
    pub fn drain(&mut self) -> Drain<String, Community> {
        self.communities.drain(..)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Community {
    // ------------------------------------------------------------------------
    pub fn new(id: &str) -> Community {
        Community {
            id: id.to_lowercase(),
            context: None,
            actors: BTreeMap::new(),
            phases: BTreeMap::new(),
        }
    }
    // ------------------------------------------------------------------------
    pub fn add_actor(&mut self, actor: Actor) {
        self.actors.insert(actor.name.clone(), actor);
    }
    // ------------------------------------------------------------------------
    pub fn has_actor(&self, actorid: &str) -> bool {
        self.actors.contains_key(actorid)
    }
    // ------------------------------------------------------------------------
    pub fn add_actorphase(&mut self, phasename: &str, actorphase: ActorPhase) {
        let phase = self
            .phases
            .entry(actorphase.actor.clone())
            .or_insert_with(Phase::default);

        phase.0.insert(phasename.to_lowercase(), actorphase);
    }
    // ------------------------------------------------------------------------
    pub fn context(&self) -> &str {
        self.context.as_deref().unwrap_or("")
    }
    // ------------------------------------------------------------------------
    pub fn phase(&self, id: &str) -> Option<&Phase> {
        self.phases.get(id)
    }
    // ------------------------------------------------------------------------
    pub fn phase_ids(&self) -> impl Iterator<Item = &String> {
        self.phases.values().flat_map(|p| p.0.keys())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Actor {
    // ------------------------------------------------------------------------
    pub fn new(name: &str) -> Actor {
        Actor {
            name: name.to_lowercase(),
            template: EntityReference::Unchecked(String::default()),
            appearances: Vec::default(),
            tags: Vec::default(),
            initializer: Vec::default(),
        }
    }
    // ------------------------------------------------------------------------
    pub fn set_template(&mut self, template: EntityReference) {
        self.template = template;
    }
    // ------------------------------------------------------------------------
    pub fn add_initializer<I: Into<spawntree::SpawnDecoration>>(&mut self, initializer: I) {
        self.initializer.push(initializer.into());
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ActorPhase {
    // ------------------------------------------------------------------------
    pub fn new(name: &str) -> ActorPhase {
        ActorPhase {
            actor: name.to_lowercase(),
            start_in_ap: Some(true),
            use_last_ap: Some(true),
            spawn_hidden: None,
            spawntimes: BTreeMap::new(),
            actionpoints: BTreeMap::new(),
            spawnpoints: Vec::default(),
            despawnpoints: Vec::default(),
            decorator: Vec::default(),
        }
    }
    // ------------------------------------------------------------------------
    pub fn add_actionpoint(&mut self, time: Option<&Time>, actionpoint: ActionPoint) {
        let time = match time.cloned() {
            Some(time) => time,
            None => Time::default(),
        };
        let aps = self.actionpoints.entry(time).or_insert_with(Vec::new);

        aps.push(actionpoint);
    }
    // ------------------------------------------------------------------------
    pub fn add_spawnsettings(&mut self, time: Time, settings: SpawnSettings) {
        self.spawntimes.insert(time, settings);
    }
    // ------------------------------------------------------------------------
    pub fn add_decorator<D: Into<spawntree::SpawnDecoration>>(&mut self, decorator: D) {
        self.decorator.push(decorator.into());
    }
    // ------------------------------------------------------------------------
    pub fn set_smart_ai<A: Into<spawntree::SmartAiDecorator>>(&mut self, smartai: A) {
        self.decorator
            .push(spawntree::SpawnDecoration::from(smartai.into()));
    }
    // ------------------------------------------------------------------------
    pub fn add_debug(&mut self, phasedebug: spawntree::PhaseDbgDecorator) {
        self.decorator.push(phasedebug.into());
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ActionPoint {
    // ------------------------------------------------------------------------
    pub fn new(actionpoint: LayerTagReference) -> ActionPoint {
        ActionPoint {
            actionpoint,
            weight: None,
        }
    }
    // ------------------------------------------------------------------------
    pub fn set_weight(&mut self, weight: f32) {
        self.weight = Some(weight);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl SpawnSettings {
    // ------------------------------------------------------------------------
    pub fn new(quantity: u8) -> SpawnSettings {
        SpawnSettings {
            quantity,
            respawn: false,
            respawn_delay: None,
        }
    }
    // ------------------------------------------------------------------------
    pub fn set_respawn(&mut self, respawn: bool) {
        self.respawn = respawn;
        if respawn {
            // sane default respawn setting is 5 gametime mins
            self.respawn_delay = Some(5 * 60);
        }
    }
    // ------------------------------------------------------------------------
    pub fn set_respawn_delay(&mut self, delay: u32) {
        if self.respawn {
            self.respawn_delay = Some(delay * 60)
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Deref for Phase {
    type Target = BTreeMap<String, ActorPhase>;
    // ------------------------------------------------------------------------
    fn deref(&self) -> &Self::Target {
        &self.0
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl DerefMut for Phase {
    // ------------------------------------------------------------------------
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Validation
// ----------------------------------------------------------------------------
use validator::validate_str_default;
// ----------------------------------------------------------------------------
impl ValidatableElement for QuestCommunities {
    // ------------------------------------------------------------------------
    fn validate(&self, _: &str) -> Result<()> {
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for Community {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if self.actors.is_empty() {
            validate_err!(id, "required \"actors\" definition is missing.")
        }
        if self.phases.is_empty() {
            validate_err!(id, "required \"phases\" definition is missing.")
        }
        if let Some(ref context) = self.context {
            validate_str_default(context, id, "^[0-9_a-z\\\\/]*$", "[a-z_0-9]")?
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for Actor {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if let EntityReference::Unchecked(template) = &self.template {
            if template.is_empty() {
                validate_err_missing!(id, "template")
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for ActorPhase {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        use self::spawntree::SpawnDecoration::*;

        // check for multiple smartai phase decorators
        let (smartai_decorator, idleai_decorator) =
            self.decorator.iter().fold((0, 0), |acc, d| match d {
                DynamicWork(_) | WanderPath(_) | WanderArea(_) => (acc.0 + 1, acc.1),
                IdleAi(_) => (acc.0, acc.1 + 1),
                _ => acc,
            });

        if smartai_decorator > 1 {
            validate_err!(
                id,
                "only one smartai decorator (dynamicWork, wanderArea or wanderPath) allowed."
            )
        }

        // check for multiple idleai phase decorators
        if idleai_decorator > 1 {
            validate_err!(
                id,
                format!(
                    "only one idleai decorator allowed. found: {}",
                    idleai_decorator
                )
            )
        }

        // unchecked actionpoint reference is invalid (no ap info!)
        for ap in self.actionpoints.values().flatten() {
            if let LayerTagReference::UncheckedTag(tag) = &ap.actionpoint {
                validate_err!(
                    id,
                    format!(
                        "unchecked actionpoint references not supported in community phases, \
                         found: {}",
                        tag
                    )
                )
            }
        }

        // spawnpoints & start_in_ap don't work
        if !self.spawnpoints.is_empty() && self.start_in_ap.unwrap_or(false) {
            validate_err!(
                id,
                "spawnpoint usage requires \"start_in_ap\" to be set to false."
            );
        }

        // TODO check for more invalid combinations
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------

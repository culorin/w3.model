//
// community::spawntree
//
// some spawntree classes used in more complex community definitions
//
// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// Decorators
// ----------------------------------------------------------------------------
#[allow(clippy::large_enum_variant)]
pub enum SpawnDecoration {
    AddTag(AddTag),
    SetAppearance(SetAppearance),
    SetAttitude(SetAttitude),
    SetImmortality(SetImmortality),
    SetLevel(SetLevel),
    AddItems(AddItems),
    GuardArea(GuardArea),
    Scripted(ScriptedInitializer),
    DynamicWork(DynamicWork),
    WanderPath(WanderPath),
    WanderArea(WanderArea),
    RiderAi(RiderAi),
    IdleAi(IdleAi),
    RainReaction(RainReaction),
    Debug(PhaseDbgDecorator),
}
// ----------------------------------------------------------------------------
#[derive(Clone, Model)]
pub struct AddTag {
    #[model(getr)]
    tags: Vec<String>,
    #[model(getr, set)]
    on_spawn_only: bool,
}
// ----------------------------------------------------------------------------
#[derive(Clone, Model)]
pub struct SetAppearance {
    #[model(getr)]
    appearance: String,
    #[model(getr, set)]
    on_spawn_only: bool,
}
// ----------------------------------------------------------------------------
#[derive(Clone, Model)]
pub struct SetAttitude {
    #[model(getr)]
    attitude: String,
}
// ----------------------------------------------------------------------------
#[derive(Clone, Model)]
pub struct SetImmortality {
    #[model(getr)]
    immortality_mode: ActorImmortality,
}
// ----------------------------------------------------------------------------
#[derive(Clone, Model)]
pub struct SetLevel {
    #[model(getr)]
    level: u8,
}
// ----------------------------------------------------------------------------
#[derive(Clone, Default, Model)]
pub struct AddItems {
    #[model(add, getr)]
    items: Vec<(ItemReference, u32)>,
    #[model(getr, set)]
    equip_first: bool,
    #[model(getr, set)]
    one_random: bool,
}
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub struct RainReaction;
// ----------------------------------------------------------------------------
#[derive(Clone, Model)]
pub struct ScriptedInitializer {
    #[model(getr)]
    class: String,
    #[model(getr)]
    phase: String,
}
// ----------------------------------------------------------------------------
pub enum SmartAiDecorator {
    DynamicWork(DynamicWork),
    WanderPath(WanderPath),
    WanderArea(WanderArea),
    Rider(RiderAi),
}
// ----------------------------------------------------------------------------
#[derive(Clone, Model)]
pub struct SmartAiScriptClasses {
    #[model(getr, set)]
    base: String,
    #[model(getr, set)]
    idletree: String,
    #[model(getr, set)]
    treeparam: String,
}
// ----------------------------------------------------------------------------
#[derive(Clone, Model)]
pub struct DynamicWork {
    #[model(getr, set)]
    movetype: MoveType,
    #[model(set, getr)]
    aptags: Vec<String>,
    #[model(set, getr)]
    aparea: Option<LayerTagReference>,
    #[model(set, getr)]
    categories: Vec<String>,
    #[model(set, getr)]
    keep_aps: bool,
    #[model(getr, set)]
    aiclasses: SmartAiScriptClasses,
}
// ----------------------------------------------------------------------------
#[derive(Clone, Model)]
pub struct WanderPath {
    #[model(getr, set)]
    speed: Option<f32>,
    #[model(getr, set)]
    movetype: MoveType,
    #[model(getr, set)]
    maxdistance: Option<f32>,
    #[model(getr, set)]
    rightside: bool,
    #[model(getr, set)]
    wanderpoint_group: Option<LayerTagReference>,
    #[model(getr, set)]
    aiclasses: SmartAiScriptClasses,
}
// ----------------------------------------------------------------------------
#[derive(Clone, Model)]
pub struct WanderArea {
    #[model(getr, set)]
    speed: Option<f32>,
    #[model(getr, set)]
    movetype: MoveType,
    #[model(getr, set)]
    mindistance: Option<f32>,
    #[model(getr, set)]
    maxdistance: Option<f32>,
    #[model(getr, set)]
    idle_duration: Option<f32>,
    #[model(getr, set)]
    idle_chance: Option<f32>,
    #[model(getr, set)]
    move_duration: Option<f32>,
    #[model(getr, set)]
    move_chance: Option<f32>,
    #[model(getr, set)]
    area: Option<LayerTagReference>,
    #[model(getr, set)]
    use_guardarea: bool,
    #[model(getr, set)]
    aiclasses: SmartAiScriptClasses,
}
// ----------------------------------------------------------------------------
#[derive(Clone, Model)]
pub struct RiderAi {
    #[model(getr, set)]
    active: bool,
    #[model(getr, set)]
    aiclasses: SmartAiScriptClasses,
}
// ----------------------------------------------------------------------------
/// generic idle ai classes wrapper
pub struct IdleAi(String);
// ----------------------------------------------------------------------------
#[derive(Clone, Model)]
pub struct PhaseDbgDecorator {
    #[model(getr)]
    dbg_tag: String,
}
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub enum Pursuit {
    None,
    Area(LayerTagReference),
    Range(f32),
}
// ----------------------------------------------------------------------------
#[derive(Clone, Model)]
pub struct GuardArea {
    #[model(getr)]
    guard: Option<LayerTagReference>,
    #[model(getr)]
    pursuit: Pursuit,
}
// ----------------------------------------------------------------------------
use primitives::enums::{ActorImmortality, MoveType};
use primitives::references::{ItemReference, LayerTagReference};
// ----------------------------------------------------------------------------
impl AddTag {
    // ------------------------------------------------------------------------
    pub fn new(tags: Vec<String>) -> AddTag {
        AddTag {
            tags,
            on_spawn_only: false,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl SetAppearance {
    // ------------------------------------------------------------------------
    pub fn new(appearance: String) -> SetAppearance {
        SetAppearance {
            appearance,
            on_spawn_only: false,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl SetAttitude {
    // ------------------------------------------------------------------------
    pub fn new(attitude: String) -> Self {
        SetAttitude { attitude }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl SetImmortality {
    // ------------------------------------------------------------------------
    pub fn new(immortality: ActorImmortality) -> Self {
        SetImmortality {
            immortality_mode: immortality,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl SetLevel {
    // ------------------------------------------------------------------------
    pub fn new(level: u8) -> Self {
        SetLevel { level }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl GuardArea {
    // ------------------------------------------------------------------------
    pub fn new(area: Option<LayerTagReference>, pursuit: Pursuit) -> GuardArea {
        GuardArea {
            guard: area,
            pursuit,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ScriptedInitializer {
    // ------------------------------------------------------------------------
    pub fn new(class: &str, phase: &str) -> ScriptedInitializer {
        ScriptedInitializer {
            class: class.to_owned(),
            phase: phase.to_owned(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl WanderPath {
    // ------------------------------------------------------------------------
    pub fn new(wanderpoints: LayerTagReference) -> WanderPath {
        WanderPath {
            wanderpoint_group: Some(wanderpoints),
            ..Default::default()
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Default for DynamicWork {
    // ------------------------------------------------------------------------
    fn default() -> DynamicWork {
        DynamicWork {
            movetype: MoveType::Walk,
            aptags: Vec::default(),
            aparea: None,
            categories: Vec::default(),
            keep_aps: false,
            aiclasses: SmartAiScriptClasses {
                base: "CSpawnTreeInitializerSmartWorkAI".to_owned(),
                idletree: "CAINpcWorkIdle".to_owned(),
                treeparam: "CAINpcWorkIdleParams".to_owned(),
            },
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Default for WanderPath {
    // ------------------------------------------------------------------------
    fn default() -> WanderPath {
        WanderPath {
            speed: None,
            movetype: MoveType::Walk,
            maxdistance: None,
            rightside: true,
            wanderpoint_group: None,
            aiclasses: SmartAiScriptClasses {
                base: "CSpawnTreeInitializerSmartWanderAI".to_owned(),
                idletree: "CAIWanderWithHistory".to_owned(),
                treeparam: "CAINpcHistoryWanderParams".to_owned(),
            },
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl WanderArea {
    // ------------------------------------------------------------------------
    pub fn new(wanderarea: LayerTagReference) -> WanderArea {
        WanderArea {
            area: Some(wanderarea),
            ..Default::default()
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Default for WanderArea {
    // ------------------------------------------------------------------------
    fn default() -> WanderArea {
        WanderArea {
            area: None,
            speed: None,
            movetype: MoveType::Walk,
            maxdistance: None,
            mindistance: None,
            idle_chance: None,
            idle_duration: None,
            move_chance: None,
            move_duration: None,
            use_guardarea: false,
            aiclasses: SmartAiScriptClasses {
                base: "CSpawnTreeInitializerSmartDynamicWanderAI".to_owned(),
                idletree: "CAIDynamicWander".to_owned(),
                treeparam: "CAIDynamicWanderParams".to_owned(),
            },
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl RiderAi {
    // ------------------------------------------------------------------------
    pub fn new(active: bool) -> RiderAi {
        RiderAi {
            active,
            ..Default::default()
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Default for RiderAi {
    // ------------------------------------------------------------------------
    fn default() -> RiderAi {
        RiderAi {
            active: true,
            aiclasses: SmartAiScriptClasses {
                base: "CSpawnTreeInitializerRiderIdleAI".to_owned(),
                idletree: "CAINpcIdleHorseRider".to_owned(),
                treeparam: "CAINpcIdleHorseRiderParams".to_owned(),
            },
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IdleAi {
    // ------------------------------------------------------------------------
    pub fn new(aiclass: &str) -> Self {
        IdleAi(aiclass.to_owned())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PhaseDbgDecorator {
    // ------------------------------------------------------------------------
    pub fn new(communityid: &str, phaseid: &str) -> PhaseDbgDecorator {
        PhaseDbgDecorator {
            dbg_tag: format!("radish_comm_phase_{}_{}", communityid, phaseid),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// converter
// ----------------------------------------------------------------------------
macro_rules! into_enum {
    ($class:ident, $enum:ident, $variant:ident) => {
        impl From<$class> for $enum {
            // ------------------------------------------------------------------------
            fn from(c: $class) -> $enum {
                $enum::$variant(c)
            }
            // ------------------------------------------------------------------------
        }
    };
}
// ----------------------------------------------------------------------------
// Decorator
// ----------------------------------------------------------------------------
into_enum!(AddTag, SpawnDecoration, AddTag);
into_enum!(SetAppearance, SpawnDecoration, SetAppearance);
into_enum!(SetAttitude, SpawnDecoration, SetAttitude);
into_enum!(SetImmortality, SpawnDecoration, SetImmortality);
into_enum!(SetLevel, SpawnDecoration, SetLevel);
into_enum!(AddItems, SpawnDecoration, AddItems);
into_enum!(GuardArea, SpawnDecoration, GuardArea);
into_enum!(RainReaction, SpawnDecoration, RainReaction);
into_enum!(ScriptedInitializer, SpawnDecoration, Scripted);
into_enum!(IdleAi, SpawnDecoration, IdleAi);
into_enum!(PhaseDbgDecorator, SpawnDecoration, Debug);
// ----------------------------------------------------------------------------
into_enum!(DynamicWork, SmartAiDecorator, DynamicWork);
into_enum!(WanderPath, SmartAiDecorator, WanderPath);
into_enum!(WanderArea, SmartAiDecorator, WanderArea);
into_enum!(RiderAi, SmartAiDecorator, Rider);
// ----------------------------------------------------------------------------
impl From<SmartAiDecorator> for SpawnDecoration {
    // ------------------------------------------------------------------------
    fn from(ai: SmartAiDecorator) -> SpawnDecoration {
        match ai {
            SmartAiDecorator::DynamicWork(ai) => SpawnDecoration::DynamicWork(ai),
            SmartAiDecorator::WanderPath(ai) => SpawnDecoration::WanderPath(ai),
            SmartAiDecorator::WanderArea(ai) => SpawnDecoration::WanderArea(ai),
            SmartAiDecorator::Rider(ai) => SpawnDecoration::RiderAi(ai),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Validation
// ----------------------------------------------------------------------------
use {Result, ValidatableElement};
// ----------------------------------------------------------------------------
impl ValidatableElement for GuardArea {
    // ------------------------------------------------------------------------
    fn validate(&self, _id: &str) -> Result<()> {
        // TODO validate some settings
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for Pursuit {
    // ------------------------------------------------------------------------
    fn validate(&self, _id: &str) -> Result<()> {
        // TODO validate some settings
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for AddTag {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if self.on_spawn_only {
            warn!("{}: boolean flag is deprecated and will be ignored!", id)
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for SetAppearance {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if self.on_spawn_only {
            warn!("{}: boolean flag is deprecated and will be ignored!", id)
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for SetAttitude {
    // ------------------------------------------------------------------------
    fn validate(&self, _: &str) -> Result<()> {
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for SetImmortality {
    // ------------------------------------------------------------------------
    fn validate(&self, _: &str) -> Result<()> {
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for SetLevel {
    // ------------------------------------------------------------------------
    fn validate(&self, _: &str) -> Result<()> {
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for AddItems {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if self.items.is_empty() {
            validate_err!(id, "at least one item must be referenced")
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for DynamicWork {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if self.aptags.is_empty() {
            validate_err_missing!(id, "actionpoint_tags");
        }
        if self.categories.is_empty() {
            validate_err_missing!(id, "categories");
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for WanderPath {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if self.wanderpoint_group.is_none() {
            validate_err_missing!(id, "wanderpoints");
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for WanderArea {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if self.area.is_none() {
            validate_err_missing!(id, "area");
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for IdleAi {
    // ------------------------------------------------------------------------
    fn validate(&self, _id: &str) -> Result<()> {
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for ScriptedInitializer {
    // ------------------------------------------------------------------------
    fn validate(&self, _: &str) -> Result<()> {
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------

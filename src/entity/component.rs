//
// entity::components
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub enum Component {
    // layer components
    Waypoint(Waypoint),
    SceneWaypoint(SceneWaypoint),
    TriggerArea(TriggerArea),
    ActionPoint(ActionPoint),
    WanderPoint(WanderPoint),
}
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub enum TriggerChannel {
    Default,
    Player,
    Npc,
    Projectiles,
    Horse,
}
// ----------------------------------------------------------------------------
#[derive(Model)]
#[model(getr)]
pub struct Waypoint {
    name: String,
    guid: GUID,
}
// ----------------------------------------------------------------------------
#[derive(Model)]
#[model(getr)]
pub struct SceneWaypoint {
    name: String,
    guid: GUID,
    //dialogset: String ?
}
// ----------------------------------------------------------------------------
#[derive(Model)]
#[model(getr)]
pub struct WanderPoint {
    name: String,
    guid: GUID,
    #[model(getr, set)]
    radius: Option<f32>,
    #[model(set, iter)]
    connections: Vec<GUID>,
}
// ----------------------------------------------------------------------------
#[derive(Model)]
pub struct TriggerArea {
    #[model(getr)]
    name: String,
    #[model(getr)]
    guid: GUID,
    pos: Vector3D,
    #[model(getr)]
    height: Option<f32>,
    #[model(getr)]
    bounding_box: MinMaxBox,
    #[model(iter)]
    worldborder: Vec<Vector3D>,
    #[model(iter)]
    channel: Vec<TriggerChannel>,
}
// ----------------------------------------------------------------------------
#[derive(Default, Model)]
#[model(getr)]
pub struct ActionPoint {
    guid: GUID,
    category: String,
    action: String,
    #[model(getr, set)]
    breakable: Option<bool>,
    #[model(getr, set)]
    collision_aware: Option<bool>,
    #[model(getr, set)]
    soft_reactions: Option<bool>,
    #[model(getr, set)]
    firesource_dependent: Option<bool>,
    #[model(getr, set)]
    ik_active: Option<bool>,
}
// ----------------------------------------------------------------------------
use std::borrow::Cow;

use primitives::{MinMaxBox, Transform3D, Vector3D, GUID};
// ----------------------------------------------------------------------------
impl Waypoint {
    // ------------------------------------------------------------------------
    pub fn new(name: &str) -> Waypoint {
        Waypoint {
            guid: GUID::new(),
            name: name.to_owned(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl SceneWaypoint {
    // ------------------------------------------------------------------------
    pub fn new(name: &str) -> SceneWaypoint {
        SceneWaypoint {
            guid: GUID::new(),
            name: name.to_owned(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl WanderPoint {
    // ------------------------------------------------------------------------
    pub fn new(name: &str) -> WanderPoint {
        WanderPoint {
            guid: GUID::new(),
            name: name.to_owned(),
            radius: None,
            connections: Vec::default(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl TriggerArea {
    // ------------------------------------------------------------------------
    pub fn new(name: &str, pos: &Vector3D, height: Option<f32>) -> TriggerArea {
        TriggerArea {
            guid: GUID::new(),
            name: name.to_owned(),
            height,
            pos: *pos,
            bounding_box: MinMaxBox {
                min: *pos,
                max: Vector3D(pos.0, pos.1, pos.2 + height.unwrap_or(2.0)),
            },
            worldborder: Vec::new(),
            channel: Vec::default(),
        }
    }
    // ------------------------------------------------------------------------
    pub fn add_border<'a, I>(&mut self, points: I) -> &mut Self
    where
        I: IntoIterator<Item = &'a Vector3D>,
    {
        for point in points {
            self.add_borderpoint(*point)
        }
        self
    }
    // ------------------------------------------------------------------------
    pub fn set_channel(&mut self, channel: Cow<Vec<TriggerChannel>>) -> &mut Self {
        self.channel = channel.into_owned();
        self
    }
    // ------------------------------------------------------------------------
    pub fn add_channel(&mut self, channel: TriggerChannel) -> &mut Self {
        self.channel.push(channel);
        self
    }
    // ------------------------------------------------------------------------
    fn add_borderpoint(&mut self, point: Vector3D) {
        self.bounding_box.extend(&point);
        self.worldborder.push(point);
    }
    // ------------------------------------------------------------------------
    pub fn pos(&self) -> Transform3D {
        // set position is only used to calculate local world points
        // therefore the origin is (0/0/0)
        Transform3D {
            scale: Vector3D(1.0, 1.0, 1.0),
            ..Default::default()
        }
    }
    // ------------------------------------------------------------------------
    pub fn localborder(&self) -> impl Iterator<Item = Vector3D> {
        self.worldborder
            .iter()
            .map(|p| *p - self.pos)
            .collect::<Vec<_>>()
            .into_iter()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ActionPoint {
    // ------------------------------------------------------------------------
    pub fn new(category: &str, action: &str) -> ActionPoint {
        ActionPoint {
            guid: GUID::new(),
            category: category.to_string(),
            action: action.to_string(),
            breakable: None,
            collision_aware: None,
            soft_reactions: None,
            firesource_dependent: None,
            ik_active: None,
        }
    }
    // ------------------------------------------------------------------------
    pub fn set_action(&mut self, category: &str, action: &str) {
        self.category = category.to_string();
        self.action = action.to_string();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Default Converter
// ----------------------------------------------------------------------------
macro_rules! into_component {
    ($componentclass:ident) => {
        impl From<$componentclass> for Component {
            // ------------------------------------------------------------------------
            fn from(c: $componentclass) -> Component {
                Component::$componentclass(c)
            }
            // ------------------------------------------------------------------------
        }
    };
}
// ----------------------------------------------------------------------------
// layer components
into_component!(Waypoint);
into_component!(SceneWaypoint);
into_component!(TriggerArea);
into_component!(ActionPoint);
into_component!(WanderPoint);
// ----------------------------------------------------------------------------

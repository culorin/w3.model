//
// model::entity::entity
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
// different types of statically supported entities
// ----------------------------------------------------------------------------
#[derive(Model)]
#[model(getr)]
pub struct BaseEntity {
    guid: GUID,
    tags: Vec<String>,
    template: Option<EntityReference>,
    streaming_distance: Option<u8>,
    placement: Transform3D,
    #[model(iter)]
    components: Vec<Component>,
    classname: Option<String>,
    debug: Option<DebugInfo>,
}
// ----------------------------------------------------------------------------
#[derive(Default, Model)]
pub struct GameplayEntity {
    _base: BaseEntity,
    #[model(getr)]
    idtag: GUID,
    #[model(getr)]
    name: String, // localized display string
}
// ----------------------------------------------------------------------------
#[derive(Default, Model)]
pub struct ActionPointEntity {
    _base: BaseEntity,
    #[model(getr)]
    idtag: GUID,
    // events: Vec<?>,
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use debug::DebugInfo;

use primitives::references::EntityReference;
use primitives::{Rotation, Transform3D, Vector3D, GUID};

use super::component::Component;
// ----------------------------------------------------------------------------
impl Default for BaseEntity {
    // ------------------------------------------------------------------------
    fn default() -> Self {
        BaseEntity {
            guid: GUID::new(),
            tags: Vec::default(),
            template: None,
            streaming_distance: Some(0),
            placement: Transform3D::default(),
            components: Vec::default(),
            classname: None,
            debug: None,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl BaseEntity {
    // ------------------------------------------------------------------------
    pub fn new_with_guid(guid: GUID) -> Self {
        BaseEntity {
            guid,
            ..Default::default()
        }
    }
    // ------------------------------------------------------------------------
    pub fn set_class(&mut self, class: Option<String>) -> &mut Self {
        self.classname = class;
        self
    }
    // ------------------------------------------------------------------------
    pub fn set_template(&mut self, template: EntityReference) -> &mut Self {
        self.template = Some(template);
        self
    }
    // ------------------------------------------------------------------------
    pub fn set_tags(&mut self, tags: &[String]) -> &mut Self {
        self.tags = tags.to_owned();
        self
    }
    // ------------------------------------------------------------------------
    pub fn set_streaming_distance(&mut self, distance: Option<u8>) -> &mut Self {
        self.streaming_distance = distance;
        self
    }
    // ------------------------------------------------------------------------
    pub fn set_pos(&mut self, pos: Vector3D) -> &mut Self {
        self.placement.pos = pos;
        self
    }
    // ------------------------------------------------------------------------
    pub fn set_rot(&mut self, rot: Rotation) -> &mut Self {
        self.placement.rot = rot;
        self
    }
    // ------------------------------------------------------------------------
    pub fn set_scale(&mut self, scale: Vector3D) -> &mut Self {
        self.placement.scale = scale;
        self
    }
    // ------------------------------------------------------------------------
    pub fn set_placement(&mut self, placement: &Transform3D) -> &mut Self {
        self.placement = *placement;
        self
    }
    // ------------------------------------------------------------------------
    pub fn add_tag(&mut self, tag: &str) -> &mut Self {
        self.tags.push(tag.into());
        self
    }
    // ------------------------------------------------------------------------
    pub fn add_component<C: Into<Component>>(&mut self, component: C) -> &mut Self {
        self.components.push(component.into());
        self
    }
    // ------------------------------------------------------------------------
    pub fn add_debuginfo(&mut self, debug: Option<DebugInfo>) -> &mut Self {
        if let Some(mut debug) = debug {
            // add layername as tag to scan for entities of specific layer
            if let (Some(ref q), Some(ref l)) =
                (debug.get_str("questid"), debug.get_str("layername"))
            {
                self.tags.push(format!("radish_layer_{}_{}", q, l));
            }

            debug.add_string_opt("class", self.classname.as_ref());

            self.debug = Some(debug);
        }
        self
    }
    // ------------------------------------------------------------------------
    pub fn uid(&self) -> String {
        format!("{}", &self.guid)
    }
    // ------------------------------------------------------------------------
    pub fn validate(&self, _errid: &str) -> Result<&Self, String> {
        // TODO validate some settings
        Ok(self)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl GameplayEntity {
    // ------------------------------------------------------------------------
    pub fn base(&self) -> &BaseEntity {
        &self._base
    }
    // ------------------------------------------------------------------------
    pub fn base_mut(&mut self) -> &mut BaseEntity {
        &mut self._base
    }
    // ------------------------------------------------------------------------
    pub fn set_name(&mut self, name: &str) -> &mut Self {
        self.name = name.into();
        self
    }
    // ------------------------------------------------------------------------
    pub fn classname(&self) -> Option<&String> {
        self._base.classname()
    }
    // ------------------------------------------------------------------------
    pub fn validate(&self, _errid: &str) -> Result<&Self, String> {
        // TODO validate some settings
        Ok(self)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ActionPointEntity {
    // ------------------------------------------------------------------------
    pub fn base(&self) -> &BaseEntity {
        &self._base
    }
    // ------------------------------------------------------------------------
    pub fn base_mut(&mut self) -> &mut BaseEntity {
        &mut self._base
    }
    // ------------------------------------------------------------------------
    pub fn classname(&self) -> Option<&String> {
        self._base.classname()
    }
    // ------------------------------------------------------------------------
    pub fn validate(&self, _errid: &str) -> Result<&Self, String> {
        // TODO validate some settings
        Ok(self)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------

//
// model::layer::entities
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub enum LayerContentEntity {
    Debug(DebugEntity),
    Static(StaticEntity),
    Interactive(InteractiveEntity),
    MapPin(MapPin),
    Area(Area),
    Waypoint(Waypoint),
    ScenePoint(ScenePoint),
    ActionPoint(ActionPoint),
    WanderPointGroup(WanderPointGroup),
}
// ----------------------------------------------------------------------------
pub enum EntitySpecialization {
    Areas(String),
    Static(String),
    MapPin(String),
    Scenepoint(String),
    Actionpoint(String),
    Waypoint(String),
}
// ----------------------------------------------------------------------------
#[derive(Model)]
pub struct StaticEntity {
    #[model(getr)]
    id: String,
    #[model(set)]
    specialization: Option<EntitySpecialization>,
}
// ----------------------------------------------------------------------------
#[derive(Model)]
pub struct InteractiveEntity {
    #[model(getr)]
    id: String,
    #[model(getr, set)]
    valid_interactions: Vec<String>,
}
// ----------------------------------------------------------------------------
#[derive(Default, Model)]
pub struct MapPin {
    #[model(getr)]
    id: String,
    #[model(getr)]
    world: String,
    #[model(getr)]
    placement: Transform3D,
    #[model(getr)]
    debug: Option<DebugInfo>,
}
// ----------------------------------------------------------------------------
#[derive(Default, Model)]
pub struct Area {
    #[model(getr)]
    id: String,
    #[model(getr)]
    guid: GUID,
    #[model(getr)]
    world: String,
    #[model(getr)]
    placement: Transform3D,
    #[model(getr, set)]
    height: Option<f32>,
    #[model(getr)]
    border: Vec<Vector3D>,
    #[model(getr, add)]
    channel: Vec<component::TriggerChannel>,
    #[model(getr)]
    debug: Option<DebugInfo>,
}
// ----------------------------------------------------------------------------
#[derive(Default, Model)]
pub struct ActionPoint {
    #[model(getr)]
    id: String,
    #[model(getr)]
    world: String,
    #[model(getr)]
    placement: Transform3D,
    #[model(getr)]
    action: (String, String),
    #[model(getr, set)]
    tags: Vec<String>,
    #[model(getr, set)]
    breakable: Option<bool>,
    #[model(getr, set)]
    collision_aware: Option<bool>,
    #[model(getr, set)]
    soft_reactions: Option<bool>,
    #[model(getr, set)]
    firesource_dependent: Option<bool>,
    #[model(getr, set)]
    ik_active: Option<bool>,
    #[model(getr)]
    debug: Option<DebugInfo>,
}
// ----------------------------------------------------------------------------
#[derive(Default, Model)]
pub struct ScenePoint {
    #[model(getr)]
    id: String,
    #[model(getr)]
    world: String,
    #[model(getr)]
    placement: Transform3D,
    #[model(getr)]
    debug: Option<DebugInfo>,
}
// ----------------------------------------------------------------------------
#[derive(Default, Model)]
pub struct Waypoint {
    #[model(getr)]
    id: String,
    #[model(getr)]
    world: String,
    #[model(getr)]
    placement: Transform3D,
    #[model(getr)]
    debug: Option<DebugInfo>,
}
// ----------------------------------------------------------------------------
#[derive(Default, Model)]
pub struct WanderPointGroup {
    #[model(getr)]
    id: String,
    #[model(getr)]
    world: String,
    #[model(getr, add)]
    points: Vec<WanderPoint>,
}
// ----------------------------------------------------------------------------
#[derive(Default, Model)]
pub struct WanderPoint {
    #[model(getr)]
    guid: GUID,
    #[model(getr)]
    placement: Transform3D,
    #[model(getr)]
    radius: Option<f32>,
    #[model(getr)]
    connections: Vec<GUID>,
    #[model(getr)]
    debug: Option<DebugInfo>,
}
// ----------------------------------------------------------------------------
#[derive(Default, Model)]
pub struct DebugEntity {
    #[model(getr)]
    id: String,
    #[model(getr)]
    tags: Vec<String>,
    #[model(getr)]
    placement: Transform3D,
    #[model(getr)]
    debug: Option<DebugInfo>,
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::convert::TryFrom;

use debug::{DebugCategory, DebugInfo};

use primitives::{Rotation, Transform3D, Vector3D, GUID};

use entity::component;

use {Result, ValidatableElement};
// ----------------------------------------------------------------------------
impl StaticEntity {
    // ------------------------------------------------------------------------
    pub fn new<I: Into<String>>(id: I) -> StaticEntity {
        StaticEntity {
            id: id.into(),
            specialization: None,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl InteractiveEntity {
    // ------------------------------------------------------------------------
    pub fn new<I: Into<String>>(id: I) -> InteractiveEntity {
        InteractiveEntity {
            id: id.into(),
            valid_interactions: Vec::default(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl MapPin {
    // ------------------------------------------------------------------------
    pub fn set_world(&mut self, worldname: &str) {
        self.world = worldname.to_lowercase();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Waypoint {
    // ------------------------------------------------------------------------
    pub fn set_world(&mut self, worldname: &str) {
        self.world = worldname.to_lowercase();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl WanderPointGroup {
    // ------------------------------------------------------------------------
    pub fn set_world(&mut self, worldname: &str) {
        self.world = worldname.to_lowercase();
    }
    // ------------------------------------------------------------------------
    pub fn connect(&mut self) {
        trace!("> linking wanderpoints in order...");

        if self.points.first().map(|p| p.placement.pos)
            == self.points.last().map(|p| p.placement.pos)
        {
            debug!("> wanderpoint startpoint equals last point: linking as cycle...");
            self.points.pop();

            let mut prev: Option<&mut WanderPoint> = None;

            for wp in &mut self.points {
                if let Some(ref mut prev) = prev {
                    prev.connections.push(wp.guid.clone());
                }
                prev = Some(wp);
            }
            let first_guid = self.points[0].guid.clone();
            let last_guid = self.points[self.points.len() - 1].guid.clone();

            if let Some(last) = self.points.last_mut() {
                last.connections.push(first_guid)
            }
            if let Some(first) = self.points.first_mut() {
                first.connections.push(last_guid)
            }
        } else {
            let mut prev: Option<&mut WanderPoint> = None;

            for wp in &mut self.points {
                if let Some(ref mut prev) = prev {
                    prev.connections.push(wp.guid.clone());
                    wp.connections.push(prev.guid.clone());
                }
                prev = Some(wp);
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl WanderPoint {
    // ------------------------------------------------------------------------
    pub fn set_pointradius(&mut self, radius: f32) {
        self.radius = Some(radius);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ScenePoint {
    // ------------------------------------------------------------------------
    pub fn set_world(&mut self, worldname: &str) {
        self.world = worldname.to_lowercase();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Area {
    // ------------------------------------------------------------------------
    pub fn add_borderpoint(&mut self, point: Vector3D) {
        self.border.push(point);
    }
    // ------------------------------------------------------------------------
    pub fn add_border<'a, I>(&mut self, points: I)
    where
        I: IntoIterator<Item = &'a Vector3D>,
    {
        for point in points {
            self.border.push(*point)
        }
    }
    // ------------------------------------------------------------------------
    pub fn set_world(&mut self, worldname: &str) {
        self.world = worldname.to_lowercase();
    }
    // ------------------------------------------------------------------------
    pub fn finalize(&mut self) -> Result<()> {
        let scale = 1.0 / self.border.len() as f32;
        self.placement.pos = scale
            * self
                .border
                .iter()
                .fold(Vector3D::default(), |sum, &p| sum + p);

        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ActionPoint {
    // ------------------------------------------------------------------------
    pub fn set_action(&mut self, category: &str, action: &str) {
        self.action = (category.into(), action.into());
    }
    // ------------------------------------------------------------------------
    pub fn set_world(&mut self, worldname: &str) {
        self.world = worldname.to_lowercase();
    }
    // ------------------------------------------------------------------------
    pub fn action_category(&self) -> Option<&str> {
        if self.action.0.is_empty() {
            None
        } else {
            Some(&self.action.0)
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl DebugEntity {
    // ------------------------------------------------------------------------
    pub fn new<I: Into<String>>(id: I, dbginfo: DebugInfo) -> DebugEntity {
        let id = id.into();
        DebugEntity {
            id: id.clone(),
            tags: vec![id],
            placement: Transform3D::default(),
            debug: Some(dbginfo),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// utility macros
// ----------------------------------------------------------------------------
macro_rules! transform_for_entityclass {
    ($class:ident) => {
        impl $class {
            // ------------------------------------------------------------------------
            pub fn set_pos(&mut self, pos: Vector3D) {
                self.placement.pos = pos;
            }
            // ------------------------------------------------------------------------
            pub fn set_rot(&mut self, rot: Rotation) {
                self.placement.rot = rot;
            }
            // ------------------------------------------------------------------------
            pub fn set_scale(&mut self, scale: Vector3D) {
                self.placement.scale = scale;
            }
            // ------------------------------------------------------------------------
            pub fn set_placement(&mut self, placement: Transform3D) {
                self.placement = placement;
            }
            // ------------------------------------------------------------------------
        }
    };
}
// ----------------------------------------------------------------------------
macro_rules! new_with_id {
    ($class:ident) => {
        impl $class {
            pub fn new<I: Into<String>>(id: I) -> $class {
                $class {
                    id: id.into(),
                    ..Default::default()
                }
            }
        }
    };
}
// ----------------------------------------------------------------------------
macro_rules! into_contententity {
    ($class:ident, $type:ident) => {
        impl From<$class> for LayerContentEntity {
            fn from(entity: $class) -> LayerContentEntity {
                LayerContentEntity::$type(entity)
            }
        }
    };
}
// ----------------------------------------------------------------------------
new_with_id!(MapPin);
new_with_id!(Area);
new_with_id!(Waypoint);
new_with_id!(ScenePoint);
new_with_id!(ActionPoint);
new_with_id!(WanderPointGroup);
// ----------------------------------------------------------------------------
transform_for_entityclass!(MapPin);
transform_for_entityclass!(ActionPoint);
transform_for_entityclass!(Waypoint);
transform_for_entityclass!(ScenePoint);
transform_for_entityclass!(WanderPoint);
// ----------------------------------------------------------------------------
impl From<StaticEntity> for LayerContentEntity {
    fn from(entity: StaticEntity) -> LayerContentEntity {
        LayerContentEntity::Static(entity)
    }
}
// ----------------------------------------------------------------------------
impl From<InteractiveEntity> for LayerContentEntity {
    fn from(entity: InteractiveEntity) -> LayerContentEntity {
        LayerContentEntity::Interactive(entity)
    }
}
// ----------------------------------------------------------------------------
impl<'a> TryFrom<&'a str> for EntitySpecialization {
    type Error = String;

    fn try_from(specialization: &str) -> Result<Self> {
        use self::EntitySpecialization::*;

        match specialization.find('.') {
            Some(pos) => {
                let (category, spec) = specialization.split_at(pos);

                match category {
                    "areas" => Ok(Areas(spec[1..].to_string())),
                    "statics" | "staticentities" => Ok(Static(spec[1..].to_string())),
                    "mappins" => Ok(MapPin(spec[1..].to_string())),
                    "scenepoints" => Ok(Scenepoint(spec[1..].to_string())),
                    "actionpoints" => Ok(Actionpoint(spec[1..].to_string())),
                    "waypoints" => Ok(Waypoint(spec[1..].to_string())),
                    _ => Err(format!(
                        "found unsupported entity specialization: {}",
                        specialization
                    )),
                }
            }
            None => Err(format!(
                "found invalid entity specialization: {}",
                specialization
            )),
        }
    }
}
// ----------------------------------------------------------------------------
into_contententity!(MapPin, MapPin);
into_contententity!(Area, Area);
into_contententity!(Waypoint, Waypoint);
into_contententity!(ScenePoint, ScenePoint);
into_contententity!(ActionPoint, ActionPoint);
into_contententity!(WanderPointGroup, WanderPointGroup);
into_contententity!(DebugEntity, Debug);
// ----------------------------------------------------------------------------
// Validation
// ----------------------------------------------------------------------------
use validator::validate_str_default as validate_str;
use validator::{float_is_between, validate_float};
// ----------------------------------------------------------------------------
impl ValidatableElement for StaticEntity {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        validate_str(
            &self.id,
            &format!("{} static entity name", id),
            "^[0-9_a-zA-Z]*$",
            "[a-z_0-9]",
        )?;

        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for InteractiveEntity {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        validate_str(
            &self.id,
            &format!("{} interactive entity name", id),
            "^[0-9_a-zA-Z]*$",
            "[a-z_0-9]",
        )?;
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for MapPin {
    // ------------------------------------------------------------------------
    fn validate(&self, _id: &str) -> Result<()> {
        // TODO validate position
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for Area {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if let Some(height) = self.height {
            validate_float(
                height,
                &format!("{}/height", id),
                &[float_is_between(0.1, 10000.0)],
            )?;
        }

        if self.border.len() < 3 {
            validate_err!(id, "at least three border points for area required.");
        }
        // convex test/reordering? 2d algorithm while ignoring z
        // https://en.wikibooks.org/wiki/Algorithm_Implementation/Geometry/Convex_hull/Monotone_chain
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for Waypoint {
    // ------------------------------------------------------------------------
    fn validate(&self, _id: &str) -> Result<()> {
        // TODO validate position
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for ScenePoint {
    // ------------------------------------------------------------------------
    fn validate(&self, _id: &str) -> Result<()> {
        // TODO validate position
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for ActionPoint {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        validate_str(
            &self.action.0,
            &format!("{} action category", id),
            "^[0-9_a-zA-Z]*$",
            "[a-z_0-9]",
        )?;
        validate_str(
            &self.action.1,
            &format!("{} action name", id),
            "^[0-9_a-zA-Z]*$",
            "[a-z_0-9]",
        )?;
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for WanderPointGroup {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if self.points.len() < 2
            || (self.points.len() == 2
                && self.points[0].placement.pos == self.points[1].placement.pos)
        {
            validate_err!(id, "at least two different wanderpoints required.");
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for WanderPoint {
    // ------------------------------------------------------------------------
    fn validate(&self, _: &str) -> Result<()> {
        // nothing to do
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// debug infos
// ----------------------------------------------------------------------------
//TODO either macro or free function
impl LayerContentEntity {
    // ------------------------------------------------------------------------
    pub fn clear_debuginfo(&mut self) {
        use self::LayerContentEntity::*;
        match *self {
            Static(ref mut e) => e.clear_debuginfo(),
            Interactive(ref mut e) => e.clear_debuginfo(),
            MapPin(ref mut e) => e.clear_debuginfo(),
            Area(ref mut e) => e.clear_debuginfo(),
            Waypoint(ref mut e) => e.clear_debuginfo(),
            ScenePoint(ref mut e) => e.clear_debuginfo(),
            ActionPoint(ref mut e) => e.clear_debuginfo(),
            WanderPointGroup(ref mut e) => e.clear_debuginfo(),
            Debug(_) => {}
        }
    }
    // ------------------------------------------------------------------------
    pub(super) fn generate_debuginfo(
        &mut self,
        questid: &str,
        layerid: &str,
        context: Option<&str>,
    ) -> Result<()> {
        use self::LayerContentEntity::*;
        match *self {
            Static(ref mut e) => e.generate_debuginfo(questid, layerid, context),
            Interactive(ref mut e) => e.generate_debuginfo(questid, layerid, context),
            MapPin(ref mut e) => e.generate_debuginfo(questid, layerid, context),
            Area(ref mut e) => e.generate_debuginfo(questid, layerid, context),
            Waypoint(ref mut e) => e.generate_debuginfo(questid, layerid, context),
            ScenePoint(ref mut e) => e.generate_debuginfo(questid, layerid, context),
            ActionPoint(ref mut e) => e.generate_debuginfo(questid, layerid, context),
            WanderPointGroup(ref mut e) => e.generate_debuginfo(questid, layerid, context),
            Debug(_) => Ok(()),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
trait GenerateDebugInfo {
    fn clear_debuginfo(&mut self);
    fn generate_debuginfo(
        &mut self,
        questid: &str,
        layername: &str,
        context: Option<&str>,
    ) -> Result<()>;
}
// ----------------------------------------------------------------------------
impl GenerateDebugInfo for StaticEntity {
    // ------------------------------------------------------------------------
    fn clear_debuginfo(&mut self) {}
    // ------------------------------------------------------------------------
    fn generate_debuginfo(
        &mut self,
        _questid: &str,
        _layername: &str,
        _context: Option<&str>,
    ) -> Result<()> {
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl GenerateDebugInfo for InteractiveEntity {
    // ------------------------------------------------------------------------
    fn clear_debuginfo(&mut self) {}
    // ------------------------------------------------------------------------
    fn generate_debuginfo(
        &mut self,
        _questid: &str,
        _layername: &str,
        _context: Option<&str>,
    ) -> Result<()> {
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl GenerateDebugInfo for MapPin {
    // ------------------------------------------------------------------------
    fn clear_debuginfo(&mut self) {
        self.debug = None;
    }
    // ------------------------------------------------------------------------
    fn generate_debuginfo(
        &mut self,
        questid: &str,
        layername: &str,
        context: Option<&str>,
    ) -> Result<()> {
        self.debug = Some(
            DebugInfo::new(DebugCategory::LayerMapPin)
                .add_str("id", self.id.clone())
                .add_str("questid", questid)
                .add_str("layername", layername)
                .add_str_opt("layercontext", context)
                .build(),
        );
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl GenerateDebugInfo for Waypoint {
    // ------------------------------------------------------------------------
    fn clear_debuginfo(&mut self) {
        self.debug = None;
    }
    // ------------------------------------------------------------------------
    fn generate_debuginfo(
        &mut self,
        questid: &str,
        layername: &str,
        context: Option<&str>,
    ) -> Result<()> {
        self.debug = Some(
            DebugInfo::new(DebugCategory::LayerWaypoint)
                .add_str("id", self.id.clone())
                .add_str("questid", questid)
                .add_str("layername", layername)
                .add_str_opt("layercontext", context)
                .build(),
        );
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl GenerateDebugInfo for WanderPointGroup {
    // ------------------------------------------------------------------------
    fn clear_debuginfo(&mut self) {
        self.points
            .iter_mut()
            .for_each(WanderPoint::clear_debuginfo);
    }
    // ------------------------------------------------------------------------
    fn generate_debuginfo(
        &mut self,
        questid: &str,
        layername: &str,
        context: Option<&str>,
    ) -> Result<()> {
        for wp in &mut self.points {
            wp.generate_debuginfo(&self.id, questid, layername, context)?;
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl WanderPoint {
    // ------------------------------------------------------------------------
    fn clear_debuginfo(&mut self) {
        self.debug = None;
    }
    // ------------------------------------------------------------------------
    #[allow(clippy::unnecessary_wraps)]
    fn generate_debuginfo(
        &mut self,
        name: &str,
        questid: &str,
        layername: &str,
        context: Option<&str>,
    ) -> Result<()> {
        self.debug = Some(
            DebugInfo::new(DebugCategory::LayerWanderpoint)
                .add_str("id", name.to_owned())
                .add_str("questid", questid)
                .add_str("layername", layername)
                .add_str_opt("layercontext", context)
                .add_float_opt("radius", self.radius)
                .build(),
        );
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl GenerateDebugInfo for ScenePoint {
    // ------------------------------------------------------------------------
    fn clear_debuginfo(&mut self) {
        self.debug = None;
    }
    // ------------------------------------------------------------------------
    fn generate_debuginfo(
        &mut self,
        questid: &str,
        layername: &str,
        context: Option<&str>,
    ) -> Result<()> {
        self.debug = Some(
            DebugInfo::new(DebugCategory::LayerScenepoint)
                .add_str("id", self.id.clone())
                .add_str("questid", questid)
                .add_str("layername", layername)
                .add_str_opt("layercontext", context)
                .build(),
        );
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl GenerateDebugInfo for Area {
    // ------------------------------------------------------------------------
    fn clear_debuginfo(&mut self) {
        self.debug = None;
    }
    // ------------------------------------------------------------------------
    fn generate_debuginfo(
        &mut self,
        questid: &str,
        layername: &str,
        context: Option<&str>,
    ) -> Result<()> {
        self.debug = Some(
            DebugInfo::new(DebugCategory::LayerArea)
                .add_str("id", self.id.clone())
                .add_str("questid", questid)
                .add_str("layername", layername)
                .add_str_opt("layercontext", context)
                .build(),
        );
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl GenerateDebugInfo for ActionPoint {
    // ------------------------------------------------------------------------
    fn clear_debuginfo(&mut self) {
        self.debug = None;
    }
    // ------------------------------------------------------------------------
    fn generate_debuginfo(
        &mut self,
        questid: &str,
        layername: &str,
        context: Option<&str>,
    ) -> Result<()> {
        self.debug = Some(
            DebugInfo::new(DebugCategory::LayerActionpoint)
                .add_str("id", self.id.clone())
                .add_str("questid", questid)
                .add_str("layername", layername)
                .add_str_opt("layercontext", context)
                .add_str("action", self.action.1.clone())
                .add_str_opt("category", self.action_category())
                .build(),
        );
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------

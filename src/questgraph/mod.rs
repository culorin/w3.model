//
// model::questgraph quest structures
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Default, ModelRepo)]
pub struct QuestStructure {
    root: Option<QuestRootSegment>,
    #[modelrepo(getr, getmr, add, del)]
    segments: IndexMap<String, QuestSegment>,
}
// ----------------------------------------------------------------------------
pub mod conditions;
pub mod journals;
pub mod misc;
pub mod primitives;
pub mod scripting;
pub mod segments;
// ----------------------------------------------------------------------------
/// quest block trait
pub trait QuestBlock: ValidatableElement {
    fn uid(&self) -> &BlockUid;
    fn segmentid(&self) -> &SegmentId;
    fn set_segmentid(&mut self, id: &SegmentId);
    // ------------------------------------------------------------------------
    fn links<'a>(&'a self) -> Box<dyn Iterator<Item = (&'a OutSocketId, &'a Vec<Link>)> + 'a>;
    fn reset_links(&mut self);
    fn add_link_to(
        &mut self,
        out_socket: Option<OutSocketId>,
        block: LinkTarget,
        in_socket: Option<InSocketId>,
    );
    fn delete_links_for_socket(&mut self, socket: &OutSocketId);
    // ------------------------------------------------------------------------
    fn editordata(&self) -> Option<&EditorBlockData>;
    fn editordata_or_default(&mut self) -> &mut EditorBlockData;
}
// ----------------------------------------------------------------------------
// Valid segment & quest level blocks
// ----------------------------------------------------------------------------
/// segment blocktypes guard wrapper
pub enum SegmentBlock {
    SubSegment(SubSegment),
    WaitUntil(WaitUntil),
    Script(Script),
    Layers(Layers),
    Spawnsets(Spawnsets),
    JournalEntry(JournalEntry),
    JournalObjective(JournalObjective),
    JournalMappin(JournalMappin),
    JournalPhaseObjectives(JournalPhaseObjectives),
    JournalQuestOutcome(JournalQuestOutcome),
    Interaction(Interaction),
    Scene(Scene),
    TimeManagement(TimeManagement),
    AddFact(AddFact),
    Reward(Reward),
    Teleport(Teleport),
    ChangeWorld(ChangeWorld),
    Randomize(Randomize),
}
// ----------------------------------------------------------------------------
pub use self::primitives::{BlockUid, EditorBlockData, SegmentId};
// ----------------------------------------------------------------------------
use std::iter::Iterator;

use indexmap::map::{Drain, IndexMap};

use self::conditions::WaitUntil;
use self::journals::{
    JournalEntry, JournalMappin, JournalObjective, JournalPhaseObjectives, JournalQuestOutcome,
};
use self::misc::{
    AddFact, ChangeWorld, Interaction, Layers, Randomize, Reward, Scene, Spawnsets, Teleport,
    TimeManagement,
};
use self::primitives::{BlockName, InSocketId, Link, LinkTarget, OutSocketId};
use self::scripting::Script;
use self::segments::{QuestRootSegment, QuestSegment, SubSegment};
use {Result, ValidatableElement};
// ----------------------------------------------------------------------------
// internal helper trait
// ----------------------------------------------------------------------------
trait ManagedQuestBlock {
    fn rename(&mut self, name: BlockName);
    fn update_linktarget_blockname(&mut self, old: &LinkTarget, new: &BlockName);
    fn prune_links(&mut self, target: &LinkTarget, valid_sockets: &[InSocketId]);
    fn change_link_target(
        &mut self,
        old_target: &LinkTarget,
        old_socket: &InSocketId,
        new_target: &LinkTarget,
        new_socket: &InSocketId,
    );
}
// ----------------------------------------------------------------------------
impl QuestStructure {
    // ------------------------------------------------------------------------
    pub fn root(&self) -> Result<&QuestRootSegment> {
        self.root
            .as_ref()
            .ok_or_else(|| "missing root segment".into())
    }
    // ------------------------------------------------------------------------
    pub fn root_mut(&mut self) -> Result<&mut QuestRootSegment> {
        self.root
            .as_mut()
            .ok_or_else(|| "missing root segment".into())
    }
    // ------------------------------------------------------------------------
    pub fn set_root(&mut self, root: QuestRootSegment) -> Result<()> {
        if self.root.is_none() {
            self.root = Some(root);
            Ok(())
        } else {
            Err("only one quest structure definition allowed.".into())
        }
    }
    // ------------------------------------------------------------------------
    pub fn take_root(&mut self) -> Result<QuestRootSegment> {
        self.root
            .take()
            .ok_or_else(|| "missing root segment".into())
    }
    // ------------------------------------------------------------------------
    pub fn drain_segments(&mut self) -> Drain<String, QuestSegment> {
        self.segments.drain(..)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for QuestStructure {
    // ------------------------------------------------------------------------
    fn validate(&self, _: &str) -> Result<()> {
        if self.root.is_some() {
            // - subsegment in/out blocks match subsegement block sockets?
            // - unused subsegments -> warn only?
            // - orphan blocks? -> warn only?
            // TODO implement validate QuestStructure
            Ok(())
        } else {
            Err("expected quest root structure definition not found.".to_string())
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// convenience methods for segmentblocks
// ----------------------------------------------------------------------------
impl ValidatableElement for SegmentBlock {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        self.as_validateable().validate(id)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl QuestBlock for SegmentBlock {
    // ------------------------------------------------------------------------
    fn uid(&self) -> &BlockUid {
        self.as_quest_block().uid()
    }
    // ------------------------------------------------------------------------
    fn segmentid(&self) -> &SegmentId {
        self.as_quest_block().segmentid()
    }
    // ------------------------------------------------------------------------
    fn set_segmentid(&mut self, id: &SegmentId) {
        self.as_quest_block_mut().set_segmentid(id)
    }
    // ------------------------------------------------------------------------
    fn links<'a>(&'a self) -> Box<dyn Iterator<Item = (&'a OutSocketId, &'a Vec<Link>)> + 'a> {
        self.as_quest_block().links()
    }
    // ------------------------------------------------------------------------
    fn reset_links(&mut self) {
        self.as_quest_block_mut().reset_links()
    }
    // ------------------------------------------------------------------------
    fn add_link_to(
        &mut self,
        out_socket: Option<OutSocketId>,
        block: LinkTarget,
        in_socket: Option<InSocketId>,
    ) {
        self.as_quest_block_mut()
            .add_link_to(out_socket, block, in_socket)
    }
    // ------------------------------------------------------------------------
    fn delete_links_for_socket(&mut self, socket: &OutSocketId) {
        self.as_quest_block_mut().delete_links_for_socket(socket);
    }
    // ------------------------------------------------------------------------
    fn editordata(&self) -> Option<&EditorBlockData> {
        self.as_quest_block().editordata()
    }
    // ------------------------------------------------------------------------
    fn editordata_or_default(&mut self) -> &mut EditorBlockData {
        self.as_quest_block_mut().editordata_or_default()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ManagedQuestBlock for SegmentBlock {
    // ------------------------------------------------------------------------
    fn rename(&mut self, new_name: BlockName) {
        self.as_managed_quest_block_mut().rename(new_name);
    }
    // ------------------------------------------------------------------------
    fn update_linktarget_blockname(&mut self, old: &LinkTarget, new: &BlockName) {
        self.as_managed_quest_block_mut()
            .update_linktarget_blockname(old, new);
    }
    // ------------------------------------------------------------------------
    fn prune_links(&mut self, target: &LinkTarget, valid_sockets: &[InSocketId]) {
        self.as_managed_quest_block_mut()
            .prune_links(target, valid_sockets);
    }
    // ------------------------------------------------------------------------
    fn change_link_target(
        &mut self,
        old_target: &LinkTarget,
        old_socket: &InSocketId,
        new_target: &LinkTarget,
        new_socket: &InSocketId,
    ) {
        self.as_managed_quest_block_mut()
            .change_link_target(old_target, old_socket, new_target, new_socket);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl SegmentBlock {
    // ------------------------------------------------------------------------
    fn as_validateable(&self) -> &dyn ValidatableElement {
        match *self {
            SegmentBlock::SubSegment(ref o) => o,
            SegmentBlock::WaitUntil(ref o) => o,
            SegmentBlock::Script(ref o) => o,
            SegmentBlock::Layers(ref o) => o,
            SegmentBlock::Spawnsets(ref o) => o,
            SegmentBlock::JournalEntry(ref o) => o,
            SegmentBlock::JournalObjective(ref o) => o,
            SegmentBlock::JournalMappin(ref o) => o,
            SegmentBlock::JournalPhaseObjectives(ref o) => o,
            SegmentBlock::JournalQuestOutcome(ref o) => o,
            SegmentBlock::Interaction(ref o) => o,
            SegmentBlock::Scene(ref o) => o,
            SegmentBlock::TimeManagement(ref o) => o,
            SegmentBlock::AddFact(ref o) => o,
            SegmentBlock::Reward(ref o) => o,
            SegmentBlock::Teleport(ref o) => o,
            SegmentBlock::ChangeWorld(ref o) => o,
            SegmentBlock::Randomize(ref o) => o,
        }
    }
    // ------------------------------------------------------------------------
    // every segmentblock has a questblock implementation, expose as trait object
    // for generic QuestBlock "implementation"
    fn as_quest_block(&self) -> &dyn QuestBlock {
        match *self {
            SegmentBlock::SubSegment(ref o) => o,
            SegmentBlock::WaitUntil(ref o) => o,
            SegmentBlock::Script(ref o) => o,
            SegmentBlock::Layers(ref o) => o,
            SegmentBlock::Spawnsets(ref o) => o,
            SegmentBlock::JournalEntry(ref o) => o,
            SegmentBlock::JournalObjective(ref o) => o,
            SegmentBlock::JournalMappin(ref o) => o,
            SegmentBlock::JournalPhaseObjectives(ref o) => o,
            SegmentBlock::JournalQuestOutcome(ref o) => o,
            SegmentBlock::Interaction(ref o) => o,
            SegmentBlock::Scene(ref o) => o,
            SegmentBlock::TimeManagement(ref o) => o,
            SegmentBlock::AddFact(ref o) => o,
            SegmentBlock::Reward(ref o) => o,
            SegmentBlock::Teleport(ref o) => o,
            SegmentBlock::ChangeWorld(ref o) => o,
            SegmentBlock::Randomize(ref o) => o,
        }
    }
    // ------------------------------------------------------------------------
    fn as_quest_block_mut(&mut self) -> &mut dyn QuestBlock {
        match *self {
            SegmentBlock::SubSegment(ref mut o) => o,
            SegmentBlock::WaitUntil(ref mut o) => o,
            SegmentBlock::Script(ref mut o) => o,
            SegmentBlock::Layers(ref mut o) => o,
            SegmentBlock::Spawnsets(ref mut o) => o,
            SegmentBlock::JournalEntry(ref mut o) => o,
            SegmentBlock::JournalObjective(ref mut o) => o,
            SegmentBlock::JournalMappin(ref mut o) => o,
            SegmentBlock::JournalPhaseObjectives(ref mut o) => o,
            SegmentBlock::JournalQuestOutcome(ref mut o) => o,
            SegmentBlock::Interaction(ref mut o) => o,
            SegmentBlock::Scene(ref mut o) => o,
            SegmentBlock::TimeManagement(ref mut o) => o,
            SegmentBlock::AddFact(ref mut o) => o,
            SegmentBlock::Reward(ref mut o) => o,
            SegmentBlock::Teleport(ref mut o) => o,
            SegmentBlock::ChangeWorld(ref mut o) => o,
            SegmentBlock::Randomize(ref mut o) => o,
        }
    }
    // ------------------------------------------------------------------------
    fn as_managed_quest_block_mut(&mut self) -> &mut dyn ManagedQuestBlock {
        match *self {
            SegmentBlock::SubSegment(ref mut o) => o,
            SegmentBlock::WaitUntil(ref mut o) => o,
            SegmentBlock::Script(ref mut o) => o,
            SegmentBlock::Layers(ref mut o) => o,
            SegmentBlock::Spawnsets(ref mut o) => o,
            SegmentBlock::JournalEntry(ref mut o) => o,
            SegmentBlock::JournalObjective(ref mut o) => o,
            SegmentBlock::JournalMappin(ref mut o) => o,
            SegmentBlock::JournalPhaseObjectives(ref mut o) => o,
            SegmentBlock::JournalQuestOutcome(ref mut o) => o,
            SegmentBlock::Interaction(ref mut o) => o,
            SegmentBlock::Scene(ref mut o) => o,
            SegmentBlock::TimeManagement(ref mut o) => o,
            SegmentBlock::AddFact(ref mut o) => o,
            SegmentBlock::Reward(ref mut o) => o,
            SegmentBlock::Teleport(ref mut o) => o,
            SegmentBlock::ChangeWorld(ref mut o) => o,
            SegmentBlock::Randomize(ref mut o) => o,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------

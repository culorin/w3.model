//
// questgraph::journals
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Model, QuestBlock)]
#[block(no_new)]
pub struct JournalEntry {
    #[model(getr)]
    id: BlockUid, // -> mapped to name
    #[model(getr)]
    guid: GUID,
    out: Links, // multiple outsockets possible, ids must match name
    #[model(getr, set)]
    notify: bool,
    #[model(getr, set)]
    include_root: bool, // only activate root journal on demand (-> shorter paths)
    #[model(getr, set)]
    reference: Option<JournalElementReference>,

    #[model(set)]
    editordata: Option<EditorBlockData>,
}
// ----------------------------------------------------------------------------
#[derive(Model, QuestBlock)]
#[block(no_new)]
pub struct JournalObjective {
    #[model(getr)]
    id: BlockUid,
    #[model(getr)]
    guid: GUID,
    out: Links,
    #[model(getr, set)]
    notify: bool,
    #[model(getr, set)]
    track: bool,
    #[model(getr, set)]
    reference: Option<JournalElementReference>,

    #[model(set)]
    editordata: Option<EditorBlockData>,
}
// ----------------------------------------------------------------------------
#[derive(Model, QuestBlock)]
#[block(no_new)]
pub struct JournalMappin {
    #[model(getr)]
    id: BlockUid,
    #[model(getr)]
    guid: GUID,
    out: Links,
    #[model(getr, set)]
    reference: Option<JournalElementReference>,

    #[model(set)]
    editordata: Option<EditorBlockData>,
}
// ----------------------------------------------------------------------------
#[derive(Model, QuestBlock)]
#[block(no_new)]
pub struct JournalPhaseObjectives {
    #[model(getr)]
    id: BlockUid,
    #[model(getr)]
    guid: GUID,
    out: Links,
    #[model(getr, set)]
    notify: bool,
    #[model(getr, set)]
    reference: Option<JournalElementReference>,

    #[model(set)]
    editordata: Option<EditorBlockData>,
}
// ----------------------------------------------------------------------------
#[derive(Model, QuestBlock)]
#[block(no_new)]
pub struct JournalQuestOutcome {
    #[model(getr)]
    id: BlockUid,
    #[model(getr)]
    guid: GUID,
    out: Links,
    #[model(getr, set)]
    notify: bool,
    #[model(getr, set)]
    reference: Option<JournalElementReference>,

    #[model(set)]
    editordata: Option<EditorBlockData>,
}
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub enum JournalElementReference {
    Character(String, String),              // character, entry
    Creature(String, String),               // creature, entry
    Quest(String, String),                  // quest, entry
    Objective(String, String, String),      // quest, phase, objective
    Mappin(String, String, String, String), // quest, phase, objective, mappin
    PhaseObjectives(String, String),        // quest, phase
    QuestOutcome(String),                   // quest
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use {Result, ValidatableElement};

use primitives::GUID;

use super::primitives::{
    BlockId, BlockName, BlockUid, EditorBlockData, InSocketId, Link, LinkTarget, Links,
    OutSocketId, SegmentId,
};
use super::{ManagedQuestBlock, QuestBlock, SegmentBlock};
// ----------------------------------------------------------------------------
impl JournalEntry {
    // ------------------------------------------------------------------------
    pub fn new(name: &str) -> JournalEntry {
        JournalEntry {
            id: BlockUid::new(BlockId::JournalEntry(name.into())),
            guid: GUID::new(),
            out: Links::default(),
            notify: true,
            include_root: false,
            reference: None,

            editordata: None,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl JournalObjective {
    // ------------------------------------------------------------------------
    pub fn new(name: &str) -> JournalObjective {
        JournalObjective {
            id: BlockUid::new(BlockId::JournalObjective(name.into())),
            guid: GUID::new(),
            out: Links::default(),
            notify: true,
            track: false,
            reference: None,

            editordata: None,
        }
    }
    // ------------------------------------------------------------------------
    pub fn include_root(&self) -> &bool {
        &false
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl JournalMappin {
    // ------------------------------------------------------------------------
    pub fn new(name: &str) -> JournalMappin {
        JournalMappin {
            id: BlockUid::new(BlockId::JournalMappin(name.into())),
            guid: GUID::new(),
            out: Links::default(),
            reference: None,

            editordata: None,
        }
    }
    // ------------------------------------------------------------------------
    pub fn include_root(&self) -> &bool {
        &false
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl JournalPhaseObjectives {
    // ------------------------------------------------------------------------
    pub fn new(name: &str) -> JournalPhaseObjectives {
        JournalPhaseObjectives {
            id: BlockUid::new(BlockId::JournalPhaseObjectives(name.into())),
            guid: GUID::new(),
            out: Links::default(),
            notify: true,
            reference: None,

            editordata: None,
        }
    }
    // ------------------------------------------------------------------------
    pub fn include_root(&self) -> &bool {
        &false
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl JournalQuestOutcome {
    // ------------------------------------------------------------------------
    pub fn new(name: &str) -> JournalQuestOutcome {
        JournalQuestOutcome {
            id: BlockUid::new(BlockId::JournalQuestOutcome(name.into())),
            guid: GUID::new(),
            out: Links::default(),
            notify: true,
            reference: None,

            editordata: None,
        }
    }
    // ------------------------------------------------------------------------
    pub fn include_root(&self) -> &bool {
        &false
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Validation
// ----------------------------------------------------------------------------
impl ValidatableElement for JournalEntry {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        match self.reference {
            Some(_) => Ok(()),
            None => Err(format!(
                "{}: block does not define a referenced \"entry\".",
                id
            )),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for JournalObjective {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        match self.reference {
            Some(_) => Ok(()),
            None => Err(format!(
                "{}: block does not define a referenced quest \"objective\".",
                id
            )),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for JournalMappin {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        match self.reference {
            Some(_) => Ok(()),
            None => Err(format!(
                "{}: block does not define a referenced quest objective \"mappin\".",
                id
            )),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for JournalPhaseObjectives {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        match self.reference {
            Some(_) => Ok(()),
            None => Err(format!(
                "{}: block does not define a referenced quest \"phase\".",
                id
            )),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for JournalQuestOutcome {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        match self.reference {
            Some(_) => Ok(()),
            None => Err(format!(
                "{}: block does not define a referenced \"quest\".",
                id
            )),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------

//
// quest block primitives
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Default, Debug)]
pub struct EditorBlockData {
    pub pos: (f32, f32),
    pub hint: Option<String>,
    pub in_sockets: Vec<InSocketId>,
    pub out_sockets: Vec<OutSocketId>,
}
// ----------------------------------------------------------------------------
#[derive(Default, Clone)]
pub struct EditorSegmentData {
    pub pos: (f32, f32),
    pub zoom: f32,
}
// ----------------------------------------------------------------------------
#[derive(Eq, PartialEq, Hash, Clone, PartialOrd, Ord, Debug)]
/// unique id for a quest segment/quest phase
pub struct SegmentId(String); // TODO add segment group?  -> <group>:segment:

#[derive(Clone, Eq, PartialEq, Hash, PartialOrd, Ord, Debug)]
pub struct BlockName(String);

#[derive(Default, Clone, Eq, PartialEq, Hash)]
pub struct BlockUid(SegmentId, BlockId, bool);
#[derive(Clone, Eq, PartialEq, Hash, PartialOrd, Ord)]
pub struct InSocketId(pub String);
#[derive(Clone, Eq, PartialEq, Hash, PartialOrd, Ord)]
pub struct OutSocketId(pub String);
// ----------------------------------------------------------------------------
#[derive(Clone, Eq, PartialEq, Hash, Ord, PartialOrd, Debug)]
/// Available blocks
pub enum BlockId {
    SubSegment(BlockName),
    QuestStart(BlockName),
    QuestEnd(BlockName),

    SegmentIn(BlockName),
    SegmentOut(BlockName),

    Scene(BlockName),
    Interaction(BlockName),

    WaitUntil(BlockName),
    Script(BlockName),
    Layers(BlockName),
    Spawnsets(BlockName),
    Spawn(BlockName),
    Despawn(BlockName),
    JournalEntry(BlockName),
    JournalObjective(BlockName),
    JournalMappin(BlockName),
    JournalPhaseObjectives(BlockName),
    JournalQuestOutcome(BlockName),

    PauseTime(BlockName),
    UnpauseTime(BlockName),
    ShiftTime(BlockName),
    SetTime(BlockName),

    AddFact(BlockName),
    Reward(BlockName),
    Teleport(BlockName),
    ChangeWorld(BlockName),
    Randomize(BlockName),
    // this is a workaround for easier questblock construction in proc macro
    Invalid,
}
// ----------------------------------------------------------------------------
#[derive(Clone, Eq, PartialEq)]
/// Reduced set of linkable blocks
pub enum LinkTarget {
    SubSegment(BlockName),
    Scene(BlockName),
    Interaction(BlockName),
    WaitUntil(BlockName),
    Script(BlockName),
    Layers(BlockName),
    Spawnsets(BlockName),
    Spawn(BlockName),
    Despawn(BlockName),
    QuestEnd(BlockName),
    JournalEntry(BlockName),
    JournalObjective(BlockName),
    JournalMappin(BlockName),
    JournalPhaseObjectives(BlockName),
    JournalQuestOutcome(BlockName),

    PauseTime(BlockName),
    UnpauseTime(BlockName),
    ShiftTime(BlockName),
    SetTime(BlockName),

    AddFact(BlockName),
    Reward(BlockName),
    Teleport(BlockName),
    ChangeWorld(BlockName),
    Randomize(BlockName),

    SegmentOut(BlockName),

    // internal marker for deadend link
    DeadEndMarker,
}
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub struct Link(SegmentId, LinkTarget, InSocketId);
// ----------------------------------------------------------------------------
#[derive(Clone, Default)]
pub struct Links(BTreeMap<OutSocketId, Vec<Link>>);
// ----------------------------------------------------------------------------
pub fn has_static_sockets(block: &BlockId) -> bool {
    use self::BlockId::*;

    matches!(*block,
        QuestStart(_)
        | SegmentIn(_)
        | QuestEnd(_)
        | SegmentOut(_)
        | JournalEntry(_)
        | JournalObjective(_)
        | JournalMappin(_)
        | JournalPhaseObjectives(_)
        | JournalQuestOutcome(_))
}
// ----------------------------------------------------------------------------
pub fn get_default_sockets(block: &BlockId) -> (Vec<InSocketId>, Vec<OutSocketId>) {
    match *block {
        BlockId::QuestStart(_) | BlockId::SegmentIn(_) => (vec![], vec![" ".into()]),

        BlockId::QuestEnd(_) | BlockId::SegmentOut(_) => (vec![" ".into()], vec![]),

        BlockId::SubSegment(_) => (vec![], vec![]),

        BlockId::JournalEntry(_) => (vec!["Activate".into()], vec![OutSocketId::default()]),

        BlockId::JournalObjective(_) => (
            vec![
                "Activate".into(),
                "Deactivate".into(),
                "Success".into(),
                "Failure".into(),
            ],
            vec![OutSocketId::default()],
        ),

        BlockId::JournalPhaseObjectives(_) => (
            vec![
                "Deactivate".into(),
                "Success".into(),
                "Failure".into(),
            ],
            vec![OutSocketId::default()],
        ),

        BlockId::JournalQuestOutcome(_) => (
            vec!["Success".into(), "Failure".into()],
            vec![OutSocketId::default()],
        ),

        BlockId::JournalMappin(_) => (
            vec!["Enable".into(), "Disable".into()],
            vec![OutSocketId::default()],
        ),

        BlockId::Scene(_) | BlockId::Interaction(_) => {
            (vec!["Input".into()], vec!["Output".into()])
        }

        BlockId::Randomize(_) => {
            (vec![InSocketId::default()], vec!["Out1".into(), "Out2".into()])
        }

        BlockId::Invalid => panic!("found invalid block id!"),

        _ => (vec![InSocketId::default()], vec![OutSocketId::default()]),
    }
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::collections::BTreeMap;
// ----------------------------------------------------------------------------
impl SegmentId {
    // ------------------------------------------------------------------------
    #[inline]
    pub fn is_quest_root(&self) -> bool {
        self.0 == "quest"
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl BlockUid {
    // ------------------------------------------------------------------------
    pub fn new(id: BlockId) -> BlockUid {
        BlockUid(SegmentId::default(), id, false)
    }
    // ------------------------------------------------------------------------
    pub fn new_with_default(id: BlockId, is_default: bool) -> BlockUid {
        BlockUid(SegmentId::default(), id, is_default)
    }
    // ------------------------------------------------------------------------
    pub fn segmentid(&self) -> &SegmentId {
        &self.0
    }
    // ------------------------------------------------------------------------
    pub fn blockid(&self) -> &BlockId {
        &self.1
    }
    // ------------------------------------------------------------------------
    pub fn blockname(&self) -> &BlockName {
        self.1.name()
    }
    // ------------------------------------------------------------------------
    pub(super) fn set_blockname(&mut self, name: BlockName) {
        self.1.set_name(name);
    }
    // ------------------------------------------------------------------------
    pub fn is_default(&self) -> bool {
        self.2
    }
    // ------------------------------------------------------------------------
    pub fn set_segmentid(&mut self, id: &SegmentId) {
        self.0 = id.to_owned()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl BlockId {
    // ------------------------------------------------------------------------
    pub fn cloned_with_name<B: Into<BlockName>>(&self, newname: B) -> BlockId {
        let mut new = self.clone();
        new.set_name(newname.into());
        new
    }
    // ------------------------------------------------------------------------
    pub fn name(&self) -> &BlockName {
        match *self {
            BlockId::QuestStart(ref name)
            | BlockId::QuestEnd(ref name)
            | BlockId::SegmentIn(ref name)
            | BlockId::SegmentOut(ref name)
            | BlockId::SubSegment(ref name)
            | BlockId::Scene(ref name)
            | BlockId::Interaction(ref name)
            | BlockId::WaitUntil(ref name)
            | BlockId::Script(ref name)
            | BlockId::Layers(ref name)
            | BlockId::Spawnsets(ref name)
            | BlockId::Spawn(ref name)
            | BlockId::Despawn(ref name)
            | BlockId::JournalEntry(ref name)
            | BlockId::JournalObjective(ref name)
            | BlockId::JournalMappin(ref name)
            | BlockId::JournalPhaseObjectives(ref name)
            | BlockId::JournalQuestOutcome(ref name)
            | BlockId::PauseTime(ref name)
            | BlockId::UnpauseTime(ref name)
            | BlockId::ShiftTime(ref name)
            | BlockId::SetTime(ref name)
            | BlockId::AddFact(ref name)
            | BlockId::Reward(ref name)
            | BlockId::Teleport(ref name)
            | BlockId::ChangeWorld(ref name)
            | BlockId::Randomize(ref name) => name,

            BlockId::Invalid => panic!("found invalid block id!"),
        }
    }
    // ------------------------------------------------------------------------
    pub fn set_name(&mut self, new_name: BlockName) {
        match *self {
            BlockId::QuestStart(ref mut name)
            | BlockId::QuestEnd(ref mut name)
            | BlockId::SegmentIn(ref mut name)
            | BlockId::SegmentOut(ref mut name)
            | BlockId::SubSegment(ref mut name)
            | BlockId::Scene(ref mut name)
            | BlockId::Interaction(ref mut name)
            | BlockId::WaitUntil(ref mut name)
            | BlockId::Script(ref mut name)
            | BlockId::Layers(ref mut name)
            | BlockId::Spawnsets(ref mut name)
            | BlockId::Spawn(ref mut name)
            | BlockId::Despawn(ref mut name)
            | BlockId::JournalEntry(ref mut name)
            | BlockId::JournalObjective(ref mut name)
            | BlockId::JournalMappin(ref mut name)
            | BlockId::JournalPhaseObjectives(ref mut name)
            | BlockId::JournalQuestOutcome(ref mut name)
            | BlockId::PauseTime(ref mut name)
            | BlockId::UnpauseTime(ref mut name)
            | BlockId::ShiftTime(ref mut name)
            | BlockId::SetTime(ref mut name)
            | BlockId::AddFact(ref mut name)
            | BlockId::Reward(ref mut name)
            | BlockId::Teleport(ref mut name)
            | BlockId::ChangeWorld(ref mut name)
            | BlockId::Randomize(ref mut name) => *name = new_name,

            BlockId::Invalid => panic!("found invalid block id!"),
        }
    }
    // ------------------------------------------------------------------------
    pub fn type_caption(&self) -> &str {
        match *self {
            BlockId::QuestStart(_) => "Quest Start",
            BlockId::QuestEnd(_) => "Quest End",
            BlockId::SegmentIn(_) => "Segment In",
            BlockId::SegmentOut(_) => "Segment Out",
            BlockId::SubSegment(_) => "Sub-Segment",
            BlockId::Scene(_) => "Scene",
            BlockId::Interaction(_) => "Interaction",
            BlockId::WaitUntil(_) => "Wait-Until",
            BlockId::Script(_) => "Script",
            BlockId::Layers(_) => "Layers",
            BlockId::Spawnsets(_) => "Spawnsets",
            BlockId::Spawn(_) => "Spawn",
            BlockId::Despawn(_) => "Despawn",
            BlockId::JournalEntry(_) => "Journal-Entry",
            BlockId::JournalObjective(_) => "Quest-Objective",
            BlockId::JournalMappin(_) => "Quest-Mappin",
            BlockId::JournalPhaseObjectives(_) => "Quest-Phase",
            BlockId::JournalQuestOutcome(_) => "Quest-Outcome",
            BlockId::PauseTime(_) => "Pause-Time",
            BlockId::UnpauseTime(_) => "Unpause-Time",
            BlockId::ShiftTime(_) => "Shift-Time",
            BlockId::SetTime(_) => "Set-Time",
            BlockId::AddFact(_) => "Add-Fact",
            BlockId::Reward(_) => "Reward",
            BlockId::Teleport(_) => "Teleport",
            BlockId::ChangeWorld(_) => "Change-World",
            BlockId::Randomize(_) => "Randomize",
            BlockId::Invalid => "-invalid-",
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl BlockName {
    // ------------------------------------------------------------------------
    pub fn as_str(&self) -> &str {
        &self.0
    }
    // ------------------------------------------------------------------------
    pub fn caption(&self) -> String {
        self.0.replace('_', " ")
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Link {
    // ------------------------------------------------------------------------
    pub fn new(
        segment_id: Option<SegmentId>,
        block: LinkTarget,
        in_socket: Option<InSocketId>,
    ) -> Link {
        // overwrite the socket for some blocks
        let in_socket = match block {
            LinkTarget::QuestEnd(_) | LinkTarget::SegmentOut(_) => " ".into(),
            // some defaults
            LinkTarget::JournalEntry(_)
            | LinkTarget::JournalObjective(_)
            | LinkTarget::JournalPhaseObjectives(_) => {
                in_socket.unwrap_or_else(|| "Activate".into())
            }
            LinkTarget::JournalQuestOutcome(_) => in_socket.unwrap_or_else(|| "Success".into()),
            LinkTarget::JournalMappin(_) => in_socket.unwrap_or_else(|| "Enable".into()),
            // scenes
            LinkTarget::Scene(_) | LinkTarget::Interaction(_) => {
                in_socket.unwrap_or_else(|| "Input".into())
            }
            _ => in_socket.unwrap_or_else(|| "In".into()),
        };

        Link(segment_id.unwrap_or_default(), block, in_socket)
    }
    // ------------------------------------------------------------------------
    pub fn segmentid(&self) -> &SegmentId {
        &self.0
    }
    // ------------------------------------------------------------------------
    pub fn target_block(&self) -> &LinkTarget {
        &self.1
    }
    // ------------------------------------------------------------------------
    pub fn target_socket(&self) -> &InSocketId {
        &self.2
    }
    // ------------------------------------------------------------------------
    pub fn set_segmentid(&mut self, id: &SegmentId) -> &Self {
        self.0 = id.to_owned();
        self
    }
    // ------------------------------------------------------------------------
    pub(super) fn set_target_blockname(&mut self, new: BlockName) {
        self.1.set_name(new);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl LinkTarget {
    // ------------------------------------------------------------------------
    fn set_name(&mut self, new_name: BlockName) {
        match *self {
            LinkTarget::SubSegment(ref mut name)
            | LinkTarget::Scene(ref mut name)
            | LinkTarget::Interaction(ref mut name)
            | LinkTarget::WaitUntil(ref mut name)
            | LinkTarget::Script(ref mut name)
            | LinkTarget::Layers(ref mut name)
            | LinkTarget::Spawnsets(ref mut name)
            | LinkTarget::Spawn(ref mut name)
            | LinkTarget::Despawn(ref mut name)
            | LinkTarget::QuestEnd(ref mut name)
            | LinkTarget::JournalEntry(ref mut name)
            | LinkTarget::JournalObjective(ref mut name)
            | LinkTarget::JournalMappin(ref mut name)
            | LinkTarget::JournalPhaseObjectives(ref mut name)
            | LinkTarget::JournalQuestOutcome(ref mut name)
            | LinkTarget::PauseTime(ref mut name)
            | LinkTarget::UnpauseTime(ref mut name)
            | LinkTarget::ShiftTime(ref mut name)
            | LinkTarget::SetTime(ref mut name)
            | LinkTarget::AddFact(ref mut name)
            | LinkTarget::Reward(ref mut name)
            | LinkTarget::Teleport(ref mut name)
            | LinkTarget::ChangeWorld(ref mut name)
            | LinkTarget::Randomize(ref mut name)
            | LinkTarget::SegmentOut(ref mut name) => *name = new_name,

            LinkTarget::DeadEndMarker => {}
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Links {
    // ------------------------------------------------------------------------
    pub fn set_segmentid(&mut self, id: &SegmentId) {
        for socket in self.values_mut() {
            for link in socket.iter_mut() {
                link.set_segmentid(id);
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl InSocketId {
    pub fn is_default(&self) -> bool {
        &self.0 == "In"
    }
}
// ----------------------------------------------------------------------------
impl OutSocketId {
    pub fn is_default(&self) -> bool {
        &self.0 == "Out"
    }
}
// ----------------------------------------------------------------------------
// Default impl
// ----------------------------------------------------------------------------
impl Default for BlockId {
    fn default() -> BlockId {
        BlockId::Invalid
    }
}
// ----------------------------------------------------------------------------
impl Default for InSocketId {
    fn default() -> InSocketId {
        InSocketId("In".into())
    }
}
// ----------------------------------------------------------------------------
impl Default for OutSocketId {
    fn default() -> OutSocketId {
        OutSocketId("Out".into())
    }
}
// ----------------------------------------------------------------------------
impl Default for SegmentId {
    fn default() -> SegmentId {
        SegmentId("quest".into())
    }
}
// ----------------------------------------------------------------------------
// specialized comparison
// ----------------------------------------------------------------------------
impl PartialEq<LinkTarget> for BlockId {
    // ------------------------------------------------------------------------
    fn eq(&self, link: &LinkTarget) -> bool {
        format!("{}", self) == format!("{}", link)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PartialEq<str> for InSocketId {
    fn eq(&self, other: &str) -> bool {
        self.0.eq(other)
    }
}
// ----------------------------------------------------------------------------
impl PartialEq<str> for OutSocketId {
    fn eq(&self, other: &str) -> bool {
        self.0.eq(other)
    }
}
// ----------------------------------------------------------------------------
// Deref
// ----------------------------------------------------------------------------
use std::ops::Deref;
use std::ops::DerefMut;
// ----------------------------------------------------------------------------
impl Deref for BlockName {
    type Target = String;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
// ----------------------------------------------------------------------------
impl Deref for SegmentId {
    type Target = String;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
// ----------------------------------------------------------------------------
impl Deref for Links {
    type Target = BTreeMap<OutSocketId, Vec<Link>>;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
// ----------------------------------------------------------------------------
impl DerefMut for Links {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
// ----------------------------------------------------------------------------
// Converter
// ----------------------------------------------------------------------------
impl<'a> From<&'a str> for SegmentId {
    // ------------------------------------------------------------------------
    fn from(id: &str) -> SegmentId {
        SegmentId(id.to_owned())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl From<String> for BlockName {
    fn from(s: String) -> BlockName {
        BlockName(s)
    }
}
// ----------------------------------------------------------------------------
impl<'a> From<&'a str> for BlockName {
    fn from(s: &str) -> BlockName {
        BlockName(s.to_owned())
    }
}
// ----------------------------------------------------------------------------
impl<'a> From<&'a String> for BlockName {
    fn from(s: &String) -> BlockName {
        BlockName(s.to_owned())
    }
}
// ----------------------------------------------------------------------------
impl<'a> From<&'a str> for InSocketId {
    fn from(s: &str) -> InSocketId {
        InSocketId(s.to_owned())
    }
}
// ----------------------------------------------------------------------------
impl<'a> From<&'a str> for OutSocketId {
    fn from(s: &str) -> OutSocketId {
        OutSocketId(s.to_owned())
    }
}
// ----------------------------------------------------------------------------
impl<'a> From<&'a BlockId> for String {
    fn from(id: &BlockId) -> String {
        format!("{}", id)
    }
}
// ----------------------------------------------------------------------------
use std::convert::TryFrom;
// ----------------------------------------------------------------------------
impl<'a> TryFrom<&'a LinkTarget> for BlockId {
    type Error = String;
    // ------------------------------------------------------------------------
    fn try_from(target: &LinkTarget) -> Result<Self, String> {
        use self::LinkTarget::*;

        let block_id = match *target {
            SubSegment(ref name) => BlockId::SubSegment(name.clone()),
            Scene(ref name) => BlockId::Scene(name.clone()),
            Interaction(ref name) => BlockId::Interaction(name.clone()),
            WaitUntil(ref name) => BlockId::WaitUntil(name.clone()),
            Script(ref name) => BlockId::Script(name.clone()),
            Layers(ref name) => BlockId::Layers(name.clone()),
            Spawnsets(ref name) => BlockId::Spawnsets(name.clone()),
            Spawn(ref name) => BlockId::Spawn(name.clone()),
            Despawn(ref name) => BlockId::Despawn(name.clone()),
            QuestEnd(ref name) => BlockId::QuestEnd(name.clone()),
            JournalEntry(ref name) => BlockId::JournalEntry(name.clone()),
            JournalObjective(ref name) => BlockId::JournalObjective(name.clone()),
            JournalMappin(ref name) => BlockId::JournalMappin(name.clone()),
            JournalPhaseObjectives(ref name) => BlockId::JournalPhaseObjectives(name.clone()),
            JournalQuestOutcome(ref name) => BlockId::JournalQuestOutcome(name.clone()),
            PauseTime(ref name) => BlockId::PauseTime(name.clone()),
            UnpauseTime(ref name) => BlockId::UnpauseTime(name.clone()),
            ShiftTime(ref name) => BlockId::ShiftTime(name.clone()),
            SetTime(ref name) => BlockId::SetTime(name.clone()),
            AddFact(ref name) => BlockId::AddFact(name.clone()),
            Reward(ref name) => BlockId::Reward(name.clone()),
            Teleport(ref name) => BlockId::Teleport(name.clone()),
            ChangeWorld(ref name) => BlockId::ChangeWorld(name.clone()),
            Randomize(ref name) => BlockId::Randomize(name.clone()),
            SegmentOut(ref name) => BlockId::SegmentOut(name.clone()),

            DeadEndMarker => return Err("no blockid for linktarget \"deadendmarker\"".into()),
        };
        Ok(block_id)
    }
}
// ----------------------------------------------------------------------------
impl<'a> TryFrom<&'a BlockId> for LinkTarget {
    type Error = String;
    // ------------------------------------------------------------------------
    fn try_from(id: &BlockId) -> Result<Self, String> {
        use self::BlockId::*;

        let block_id = match *id {
            SubSegment(ref name) => LinkTarget::SubSegment(name.clone()),
            Scene(ref name) => LinkTarget::Scene(name.clone()),
            Interaction(ref name) => LinkTarget::Interaction(name.clone()),
            WaitUntil(ref name) => LinkTarget::WaitUntil(name.clone()),
            Script(ref name) => LinkTarget::Script(name.clone()),
            Layers(ref name) => LinkTarget::Layers(name.clone()),
            Spawnsets(ref name) => LinkTarget::Spawnsets(name.clone()),
            Spawn(ref name) => LinkTarget::Spawn(name.clone()),
            Despawn(ref name) => LinkTarget::Despawn(name.clone()),
            QuestEnd(ref name) => LinkTarget::QuestEnd(name.clone()),
            JournalEntry(ref name) => LinkTarget::JournalEntry(name.clone()),
            JournalObjective(ref name) => LinkTarget::JournalObjective(name.clone()),
            JournalMappin(ref name) => LinkTarget::JournalMappin(name.clone()),
            JournalPhaseObjectives(ref name) => LinkTarget::JournalPhaseObjectives(name.clone()),
            JournalQuestOutcome(ref name) => LinkTarget::JournalQuestOutcome(name.clone()),
            PauseTime(ref name) => LinkTarget::PauseTime(name.clone()),
            UnpauseTime(ref name) => LinkTarget::UnpauseTime(name.clone()),
            ShiftTime(ref name) => LinkTarget::ShiftTime(name.clone()),
            SetTime(ref name) => LinkTarget::SetTime(name.clone()),
            AddFact(ref name) => LinkTarget::AddFact(name.clone()),
            Reward(ref name) => LinkTarget::Reward(name.clone()),
            Teleport(ref name) => LinkTarget::Teleport(name.clone()),
            ChangeWorld(ref name) => LinkTarget::ChangeWorld(name.clone()),
            Randomize(ref name) => LinkTarget::Randomize(name.clone()),
            SegmentOut(ref name) => LinkTarget::SegmentOut(name.clone()),

            QuestStart(_) | SegmentIn(_) => {
                return Err("no linktarget for \"queststart\" or \"segmentin\"".into())
            }
            Invalid => return Err("no linktarget for \"invalid\"".into()),
        };
        Ok(block_id)
    }
}
// ----------------------------------------------------------------------------
// Display trait impl
// ----------------------------------------------------------------------------
use std::fmt;
// ----------------------------------------------------------------------------
impl fmt::Display for SegmentId {
    // ------------------------------------------------------------------------
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl fmt::Display for InSocketId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}
// ----------------------------------------------------------------------------
impl fmt::Debug for InSocketId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "insocket:{}", self.0)
    }
}
// ----------------------------------------------------------------------------
impl fmt::Display for OutSocketId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}
// ----------------------------------------------------------------------------
impl fmt::Debug for OutSocketId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "outsocket:{}", self.0)
    }
}
// ----------------------------------------------------------------------------
// is used as nodeid -> must match format of LinkTarget *exactly*
impl fmt::Display for BlockId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            BlockId::QuestStart(ref name) => write!(f, "start.{}", name.0),
            BlockId::QuestEnd(ref name) => write!(f, "end.{}", name.0),
            BlockId::SegmentIn(ref name) => write!(f, "in.{}", name.0),
            BlockId::SegmentOut(ref name) => write!(f, "out.{}", name.0),
            BlockId::SubSegment(ref name) => write!(f, "subsegment.{}", name.0),
            BlockId::Scene(ref name) => write!(f, "scene.{}", name.0),
            BlockId::Interaction(ref name) => write!(f, "interaction.{}", name.0),
            BlockId::WaitUntil(ref name) => write!(f, "waituntil.{}", name.0),
            BlockId::Script(ref name) => write!(f, "script.{}", name.0),
            BlockId::Layers(ref name) => write!(f, "changelayers.{}", name.0),
            BlockId::Spawnsets(ref name) => write!(f, "spawnsets.{}", name.0),
            BlockId::Spawn(ref name) => write!(f, "spawn.{}", name.0),
            BlockId::Despawn(ref name) => write!(f, "despawn.{}", name.0),
            BlockId::JournalEntry(ref name) => write!(f, "journal.{}", name.0),
            BlockId::JournalObjective(ref name) => write!(f, "objective.{}", name.0),
            BlockId::JournalMappin(ref name) => write!(f, "mappin.{}", name.0),
            BlockId::JournalPhaseObjectives(ref name) => write!(f, "phaseobjectives.{}", name.0),
            BlockId::JournalQuestOutcome(ref name) => write!(f, "questoutcome.{}", name.0),
            BlockId::PauseTime(ref name) => write!(f, "pausetime.{}", name.0),
            BlockId::UnpauseTime(ref name) => write!(f, "unpausetime.{}", name.0),
            BlockId::ShiftTime(ref name) => write!(f, "shifttime.{}", name.0),
            BlockId::SetTime(ref name) => write!(f, "settime.{}", name.0),
            BlockId::AddFact(ref name) => write!(f, "addfact.{}", name.0),
            BlockId::Reward(ref name) => write!(f, "reward.{}", name.0),
            BlockId::Teleport(ref name) => write!(f, "teleport.{}", name.0),
            BlockId::ChangeWorld(ref name) => write!(f, "changeworld.{}", name.0),
            BlockId::Randomize(ref name) => write!(f, "randomize.{}", name.0),

            BlockId::Invalid => panic!("found invalid block id!"),
        }
    }
}
// ----------------------------------------------------------------------------
// is used as nodeid -> must match format of BlockId *exactly*
impl fmt::Display for LinkTarget {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            LinkTarget::QuestEnd(ref name) => write!(f, "end.{}", name.0),
            LinkTarget::SegmentOut(ref name) => write!(f, "out.{}", name.0),
            LinkTarget::SubSegment(ref name) => write!(f, "subsegment.{}", name.0),
            LinkTarget::Scene(ref name) => write!(f, "scene.{}", name.0),
            LinkTarget::Interaction(ref name) => write!(f, "interaction.{}", name.0),
            LinkTarget::WaitUntil(ref name) => write!(f, "waituntil.{}", name.0),
            LinkTarget::Script(ref name) => write!(f, "script.{}", name.0),
            LinkTarget::Layers(ref name) => write!(f, "changelayers.{}", name.0),
            LinkTarget::Spawnsets(ref name) => write!(f, "spawnsets.{}", name.0),
            LinkTarget::Spawn(ref name) => write!(f, "spawn.{}", name.0),
            LinkTarget::Despawn(ref name) => write!(f, "despawn.{}", name.0),
            LinkTarget::JournalEntry(ref name) => write!(f, "journal.{}", name.0),
            LinkTarget::JournalObjective(ref name) => write!(f, "objective.{}", name.0),
            LinkTarget::JournalMappin(ref name) => write!(f, "mappin.{}", name.0),
            LinkTarget::JournalPhaseObjectives(ref name) => write!(f, "phaseobjectives.{}", name.0),
            LinkTarget::JournalQuestOutcome(ref name) => write!(f, "questoutcome.{}", name.0),
            LinkTarget::PauseTime(ref name) => write!(f, "pausetime.{}", name.0),
            LinkTarget::UnpauseTime(ref name) => write!(f, "unpausetime.{}", name.0),
            LinkTarget::ShiftTime(ref name) => write!(f, "shifttime.{}", name.0),
            LinkTarget::SetTime(ref name) => write!(f, "settime.{}", name.0),
            LinkTarget::AddFact(ref name) => write!(f, "addfact.{}", name.0),
            LinkTarget::Reward(ref name) => write!(f, "reward.{}", name.0),
            LinkTarget::Teleport(ref name) => write!(f, "teleport.{}", name.0),
            LinkTarget::ChangeWorld(ref name) => write!(f, "changeworld.{}", name.0),
            LinkTarget::Randomize(ref name) => write!(f, "randomize.{}", name.0),

            LinkTarget::DeadEndMarker => write!(f, ".done"),
        }
    }
}
// ----------------------------------------------------------------------------
impl fmt::Display for Link {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}:{}#{}", self.0, self.1, self.2)
    }
}
// ----------------------------------------------------------------------------
impl fmt::Display for BlockUid {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "segment[{}].block[{}]", self.0, self.1)
    }
}
// ----------------------------------------------------------------------------

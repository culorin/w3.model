//
// questgraph::misc
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Default, Model, QuestBlock)]
pub struct Layers {
    #[model(getr)]
    id: BlockUid,
    #[model(getr)]
    guid: GUID,
    out: Links,
    #[model(getr)]
    world: Option<String>,
    #[model(getr, set)]
    sync: Option<bool>,
    #[model(getr, set)]
    purge: Option<bool>, // sometimes true on hide
    #[model(getr)]
    show: Vec<String>, // ids of layer definition
    #[model(getr)]
    hide: Vec<String>, // ids of layer definition

    #[model(set)]
    editordata: Option<EditorBlockData>,
}
// ----------------------------------------------------------------------------
#[derive(Model, QuestBlock)]
#[block(no_new)]
pub struct Spawnsets {
    #[model(getr)]
    id: BlockUid,
    #[model(getr)]
    guid: GUID,
    out: Links,
    // world: Option<String>,
    #[model(getr, set)]
    phase: Option<String>, // required for spawning
    #[model(getr)]
    spawn: Vec<Spawnset>, // ids of community definition
    #[model(getr)]
    despawn: Vec<Despawnset>, // ids of community definition

    #[model(set)]
    editordata: Option<EditorBlockData>,
}
// ----------------------------------------------------------------------------
#[derive(Model, QuestBlock)]
#[block(no_new)]
pub struct Scene {
    #[model(getr)]
    id: BlockUid,
    #[model(getr)]
    guid: GUID,
    out: Links,
    #[model(getr, set)]
    scene: Option<SceneReference>,
    #[model(getr, set)]
    placement: Option<LayerTagReference>,
    #[model(getr, set)]
    interruptable: bool,
    #[model(getr, set)]
    fadein: bool,

    #[model(set)]
    editordata: Option<EditorBlockData>,
}
// ----------------------------------------------------------------------------
#[derive(Model, QuestBlock)]
#[block(no_new)]
pub struct Interaction {
    #[model(getr)]
    id: BlockUid,
    #[model(getr)]
    guid: GUID,
    out: Links,
    #[model(getr)]
    actor: Vec<String>,
    #[model(getr, set)]
    scene: Option<SceneReference>,
    #[model(getr, set)]
    placement: Option<LayerTagReference>,
    #[model(getr, set)]
    interruptable: bool,

    #[model(set)]
    editordata: Option<EditorBlockData>,
}
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub enum TimeOperation {
    Pause,
    Unpause,
    Set(Time),
    Shift(Time),
}
// ----------------------------------------------------------------------------
#[derive(Model, QuestBlock)]
#[block(no_new)]
pub struct TimeManagement {
    #[model(getr)]
    id: BlockUid,
    #[model(getr)]
    guid: GUID,
    out: Links,
    #[model(set, getr, getmr)]
    operation: TimeOperation,

    #[model(set)]
    editordata: Option<EditorBlockData>,
}
// ----------------------------------------------------------------------------
#[derive(Default, Model, QuestBlock)]
pub struct AddFact {
    #[model(getr)]
    id: BlockUid,
    #[model(getr)]
    guid: GUID,
    out: Links,
    #[model(getr)]
    fact: String,
    #[model(getr)]
    value: Option<i32>,

    #[model(set)]
    editordata: Option<EditorBlockData>,
}
// ----------------------------------------------------------------------------
#[derive(Default, Model, QuestBlock)]
pub struct Reward {
    #[model(getr)]
    id: BlockUid,
    #[model(getr)]
    guid: GUID,
    out: Links,
    #[model(getr, set)]
    reward: Option<RewardReference>,

    #[model(set)]
    editordata: Option<EditorBlockData>,
}
// ----------------------------------------------------------------------------
#[derive(Model, QuestBlock)]
#[block(no_new)]
pub struct Teleport {
    #[model(getr)]
    id: BlockUid,
    #[model(getr)]
    guid: GUID,
    out: Links,
    #[model(getr, set)]
    actor: String,
    #[model(getr, set)]
    destination: Option<LayerTagReference>,

    #[model(set)]
    editordata: Option<EditorBlockData>,
}
// ----------------------------------------------------------------------------
#[derive(Default, Model, QuestBlock)]
pub struct ChangeWorld {
    #[model(getr)]
    id: BlockUid,
    #[model(getr)]
    guid: GUID,
    out: Links,
    #[model(getr, set)]
    destination: Option<LayerTagReference>,

    #[model(set)]
    editordata: Option<EditorBlockData>,
}
// ----------------------------------------------------------------------------
#[derive(Default, Model, QuestBlock)]
pub struct Randomize {
    #[model(getr)]
    id: BlockUid,
    #[model(getr)]
    guid: GUID,
    out: Links,

    #[model(set)]
    editordata: Option<EditorBlockData>,
}
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub struct Spawnset {
    id: String,
}

#[derive(Clone)]
pub struct Despawnset(Spawnset);
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use primitives::references::{LayerTagReference, RewardReference, SceneReference};
use primitives::{Time, GUID};
use {Result, ValidatableElement};

use super::primitives::{
    BlockId, BlockName, BlockUid, EditorBlockData, InSocketId, Link, LinkTarget, Links,
    OutSocketId, SegmentId,
};
use super::{ManagedQuestBlock, QuestBlock, SegmentBlock};
// ----------------------------------------------------------------------------
impl Layers {
    // ------------------------------------------------------------------------
    pub fn add_layer_to_show(&mut self, layerid: &str) -> &mut Self {
        self.show.push(layerid.to_string());
        self
    }
    // ------------------------------------------------------------------------
    pub fn add_layer_to_hide(&mut self, layerid: &str) -> &mut Self {
        self.hide.push(layerid.to_string());
        self
    }
    // ------------------------------------------------------------------------
    pub fn set_layers_to_show(&mut self, layerids: Vec<String>) {
        self.show = layerids;
    }
    // ------------------------------------------------------------------------
    pub fn set_layers_to_hide(&mut self, layerids: Vec<String>) {
        self.hide = layerids;
    }
    // ------------------------------------------------------------------------
    pub fn set_world(&mut self, worldid: &str) {
        self.world = Some(worldid.to_lowercase());
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Spawnset {
    // ------------------------------------------------------------------------
    fn new(id: String) -> Spawnset {
        Spawnset { id }
    }
    // ------------------------------------------------------------------------
    pub fn id(&self) -> &str {
        &self.id
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Despawnset {
    // ------------------------------------------------------------------------
    pub fn id(&self) -> &str {
        &self.0.id
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Spawnsets {
    // ------------------------------------------------------------------------
    pub fn new(name: &str, spawn_only: Option<bool>) -> Spawnsets {
        let blockid = match spawn_only {
            Some(spawn) if spawn => BlockId::Spawn(name.into()),
            Some(_) => BlockId::Despawn(name.into()),
            None => BlockId::Spawnsets(name.into()),
        };

        Spawnsets {
            id: BlockUid::new(blockid),
            guid: GUID::new(),
            out: Links::default(),
            phase: None,
            spawn: Vec::default(),
            despawn: Vec::default(),

            editordata: None,
        }
    }
    // ------------------------------------------------------------------------
    pub fn set_spawnlist<'a>(&mut self, list: impl Iterator<Item = &'a str>) {
        self.spawn = list.map(|s| Spawnset::new(s.to_lowercase())).collect();
    }
    // ------------------------------------------------------------------------
    pub fn set_despawnlist<'a>(&mut self, list: impl Iterator<Item = &'a str>) {
        self.despawn = list
            .map(|s| Despawnset(Spawnset::new(s.to_lowercase())))
            .collect();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Scene {
    // ------------------------------------------------------------------------
    pub fn new(name: &str) -> Scene {
        Scene {
            id: BlockUid::new(BlockId::Scene(name.into())),
            guid: GUID::new(),
            out: Links::default(),
            scene: None,
            placement: None,
            interruptable: true,
            fadein: false,

            editordata: None,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Interaction {
    // ------------------------------------------------------------------------
    pub fn new(name: &str) -> Interaction {
        Interaction {
            id: BlockUid::new(BlockId::Interaction(name.into())),
            guid: GUID::new(),
            out: Links::default(),
            actor: Vec::default(),
            scene: None,
            placement: None,
            interruptable: true,

            editordata: None,
        }
    }
    // ------------------------------------------------------------------------
    pub fn set_actors(&mut self, actortags: &[String]) {
        self.actor = actortags.iter().map(|a| a.to_lowercase()).collect();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl TimeManagement {
    // ------------------------------------------------------------------------
    pub fn new(name: &str, op: TimeOperation) -> TimeManagement {
        let blockid = match op {
            TimeOperation::Pause => BlockId::PauseTime(name.into()),
            TimeOperation::Unpause => BlockId::UnpauseTime(name.into()),
            TimeOperation::Set(_) => BlockId::SetTime(name.into()),
            TimeOperation::Shift(_) => BlockId::ShiftTime(name.into()),
        };
        TimeManagement {
            id: BlockUid::new(blockid),
            guid: GUID::new(),
            out: Links::default(),
            operation: op,

            editordata: None,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl AddFact {
    // ------------------------------------------------------------------------
    pub fn set_fact(&mut self, factid: &str, value: i32) {
        self.fact = factid.to_string();
        self.value = Some(value);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Teleport {
    // ------------------------------------------------------------------------
    pub fn new(name: &str) -> Teleport {
        Teleport {
            id: BlockUid::new(BlockId::Teleport(name.into())),
            guid: GUID::new(),
            out: Links::default(),
            destination: None,
            actor: "PLAYER".to_string(),

            editordata: None,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Validation
// ----------------------------------------------------------------------------
use validator::validate_str_default as validate_str;
// ----------------------------------------------------------------------------
impl ValidatableElement for Layers {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if let Some(ref world) = self.world {
            validate_str(world, &format!("{} world", id), "^[0-9_a-z]*$", "[a-z_0-9]")?;
        } else {
            validate_err_missing!(id, "world");
        }

        for (i, layer) in self.show.iter().enumerate() {
            validate_str(
                layer,
                &format!("{} #{} layer to show", id, i + 1),
                "^[0-9_a-z]*$",
                "[a-z_0-9]",
            )?;
        }
        for (i, layer) in self.hide.iter().enumerate() {
            validate_str(
                layer,
                &format!("{} #{} layer to hide", id, i + 1),
                "^[0-9_a-z]*$",
                "[a-z_0-9]",
            )?;
        }

        if self.hide.is_empty() && self.show.is_empty() {
            return Err(format!("{} at least one layer must be defined", id));
        }

        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for Spawnsets {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if let Some(ref phase) = self.phase {
            validate_str(phase, &format!("{} phase", id), "^[0-9_a-z]*$", "[a-z_0-9]")?;
        } else if !self.spawn.is_empty() {
            validate_err_missing!(id, "phase");
        }

        let validate_reference = |spawnset: &str, id: &str| -> Result<()> {
            if spawnset.ends_with(".w2comm") {
                validate_str(spawnset, id, "^[0-9/\\\\._a-z]*$", "[a-z/._0-9]")
            } else {
                validate_str(spawnset, id, "^[0-9_a-z]*$", "[a-z_0-9]")
            }
        };

        for (i, spawnset) in self.spawn.iter().enumerate() {
            validate_reference(
                &spawnset.id,
                &format!("{} #{} community to spawn", id, i + 1),
            )?;
        }
        for (i, spawnset) in self.despawn.iter().enumerate() {
            validate_reference(
                &spawnset.0.id,
                &format!("{} #{} community to despawn", id, i + 1),
            )?;
        }

        if self.spawn.is_empty() && self.despawn.is_empty() {
            return Err(format!("{} at least one spawnset must be defined", id));
        }

        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for Scene {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        match self.scene {
            Some(ref scene) => scene.validate(&format!("{} scene", id))?,
            None => validate_err_missing!(id, "scene"),
        }

        match self.placement {
            Some(ref placement) => placement.validate(&format!("{} placement", id))?,
            None => validate_err_missing!(id, "placement"),
        }

        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for Interaction {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        // scene + actor + placement ?
        // load definition extract placement? world?
        // check if definition exists
        // placement
        //  - must be defined for Checked (will generate the tag and check in definition)
        //  - may be defined for unchecked (scenepoint will be generated but not checked -> warn?)
        //  - may be defined for file (~ " ~)
        match self.scene {
            Some(ref scene) => scene.validate(&format!("{} scene", id))?,
            None => validate_err_missing!(id, "scene"),
        }

        // placement is optional
        if let Some(ref placement) = self.placement {
            placement.validate(&format!("{} placement", id))?;
        }
        // check actor tags
        if self.actor.is_empty() {
            validate_err_missing!(id, "actor")
        } else {
            for actor in &self.actor {
                validate_str(
                    actor,
                    &format!("{} actor tag", id),
                    "^[0-9_a-zA-Z]*$",
                    "[a-z_0-9]",
                )?;
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for TimeManagement {
    // ------------------------------------------------------------------------
    fn validate(&self, _id: &str) -> Result<()> {
        // TODO implement validate TimeManagement
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for AddFact {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if !self.fact.is_empty() && self.value.is_some() {
            validate_str(
                &self.fact,
                &format!("{} factid", id),
                "^[0-9_a-zA-Z]*$",
                "[a-zA-Z_0-9]",
            )?;
            Ok(())
        } else {
            Err(format!("{}: required \"factid\" or \"value\" missing.", id))
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for Reward {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        match self.reward {
            Some(ref reward) => reward.validate(&format!("{} reward", id))?,
            None => validate_err_missing!(id, "reward"),
        }
        // TODO implement validate Reward
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for Teleport {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        match self.destination {
            Some(ref destination) => destination.validate(&format!("{} destination", id))?,
            None => validate_err_missing!(id, "destination"),
        }
        validate_str(
            &self.actor,
            &format!("{} actor", id),
            "^[0-9_a-zA-Z]*$",
            "[a-z_0-9]",
        )?;
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for ChangeWorld {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        match self.destination {
            Some(ref destination) => destination.validate(&format!("{} destination", id))?,
            None => validate_err_missing!(id, "destination"),
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for Randomize {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if self.out.keys().len() > 10 {
            Err(format!(
                "{}: max 10 sockets supported. found: {}",
                id,
                self.out.keys().len()
            ))
        } else {
            Ok(())
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------

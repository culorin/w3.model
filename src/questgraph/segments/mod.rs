//
// questgraph::segments
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
// Valid top level quest blocks
// ----------------------------------------------------------------------------
/// Top level object owning the top level segment for a quest
#[derive(Model)]
pub struct Quest {
    #[model(getr)]
    id: SegmentId,
    #[model(getr, set)]
    segment: QuestRootSegment,
}
// ----------------------------------------------------------------------------
/// Top level object owning an externalized segment
#[derive(Model)]
pub struct QuestPart {
    #[model(getr)]
    id: SegmentId,
    #[model(getr)]
    segment: QuestSegment,
}
// ----------------------------------------------------------------------------
/// object owning the top level segment with all its blocks. all contained
/// blocks will be extracted from collection.
#[derive(Model)]
pub struct QuestRootSegment {
    #[model(getr)]
    id: SegmentId,
    // source_data_removed: bool,
    source: Option<QuestStart>,
    sink: BTreeMap<BlockId, QuestEnd>,

    blocks: Blocks, // top level quest blocks

    #[model(getr, getmrd)]
    editordata: Option<EditorSegmentData>,
}
// ----------------------------------------------------------------------------
/// object owning a segment with all blocks. all contained blocks will be
/// extracted by the segmentid from collection.
#[derive(Model)]
pub struct QuestSegment {
    #[model(getr)]
    id: SegmentId,
    #[model(set)]
    context: Option<String>,
    #[model(getr, set)]
    external: bool,
    // source_data_removed: bool,
    input: BTreeMap<BlockId, SegmentIn>,
    output: BTreeMap<BlockId, SegmentOut>,

    blocks: Blocks,

    #[model(getr, getmrd)]
    editordata: Option<EditorSegmentData>,
}
// ----------------------------------------------------------------------------
#[derive(Default)]
pub struct Blocks(BTreeMap<BlockId, SegmentBlock>);
// ----------------------------------------------------------------------------
#[derive(Model, QuestInBlock)]
pub struct QuestStart {
    #[model(getr)]
    id: BlockUid, // -> mapped to InSocketId, default In
    #[model(getr)]
    guid: GUID,
    _in_socket: InSocketId, // quest start input, default is In, which is not saved
    _out_socket: OutSocketId, // only valid out socket is " "
    out: Links,

    #[model(set)]
    editordata: Option<EditorBlockData>,
}
// ----------------------------------------------------------------------------
#[derive(Model, QuestOutBlock)]
pub struct QuestEnd {
    #[model(getr)]
    id: BlockUid, // -> mapped to OutSocketId, default Out
    #[model(getr)]
    guid: GUID,
    _in_socket: InSocketId,   // only valid input socket is " "
    _out_socket: OutSocketId, // defines the signal for quest output, default Out, which is not saved

    #[model(set)]
    editordata: Option<EditorBlockData>,
}
// ----------------------------------------------------------------------------
#[derive(Default, Model, QuestBlock)]
// aka PhaseBlock
pub struct SubSegment {
    #[model(getr)]
    id: BlockUid, // -> mapped to name
    #[model(getr)]
    guid: GUID,
    out: Links, // MUST be at least 1? or auto generate out: .done?
    #[model(getr, set)]
    segmentlink: Option<SegmentId>,
    #[model(getr, set)]
    required_world: Option<String>,

    #[model(set)]
    editordata: Option<EditorBlockData>,
}
// ----------------------------------------------------------------------------
#[derive(Model, QuestInBlock)]
pub struct SegmentIn {
    #[model(getr)]
    id: BlockUid, // -> mapped to InSocketId, default In
    #[model(getr)]
    guid: GUID,
    _in_socket: InSocketId, // defines the link for questinput, default is In, which is not saved
    _out_socket: OutSocketId, // only valid out socket is " "
    out: Links,

    #[model(set)]
    editordata: Option<EditorBlockData>,
}
#[derive(Model, QuestOutBlock)]
pub struct SegmentOut {
    #[model(getr)]
    id: BlockUid, // -> mapped to OutSocketId, default Out
    #[model(getr)]
    guid: GUID,
    _in_socket: InSocketId,   // only valid input socket is " "
    _out_socket: OutSocketId, // defines the signal for quest output, default Out, which is not saved

    #[model(set)]
    editordata: Option<EditorBlockData>,
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::collections::BTreeMap;
use std::convert::TryFrom;

use primitives::GUID;

use super::primitives::{
    BlockId, BlockName, BlockUid, EditorBlockData, EditorSegmentData, InSocketId, Link, LinkTarget,
    Links, OutSocketId, SegmentId,
};
use super::{ManagedQuestBlock, QuestBlock, SegmentBlock};
use {Result, ValidatableElement};
// ----------------------------------------------------------------------------
// Quest-Root
// ----------------------------------------------------------------------------
impl Default for Quest {
    // ------------------------------------------------------------------------
    fn default() -> Quest {
        Quest {
            id: SegmentId::from("root:quest"),
            segment: QuestRootSegment::default(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Default for QuestRootSegment {
    // ------------------------------------------------------------------------
    fn default() -> QuestRootSegment {
        QuestRootSegment {
            id: "seg:quest".into(),

            source: None,
            sink: BTreeMap::new(),

            blocks: Blocks::default(),

            editordata: None,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl QuestRootSegment {
    // ------------------------------------------------------------------------
    pub fn set_start(&mut self, mut start: QuestStart) -> Result<()> {
        // stamp segmentid
        start.set_segmentid(&self.id);

        if self.source.is_none() {
            self.source = Some(start);
            Ok(())
        } else {
            Err("only one quest start definition allowed.".into())
        }
    }
    // ------------------------------------------------------------------------
    pub fn add_end(&mut self, mut end: QuestEnd) -> Result<()> {
        // stamp segmentid
        end.set_segmentid(&self.id);

        if self.sink.contains_key(end.id.blockid()) {
            Err(format!(
                "found duplicate output block in segment [{}]: {}",
                self.id,
                end.id.blockid()
            ))
        } else {
            self.sink.insert(end.id.blockid().clone(), end);
            Ok(())
        }
    }
    // ------------------------------------------------------------------------
    pub fn add_block<T: QuestBlock + Into<SegmentBlock>>(&mut self, mut block: T) -> Result<()> {
        // stamp segmentid
        block.set_segmentid(&self.id);

        self.blocks
            .add_block(block.uid().blockid().clone(), block.into())
    }
    // ------------------------------------------------------------------------
    pub fn start_block(&self, id: &BlockId) -> Option<&QuestStart> {
        self.source.as_ref().filter(|s| s.uid().blockid() == id)
    }
    // ------------------------------------------------------------------------
    pub fn segment_block(&self, id: &BlockId) -> Option<&SegmentBlock> {
        self.blocks.get(id)
    }
    // ------------------------------------------------------------------------
    pub fn end_block(&self, id: &BlockId) -> Option<&QuestEnd> {
        self.sink.get(id)
    }
    // ------------------------------------------------------------------------
    pub fn start_block_mut(&mut self, id: &BlockId) -> Option<&mut QuestStart> {
        self.source.as_mut().filter(|s| s.uid().blockid() == id)
    }
    // ------------------------------------------------------------------------
    pub fn segment_block_mut(&mut self, id: &BlockId) -> Option<&mut SegmentBlock> {
        self.blocks.get_mut(id)
    }
    // ------------------------------------------------------------------------
    pub fn end_block_mut(&mut self, id: &BlockId) -> Option<&mut QuestEnd> {
        self.sink.get_mut(id)
    }
    // ------------------------------------------------------------------------
    pub fn start_blocks(&self) -> impl Iterator<Item = &QuestStart> {
        self.source.iter()
    }
    // ------------------------------------------------------------------------
    pub fn segment_blocks(&self) -> impl Iterator<Item = &SegmentBlock> {
        self.blocks.values()
    }
    // ------------------------------------------------------------------------
    pub fn end_blocks(&self) -> impl Iterator<Item = &QuestEnd> {
        self.sink.values()
    }
    // ------------------------------------------------------------------------
    pub fn start_blocks_mut(&mut self) -> impl Iterator<Item = &mut QuestStart> {
        self.source.iter_mut()
    }
    // ------------------------------------------------------------------------
    pub fn segment_blocks_mut(&mut self) -> impl Iterator<Item = &mut SegmentBlock> {
        self.blocks.values_mut()
    }
    // ------------------------------------------------------------------------
    pub fn end_blocks_mut(&mut self) -> impl Iterator<Item = &mut QuestEnd> {
        self.sink.values_mut()
    }
    // ------------------------------------------------------------------------
    pub fn quest_blocks(&self) -> impl Iterator<Item = &dyn QuestBlock> {
        self.source
            .iter()
            .map(|block| block as &dyn QuestBlock)
            .chain(self.blocks.values().map(|block| block as &dyn QuestBlock))
            .chain(self.sink.values().map(|block| block as &dyn QuestBlock))
    }
    // ------------------------------------------------------------------------
    fn managed_blocks_mut(&mut self) -> impl Iterator<Item = &mut dyn ManagedQuestBlock> {
        self.source
            .iter_mut()
            .map(|block| block as &mut dyn ManagedQuestBlock)
            .chain(
                self.blocks
                    .values_mut()
                    .map(|block| block as &mut dyn ManagedQuestBlock),
            )
    }
    // ------------------------------------------------------------------------
    pub fn rename_block<T: Into<BlockName>>(&mut self, id: &BlockId, new_name: T) -> Result<()> {
        let new_name = new_name.into();
        let new_blockid = id.cloned_with_name(new_name.clone());

        if let Some(mut block) = self.sink.remove(id) {
            block.rename(new_name.clone());
            self.sink.insert(new_blockid, block);
        } else if let Some(mut block) = self.blocks.remove(id) {
            block.rename(new_name.clone());
            self.blocks.add_block(new_blockid, block)?;
        } else if self.source.iter().any(|block| block.uid().blockid() == id) {
            // quest start cannot be renamed
            return Err(String::from("quest start block cannot be renamed."));
        } else {
            return Err(format!(
                "block [{}] not found in segment [{}]. rename failed.",
                id, self.id
            ));
        }

        // rename all link targets pointing to this block
        let target = LinkTarget::try_from(id)?;
        self.managed_blocks_mut()
            .for_each(|block| block.update_linktarget_blockname(&target, &new_name));

        Ok(())
    }
    // ------------------------------------------------------------------------
    pub fn delete_block(&mut self, id: &BlockId) -> Result<()> {
        if self.source.iter().any(|block| block.uid().blockid() == id) {
            // quest start cannot be removed
            return Err(String::from("quest start block cannot be deleted."));
        }
        if self.sink.values().any(|block| block.uid().blockid() == id) {
            // quest end cannot be removed as long as it's not possible to add end-blocks
            return Err(String::from("quest end block cannot be deleted."));
        }

        if self.blocks.remove(id).is_none() && self.sink.remove(id).is_none() {
            return Err(format!(
                "block [{}] not found in segment [{}]. delete failed.",
                id, self.id
            ));
        }

        // delete all link targets pointing to this block
        // Note: segment in has no links so a linktarget cannot be generated!
        if let Ok(target) = LinkTarget::try_from(id) {
            self.managed_blocks_mut()
                .for_each(|block| block.prune_links(&target, &[]));
        }

        Ok(())
    }
    // ------------------------------------------------------------------------
    pub fn prune_links(&mut self, target_block: &BlockId, valid_sockets: &[InSocketId]) {
        if let Ok(target) = LinkTarget::try_from(target_block) {
            self.managed_blocks_mut()
                .for_each(|block| block.prune_links(&target, valid_sockets));
        }
    }
    // ------------------------------------------------------------------------
    pub fn change_link_targets(
        &mut self,
        old_target_block: &BlockId,
        old_socket: &InSocketId,
        new_target_block: &BlockId,
        new_socket: &InSocketId,
    ) {
        let old_target = LinkTarget::try_from(old_target_block);
        let new_target = LinkTarget::try_from(new_target_block);

        if let (Ok(old_target), Ok(new_target)) = (old_target, new_target) {
            self.managed_blocks_mut().for_each(|block| {
                block.change_link_target(&old_target, old_socket, &new_target, new_socket)
            })
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Quest-Subsegments
// ----------------------------------------------------------------------------
impl QuestPart {
    // ------------------------------------------------------------------------
    pub fn new(segment: QuestSegment) -> QuestPart {
        QuestPart {
            id: SegmentId::from(format!("cont:{}", &segment.id).as_str()),
            segment,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl QuestSegment {
    // ------------------------------------------------------------------------
    pub fn new<I: Into<SegmentId>>(id: I) -> QuestSegment {
        QuestSegment {
            id: id.into(),
            context: None,
            external: false,

            input: BTreeMap::new(),
            output: BTreeMap::new(),

            blocks: Blocks::default(),

            editordata: None,
        }
    }
    // ------------------------------------------------------------------------
    pub fn context(&self) -> &str {
        self.context.as_deref().unwrap_or("")
    }
    // ------------------------------------------------------------------------
    pub fn add_input(&mut self, mut input: SegmentIn) -> Result<()> {
        // stamp segmentid
        input.set_segmentid(&self.id);

        if self.input.contains_key(input.id.blockid()) {
            Err(format!(
                "found duplicate input block in segment [{}]: {}",
                self.id,
                input.id.blockid()
            ))
        } else {
            self.input.insert(input.id.blockid().clone(), input);
            Ok(())
        }
    }
    // ------------------------------------------------------------------------
    pub fn add_output(&mut self, mut output: SegmentOut) -> Result<()> {
        // stamp segmentid
        output.set_segmentid(&self.id);

        if self.output.contains_key(output.id.blockid()) {
            Err(format!(
                "found duplicate output block in segment [{}]: {}",
                self.id,
                output.id.blockid()
            ))
        } else {
            self.output.insert(output.id.blockid().clone(), output);
            Ok(())
        }
    }
    // ------------------------------------------------------------------------
    pub fn add_block<T: QuestBlock + Into<SegmentBlock>>(&mut self, mut block: T) -> Result<()> {
        // stamp segmentid
        block.set_segmentid(&self.id);

        self.blocks
            .add_block(block.uid().blockid().clone(), block.into())
    }
    // ------------------------------------------------------------------------
    pub fn in_block(&self, id: &BlockId) -> Option<&SegmentIn> {
        self.input.get(id)
    }
    // ------------------------------------------------------------------------
    pub fn segment_block(&self, id: &BlockId) -> Option<&SegmentBlock> {
        self.blocks.get(id)
    }
    // ------------------------------------------------------------------------
    pub fn out_block(&self, id: &BlockId) -> Option<&SegmentOut> {
        self.output.get(id)
    }
    // ------------------------------------------------------------------------
    pub fn in_block_mut(&mut self, id: &BlockId) -> Option<&mut SegmentIn> {
        self.input.get_mut(id)
    }
    // ------------------------------------------------------------------------
    pub fn segment_block_mut(&mut self, id: &BlockId) -> Option<&mut SegmentBlock> {
        self.blocks.get_mut(id)
    }
    // ------------------------------------------------------------------------
    pub fn out_block_mut(&mut self, id: &BlockId) -> Option<&mut SegmentOut> {
        self.output.get_mut(id)
    }
    // ------------------------------------------------------------------------
    pub fn in_blocks(&self) -> impl Iterator<Item = &SegmentIn> {
        self.input.values()
    }
    // ------------------------------------------------------------------------
    pub fn segment_blocks(&self) -> impl Iterator<Item = &SegmentBlock> {
        self.blocks.values()
    }
    // ------------------------------------------------------------------------
    pub fn out_blocks(&self) -> impl Iterator<Item = &SegmentOut> {
        self.output.values()
    }
    // ------------------------------------------------------------------------
    pub fn in_blocks_mut(&mut self) -> impl Iterator<Item = &mut SegmentIn> {
        self.input.values_mut()
    }
    // ------------------------------------------------------------------------
    pub fn segment_blocks_mut(&mut self) -> impl Iterator<Item = &mut SegmentBlock> {
        self.blocks.values_mut()
    }
    // ------------------------------------------------------------------------
    pub fn out_blocks_mut(&mut self) -> impl Iterator<Item = &mut SegmentOut> {
        self.output.values_mut()
    }
    // ------------------------------------------------------------------------
    pub fn quest_blocks(&self) -> impl Iterator<Item = &dyn QuestBlock> {
        self.input
            .values()
            .map(|block| block as &dyn QuestBlock)
            .chain(self.blocks.values().map(|block| block as &dyn QuestBlock))
            .chain(self.output.values().map(|block| block as &dyn QuestBlock))
    }
    // ------------------------------------------------------------------------
    pub fn in_out_block_count(&self) -> (usize, usize) {
        (self.input.len(), self.output.len())
    }
    // ------------------------------------------------------------------------
    pub fn in_out_ids(&self) -> (Vec<InSocketId>, Vec<OutSocketId>) {
        let in_ids = self
            .input
            .keys()
            .map(|id| InSocketId::from(id.name().as_str()))
            .collect();
        let out_ids = self
            .output
            .keys()
            .map(|id| OutSocketId::from(id.name().as_str()))
            .collect();

        (in_ids, out_ids)
    }
    // ------------------------------------------------------------------------
    fn managed_blocks_mut(&mut self) -> impl Iterator<Item = &mut dyn ManagedQuestBlock> {
        self.input
            .values_mut()
            .map(|block| block as &mut dyn ManagedQuestBlock)
            .chain(
                self.blocks
                    .values_mut()
                    .map(|block| block as &mut dyn ManagedQuestBlock),
            )
    }
    // ------------------------------------------------------------------------
    pub fn rename_block<T: Into<BlockName>>(&mut self, id: &BlockId, new_name: T) -> Result<()> {
        let new_name = new_name.into();
        let new_blockid = id.cloned_with_name(new_name.clone());

        if let Some(mut block) = self.input.remove(id) {
            block.rename(new_name.clone());
            self.input.insert(new_blockid, block);
        } else if let Some(mut block) = self.blocks.remove(id) {
            block.rename(new_name.clone());
            self.blocks.add_block(new_blockid, block)?;
        } else if let Some(mut block) = self.output.remove(id) {
            block.rename(new_name.clone());
            self.output.insert(new_blockid, block);
        } else {
            return Err(format!(
                "block [{}] not found in segment [{}]. rename failed.",
                id, self.id
            ));
        }

        // rename all link targets pointing to this block
        // Note: segment in has no links so a linktarget cannot be generated!
        if let Ok(target) = LinkTarget::try_from(id) {
            self.managed_blocks_mut()
                .for_each(|block| block.update_linktarget_blockname(&target, &new_name));
        }

        Ok(())
    }

    // ------------------------------------------------------------------------
    pub fn delete_block(&mut self, id: &BlockId) -> Result<()> {
        if self.input.len() == 1 && self.input.contains_key(id) {
            return Err(String::from("last segment-in block cannot be deleted."));
        }

        if self.blocks.remove(id).is_none()
            && self.input.remove(id).is_none()
            && self.output.remove(id).is_none()
        {
            return Err(format!(
                "block [{}] not found in segment [{}]. delete failed.",
                id, self.id
            ));
        }

        // delete all link targets pointing to this block
        // Note: segment in has no links so a linktarget cannot be generated!
        if let Ok(target) = LinkTarget::try_from(id) {
            self.managed_blocks_mut()
                .for_each(|block| block.prune_links(&target, &[]));
        }

        Ok(())
    }
    // ------------------------------------------------------------------------
    pub fn prune_links(&mut self, target_block: &BlockId, valid_sockets: &[InSocketId]) {
        if let Ok(target) = LinkTarget::try_from(target_block) {
            self.managed_blocks_mut()
                .for_each(|block| block.prune_links(&target, valid_sockets));
        }
    }
    // ------------------------------------------------------------------------
    pub fn change_link_targets(
        &mut self,
        old_target_block: &BlockId,
        old_socket: &InSocketId,
        new_target_block: &BlockId,
        new_socket: &InSocketId,
    ) {
        let old_target = LinkTarget::try_from(old_target_block);
        let new_target = LinkTarget::try_from(new_target_block);

        if let (Ok(old_target), Ok(new_target)) = (old_target, new_target) {
            self.managed_blocks_mut().for_each(|block| {
                block.change_link_target(&old_target, old_socket, &new_target, new_socket)
            })
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Blocks {
    // ------------------------------------------------------------------------
    #[allow(clippy::map_entry)]
    pub fn add_block(&mut self, id: BlockId, block: SegmentBlock) -> Result<()> {
        if self.0.contains_key(&id) {
            Err(format!("found duplicate blockid: {}", id))
        } else {
            self.0.insert(id, block);
            Ok(())
        }
    }
    // ------------------------------------------------------------------------
    fn remove(&mut self, id: &BlockId) -> Option<SegmentBlock> {
        self.0.remove(id)
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn get(&self, id: &BlockId) -> Option<&SegmentBlock> {
        self.0.get(id)
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn get_mut(&mut self, id: &BlockId) -> Option<&mut SegmentBlock> {
        self.0.get_mut(id)
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn values(&self) -> impl Iterator<Item = &SegmentBlock> {
        self.0.values()
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn values_mut(&mut self) -> impl Iterator<Item = &mut SegmentBlock> {
        self.0.values_mut()
    }
    // ------------------------------------------------------------------------
    pub fn subsegment_ids(&self) -> Vec<SegmentId> {
        self.0
            .values()
            .filter_map(|block| match *block {
                SegmentBlock::SubSegment(ref segment) => Some(segment),
                _ => None,
            })
            .filter_map(|segment| segment.segmentlink.as_ref().cloned())
            .collect()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl QuestStart {
    // ------------------------------------------------------------------------
    pub fn new(name: &str) -> QuestStart {
        // default name: In
        QuestStart {
            id: BlockUid::new_with_default(BlockId::QuestStart(name.into()), name == "In"),
            guid: GUID::new(),
            _in_socket: InSocketId(name.into()),
            _out_socket: OutSocketId(" ".into()), // the only valid output socket
            out: Links::default(),

            editordata: None,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl QuestEnd {
    // ------------------------------------------------------------------------
    pub fn new(name: &str) -> QuestEnd {
        // default name: Out
        QuestEnd {
            id: BlockUid::new_with_default(BlockId::QuestEnd(name.into()), name == "Out"),
            guid: GUID::new(),
            _in_socket: InSocketId(" ".into()),
            _out_socket: OutSocketId(name.into()),

            editordata: None,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl SegmentIn {
    // ------------------------------------------------------------------------
    pub fn new(name: &str) -> SegmentIn {
        // default name: In
        SegmentIn {
            id: BlockUid::new_with_default(BlockId::SegmentIn(name.into()), name == "In"),
            guid: GUID::new(),
            _in_socket: InSocketId(name.into()),
            _out_socket: OutSocketId(" ".into()), // the only valid output socket
            out: Links::default(),

            editordata: None,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl SegmentOut {
    // ------------------------------------------------------------------------
    pub fn new(name: &str) -> SegmentOut {
        // default name: Out
        SegmentOut {
            id: BlockUid::new_with_default(BlockId::SegmentOut(name.into()), name == "Out"),
            guid: GUID::new(),
            _in_socket: InSocketId(" ".into()),
            _out_socket: OutSocketId(name.into()),

            editordata: None,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl SubSegment {
    // ------------------------------------------------------------------------
    pub fn rename_out_socket(&mut self, old: &OutSocketId, new: &OutSocketId) {
        if let Some(links) = self.out.remove(old) {
            self.out.insert(new.clone(), links);
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Validation
// ----------------------------------------------------------------------------
use validator::validate_str_default;
// ----------------------------------------------------------------------------
impl ValidatableElement for QuestRootSegment {
    // ------------------------------------------------------------------------
    fn validate(&self, _id: &str) -> Result<()> {
        //TODO
        // one start segment
        // one end block
        // in/out of subsegments?
        // validation of in segment next links?
        // validation of orphans?
        // TODO implement validate QuestRootSegment
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for QuestSegment {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        //TODO
        // in segment
        // out segments
        // validation of in segment next links?
        // validation of orphans?
        // TODO implement validate QuestSegment
        if let Some(ref context) = self.context {
            validate_str_default(context, id, "^[0-9_a-z\\\\/]*$", "[a-z_0-9]")?
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for QuestStart {
    // ------------------------------------------------------------------------
    fn validate(&self, _id: &str) -> Result<()> {
        // at least one link?
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for QuestEnd {
    // ------------------------------------------------------------------------
    fn validate(&self, _: &str) -> Result<()> {
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for SubSegment {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if self.segmentlink.is_none() {
            validate_err_missing!(id, "segment")
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for SegmentIn {
    // ------------------------------------------------------------------------
    fn validate(&self, _id: &str) -> Result<()> {
        // at least one link?
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for SegmentOut {
    // ------------------------------------------------------------------------
    fn validate(&self, _: &str) -> Result<()> {
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------

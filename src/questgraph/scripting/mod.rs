//
// questgraph::scripting
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Default, Model, QuestBlock)]
pub struct Script {
    #[model(getr)]
    id: BlockUid, // -> mapped to name
    #[model(getr)]
    guid: GUID,
    out: Links, // either default single "Out" outsocket OR two "True" + "False"
    #[model(getr)]
    scriptcall: Option<ScriptCall>, // outsockets for script calls with bool result
    #[model(set)]
    editordata: Option<EditorBlockData>,
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use {Result, ValidatableElement};
use shared::ScriptCall;
use primitives::GUID;

use super::{ManagedQuestBlock, QuestBlock, SegmentBlock};
use super::primitives::{BlockId, BlockName, BlockUid, EditorBlockData, InSocketId, Link,
                        LinkTarget, Links, OutSocketId, SegmentId};
// ----------------------------------------------------------------------------
impl Script {
    // ------------------------------------------------------------------------
    pub fn set_function_call(&mut self, call: ScriptCall) -> &mut Self {
        self.scriptcall = Some(call);
        self
    }
    // ------------------------------------------------------------------------
    pub fn is_branching(&self) -> bool {
        self.out.contains_key(&"True".into()) || self.out.contains_key(&"False".into())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Validation
// ----------------------------------------------------------------------------
impl ValidatableElement for Script {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        match self.scriptcall {
            Some(ref call) => {
                call.validate(id)?;
                match self.out.len() {
                    0 => Ok(()),    // deadend .done -> no links
                    1 if self.out.contains_key(&"Out".into())
                        || self.out.contains_key(&"True".into())
                        || self.out.contains_key(&"False".into()) => Ok(()),
                    2 if self.out.contains_key(&"True".into()) && self.out.contains_key(&"False".into()) => Ok(()),

                    _ => Err(format!("{}: expected outsocket ids to be either 'Out', 'True' or 'False'. found: [{}]",
                            id,
                            self.out.keys()
                                .map(|id| id.0.clone())
                                .collect::<Vec<_>>()
                                .join(",")
                        )),
                }
            },
            None => Err(format!("{}: scriptcall is missing", id)),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------

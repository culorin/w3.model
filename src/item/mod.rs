//
// model::item
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Default, ModelRepo)]
pub struct QuestItems {
    #[modelrepo(getr, add)]
    items: IndexMap<String, ItemDefinition>,
    #[modelrepo(getr, add)]
    lootdefs: IndexMap<String, LootDefinition>,
    #[modelrepo(getr, add)]
    rewards: IndexMap<String, RewardDefinition>,
}
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub enum ItemCategory {
    Custom(String),
    Letter,
    Book,
    Notice,
    NoticeboardNote,
}
// ----------------------------------------------------------------------------
#[derive(Model)]
pub struct ItemDefinition {
    #[model(getr)]
    id: String, // "internal" id-name (used as part of uid)
    #[model(getr, set)]
    category: ItemCategory, // "internal" category (used as part of uid)
    #[model(getr, set)]
    caption: String, // localized name string
    #[model(getr, set)]
    description: String, // localized desction
    #[model(getr, set)]
    text: Option<String>, // localized text content
}
// ----------------------------------------------------------------------------
#[derive(Model)]
pub struct LootDefinition {
    #[model(getr)]
    id: String,
    #[model(iter, add)]
    items: Vec<ItemReference>,
}
// ----------------------------------------------------------------------------
#[derive(Model)]
pub struct RewardDefinition {
    #[model(getr)]
    id: String,
    #[model(iter, add)]
    items: Vec<ItemReference>,
}
// ----------------------------------------------------------------------------
use std::convert::From;

use indexmap::map::{Drain, IndexMap};

use primitives::references::ItemReference;

use {Result, ValidatableElement};
// ----------------------------------------------------------------------------
impl QuestItems {
    // ------------------------------------------------------------------------
    pub fn items_count(&self) -> usize {
        self.items.len()
    }
    // ------------------------------------------------------------------------
    pub fn lootdefs_count(&self) -> usize {
        self.lootdefs.len()
    }
    // ------------------------------------------------------------------------
    pub fn rewards_count(&self) -> usize {
        self.rewards.len()
    }
    // ------------------------------------------------------------------------
    pub fn drain_items(&mut self) -> Drain<String, ItemDefinition> {
        self.items.drain(..)
    }
    // ------------------------------------------------------------------------
    pub fn drain_rewards(&mut self) -> Drain<String, RewardDefinition> {
        self.rewards.drain(..)
    }
    // ------------------------------------------------------------------------
    pub fn drain_lootdefs(&mut self) -> Drain<String, LootDefinition> {
        self.lootdefs.drain(..)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Default for ItemCategory {
    // ------------------------------------------------------------------------
    fn default() -> ItemCategory {
        ItemCategory::Custom(String::from("misc"))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ItemDefinition {
    // ------------------------------------------------------------------------
    pub fn new(id: &str) -> ItemDefinition {
        ItemDefinition {
            id: id.to_lowercase(),
            category: ItemCategory::default(),
            caption: String::default(),
            description: String::default(),
            text: None,
        }
    }
    // ------------------------------------------------------------------------
    pub fn uid(&self) -> String {
        format!("{}:{}", &self.category, &self.id).to_lowercase()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl LootDefinition {
    // ------------------------------------------------------------------------
    pub fn new(id: &str) -> LootDefinition {
        LootDefinition {
            id: id.to_lowercase(),
            items: Vec::default(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl RewardDefinition {
    // ------------------------------------------------------------------------
    pub fn new(id: &str) -> RewardDefinition {
        RewardDefinition {
            id: id.to_lowercase(),
            items: Vec::default(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ItemCategory {
    // ------------------------------------------------------------------------
    pub fn as_str(&self) -> &str {
        use self::ItemCategory::*;

        match self {
            Custom(cat) => cat.as_str(),
            Letter => "letters",
            Book => "books",
            Notice => "notices",
            NoticeboardNote => "boardnotes",
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
use std::fmt;
// ----------------------------------------------------------------------------
impl fmt::Display for ItemCategory {
    // ------------------------------------------------------------------------
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.as_str())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Validation
// ----------------------------------------------------------------------------
impl ValidatableElement for ItemDefinition {
    // ------------------------------------------------------------------------
    fn validate(&self, _id: &str) -> Result<()> {
        //TODO implement validate item definition
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for LootDefinition {
    // ------------------------------------------------------------------------
    fn validate(&self, _id: &str) -> Result<()> {
        // TODO implement validate loot definition
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for RewardDefinition {
    // ------------------------------------------------------------------------
    fn validate(&self, _id: &str) -> Result<()> {
        // TODO implement validate reward definition
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> From<&'a str> for ItemCategory {
    // ------------------------------------------------------------------------
    fn from(src: &str) -> ItemCategory {
        match src.to_lowercase().as_str() {
            "letters" => ItemCategory::Letter,
            "books" => ItemCategory::Book,
            "notices" => ItemCategory::Notice,
            "boardnotes" => ItemCategory::NoticeboardNote,
            custom => ItemCategory::Custom(custom.to_owned()),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------

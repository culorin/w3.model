//
// w3 questgraph
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub fn quest_in_out_block(ast: &DeriveInput) -> quote::Tokens {
    let name = &ast.ident;
    let params = extract_attributes(&ast.attrs);

    let mut fragments = Vec::new();

    match ast.body {
        Body::Struct(_) => {
            fragments.push(generate_converter(name));
            fragments.push(generate_quest_block(name));
            if !params.no_new {
                fragments.push(generate_new(name));
            }
        }
        _ => panic!("#[derive(QuestBlock)] can only be used with structs"),
    }

    quote! {
        #(#fragments)*
    }
}
// ----------------------------------------------------------------------------
pub fn quest_in_block(ast: &DeriveInput) -> quote::Tokens {
    let name = &ast.ident;
    let mut fragments = Vec::new();

    match ast.body {
        Body::Struct(_) => {
            // same as quest_in_block (<- it has out links, one out socket only)
            fragments.push(generate_quest_in_block(name));
        }
        _ => panic!("#[derive(QuestBlock)] can only be used with structs"),
    }

    quote! {
        #(#fragments)*
    }
}
// ----------------------------------------------------------------------------
pub fn quest_out_block(ast: &DeriveInput) -> quote::Tokens {
    let name = &ast.ident;
    let mut fragments = Vec::new();

    match ast.body {
        Body::Struct(_) => {
            // no out links
            fragments.push(generate_quest_out_block(name));
        }
        _ => panic!("#[derive(QuestBlock)] can only be used with structs"),
    };

    quote! {
        #(#fragments)*
    }
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use quote;
use syn::*;

use helpers::*;
// ----------------------------------------------------------------------------
#[derive(Default, Debug)]
struct Params {
    no_new: bool,
}
// ----------------------------------------------------------------------------
fn extract_attributes(attr: &[Attribute]) -> Params {
    let mut params = Params::default();

    for flag in extract_attribute_flags("block", attr, "questblock-derive") {
        match flag {
            "no_new" => params.no_new = true,
            _ => panic!(
                "questblock-derive: unsupported QuestBlock attribute setting: {:?}",
                flag
            ),
        }
    }
    params
}
// ----------------------------------------------------------------------------
// Questblock
// ----------------------------------------------------------------------------
fn generate_converter(ident: &Ident) -> quote::Tokens {
    quote! {
        impl From<#ident> for SegmentBlock {
            fn from(o: #ident) -> SegmentBlock {
                SegmentBlock::#ident(o)
            }
        }
    }
}
// ----------------------------------------------------------------------------
fn generate_new(ident: &Ident) -> quote::Tokens {
    quote! {
        impl #ident {
            // ----------------------------------------------------------------
            pub fn new(name: &str) -> #ident {
                #ident {
                    id: BlockUid::new(BlockId::#ident(name.into())),
                    ..Default::default()
                }
            }
            // ----------------------------------------------------------------
        }
    }
}
// ----------------------------------------------------------------------------
fn generate_quest_block(ident: &Ident) -> quote::Tokens {
    quote! {
        // --------------------------------------------------------------------
        impl QuestBlock for #ident {
            // ----------------------------------------------------------------
            #[inline]
            fn uid(&self) -> &BlockUid {
                &self.id
            }
            // ----------------------------------------------------------------
            #[inline]
            fn segmentid(&self) -> &SegmentId {
                &self.id.segmentid()
            }
            // ----------------------------------------------------------------
            fn links<'a>(&'a self) -> Box<Iterator<Item = (&'a OutSocketId, &'a Vec<Link>)> + 'a> {
                Box::new(self.out.iter())
            }
            // ----------------------------------------------------------------
            fn reset_links(&mut self) {
                self.out.clear()
            }
            // ----------------------------------------------------------------
            fn add_link_to(&mut self,
                out_socket: Option<OutSocketId>, block: LinkTarget, in_socket: Option<InSocketId>)
            {
                if self.id.blockid() != &block {
                    self.out.entry(out_socket.unwrap_or("Out".into()))
                        .or_insert(Vec::new())
                        .push(Link::new(None, block, in_socket));
                }
            }
            // ----------------------------------------------------------------
            fn delete_links_for_socket(&mut self, socket: &OutSocketId) {
                self.out.remove(socket);
            }
            // ----------------------------------------------------------------
            fn set_segmentid(&mut self, id: &SegmentId) {
                self.id.set_segmentid(id);
                self.out.set_segmentid(id);
            }
            // ----------------------------------------------------------------
            fn editordata(&self) -> Option<&EditorBlockData> {
                self.editordata.as_ref()
            }
            // ----------------------------------------------------------------
            fn editordata_or_default(&mut self) -> &mut EditorBlockData {
                self.editordata.get_or_insert(EditorBlockData::default())
            }
            // ----------------------------------------------------------------
        }
        // --------------------------------------------------------------------
        impl ManagedQuestBlock for #ident {
            // ----------------------------------------------------------------
            fn rename(&mut self, name: BlockName) {
                self.id.set_blockname(name);
            }
            // ----------------------------------------------------------------
            fn update_linktarget_blockname(&mut self, old: &LinkTarget, new: &BlockName) {
                self.out.values_mut().for_each(|linkset| {
                    linkset
                        .iter_mut()
                        .filter(|l| l.target_block() == old)
                        .for_each(|link| link.set_target_blockname(new.clone()))
                });
            }
            // ----------------------------------------------------------------
            fn prune_links(&mut self, target: &LinkTarget, valid_sockets: &[InSocketId]) {
                let mut filtered_links = ::std::collections::BTreeMap::new();

                for (outsocket, links) in self.out.iter() {
                    let links = links
                        .iter()
                        .filter(|l| l.target_block() != target || valid_sockets.contains(l.target_socket()))
                        .cloned()
                        .collect::<Vec<_>>();
                    if !links.is_empty() {
                        filtered_links.insert(outsocket.to_owned(), links);
                    }
                }
                *self.out = filtered_links;
            }
            // ----------------------------------------------------------------
            fn change_link_target(
                &mut self,
                old_target: &LinkTarget,
                old_socket: &InSocketId,
                new_target: &LinkTarget,
                new_socket: &InSocketId,
            ) {
                let segmentid = self.id.segmentid();
                for linkset in self.out.values_mut() {
                    let new_links = linkset.drain(..).map(|link| {
                        if link.target_block() == old_target && link.target_socket() == old_socket {
                            Link::new(
                                Some(segmentid.clone()),
                                new_target.clone(),
                                Some(new_socket.clone())
                            )
                        } else {
                            link
                        }
                    }).collect();
                    *linkset = new_links;
                }
            }
            // ----------------------------------------------------------------
        }
        // --------------------------------------------------------------------
    }
}
// ----------------------------------------------------------------------------
// Quest in-block
// ----------------------------------------------------------------------------
fn generate_quest_in_block(ident: &Ident) -> quote::Tokens {
    quote! {
        // --------------------------------------------------------------------
        impl QuestBlock for #ident {
            // ----------------------------------------------------------------
            #[inline]
            fn uid(&self) -> &BlockUid {
                &self.id
            }
            // ----------------------------------------------------------------
            #[inline]
            fn segmentid(&self) -> &SegmentId {
                &self.id.segmentid()
            }
            // ----------------------------------------------------------------
            fn links<'a>(&'a self) -> Box<Iterator<Item = (&'a OutSocketId, &'a Vec<Link>)> + 'a> {
                Box::new(self.out.iter())
            }
            // ----------------------------------------------------------------
            fn reset_links(&mut self) {
                self.out.clear()
            }
            // ----------------------------------------------------------------
            fn add_link_to(&mut self,
                _out_socket: Option<OutSocketId>, block: LinkTarget, in_socket: Option<InSocketId>)
            {
                if self.id.blockid() != &block {
                    // out socket is empty constant
                    self.out.entry(" ".into())
                        .or_insert(Vec::new())
                        .push(Link::new(None, block, in_socket));
                }
            }
            // ----------------------------------------------------------------
            fn delete_links_for_socket(&mut self, socket: &OutSocketId) {
                warn!("tried to remove socket [{}] on in block: {}", socket, self.id);
            }
            // ----------------------------------------------------------------
            fn set_segmentid(&mut self, id: &SegmentId) {
                self.id.set_segmentid(id);
                self.out.set_segmentid(id);
            }
            // ----------------------------------------------------------------
            fn editordata(&self) -> Option<&EditorBlockData> {
                self.editordata.as_ref()
            }
            // ----------------------------------------------------------------
            fn editordata_or_default(&mut self) -> &mut EditorBlockData {
                self.editordata.get_or_insert(EditorBlockData::default())
            }
            // ----------------------------------------------------------------
        }
        // --------------------------------------------------------------------
        impl ManagedQuestBlock for #ident {
            // ----------------------------------------------------------------
            fn rename(&mut self, name: BlockName) {
                self.id.set_blockname(name);
            }
            // ------------------------------------------------------------------------
            fn update_linktarget_blockname(&mut self, old: &LinkTarget, new: &BlockName) {
                self.out.values_mut().for_each(|linkset| {
                    linkset
                        .iter_mut()
                        .filter(|l| l.target_block() == old)
                        .for_each(|link| link.set_target_blockname(new.clone()))
                });
            }
            // ----------------------------------------------------------------
            fn prune_links(&mut self, target: &LinkTarget, valid_sockets: &[InSocketId]) {
                let mut filtered_links = ::std::collections::BTreeMap::new();

                for (outsocket, links) in self.out.iter() {
                    let links = links
                        .iter()
                        .filter(|l| l.target_block() != target || valid_sockets.contains(l.target_socket()))
                        .cloned()
                        .collect::<Vec<_>>();
                    if !links.is_empty() {
                        filtered_links.insert(outsocket.to_owned(), links);
                    }
                }
                *self.out = filtered_links;
            }
            // ----------------------------------------------------------------
            fn change_link_target(
                &mut self,
                old_target: &LinkTarget,
                old_socket: &InSocketId,
                new_target: &LinkTarget,
                new_socket: &InSocketId,
            ) {
                let segmentid = self.id.segmentid();
                for linkset in self.out.values_mut() {
                    let new_links = linkset.drain(..).map(|link| {
                        if link.target_block() == old_target && link.target_socket() == old_socket {
                            Link::new(
                                Some(segmentid.clone()),
                                new_target.clone(),
                                Some(new_socket.clone())
                            )
                        } else {
                            link
                        }
                    }).collect();
                    *linkset = new_links;
                }
            }
            // ----------------------------------------------------------------
        }
        // --------------------------------------------------------------------
    }
}
// ----------------------------------------------------------------------------
// Quest out-block
// ----------------------------------------------------------------------------
fn generate_quest_out_block(ident: &Ident) -> quote::Tokens {
    quote! {
        // --------------------------------------------------------------------
        impl QuestBlock for #ident {
            // ----------------------------------------------------------------
            fn uid(&self) -> &BlockUid {
                &self.id
            }
            // ----------------------------------------------------------------
            #[inline]
            fn segmentid(&self) -> &SegmentId {
                &self.id.segmentid()
            }
            // ----------------------------------------------------------------
            fn links<'a>(&'a self) -> Box<Iterator<Item = (&'a OutSocketId, &'a Vec<Link>)> + 'a> {
                Box::new(::std::iter::empty())
            }
            // ----------------------------------------------------------------
            fn reset_links(&mut self) {
                warn!("tried to clear outgoing links on out block: {}", self.id);
            }
            // ----------------------------------------------------------------
            fn add_link_to(&mut self,
                _out_socket: Option<OutSocketId>, _block: LinkTarget, _in_socket: Option<InSocketId>)
            {
                warn!("tried to add outgoing link on out block: {}", self.id);
            }
            // ----------------------------------------------------------------
            fn delete_links_for_socket(&mut self, socket: &OutSocketId) {
                warn!("tried to remove socket [{}] on out block: {}", socket, self.id);
            }
            // ----------------------------------------------------------------
            fn set_segmentid(&mut self, id: &SegmentId) {
                self.id.set_segmentid(id);
            }
            // ----------------------------------------------------------------
            fn editordata(&self) -> Option<&EditorBlockData> {
                self.editordata.as_ref()
            }
            // ----------------------------------------------------------------
            fn editordata_or_default(&mut self) -> &mut EditorBlockData {
                self.editordata.get_or_insert(EditorBlockData::default())
            }
            // ----------------------------------------------------------------
        }
        // --------------------------------------------------------------------
        impl #ident {
            // ----------------------------------------------------------------
            pub fn as_quest_block(&self) -> &QuestBlock {
                self
            }
            // ----------------------------------------------------------------
        }
        // --------------------------------------------------------------------
        impl ManagedQuestBlock for #ident {
            // ----------------------------------------------------------------
            fn rename(&mut self, name: BlockName) {
                self.id.set_blockname(name);
            }
            // ----------------------------------------------------------------
            fn update_linktarget_blockname(&mut self, _old: &LinkTarget, _new: &BlockName) {
                // no links
            }
            // ----------------------------------------------------------------
            fn prune_links(&mut self, _target: &LinkTarget, _valid_sockets: &[InSocketId]) {
                // no links
            }
            // ----------------------------------------------------------------
            fn change_link_target(
                &mut self,
                _old_target: &LinkTarget,
                _old_socket: &InSocketId,
                _new_target: &LinkTarget,
                _new_socket: &InSocketId,
            ) {
                // no links
            }
            // ----------------------------------------------------------------
        }
        // --------------------------------------------------------------------
    }
}
// ----------------------------------------------------------------------------

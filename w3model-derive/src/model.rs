//
// w3model
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub fn model(ast: &DeriveInput) -> quote::Tokens {
    let name = &ast.ident;
    let struct_params = extract_attributes(&ast.attrs).unwrap_or_default();
    let lifetimes = extract_lifetimes(&ast.generics.lifetimes, "model-derive");

    let fragments = match ast.body {
        Body::Struct(ref data) => match *data {
            VariantData::Struct(ref fields) => fields
                .iter()
                .map(|f| generate_field_fragments(&struct_params, f))
                .collect::<Vec<_>>(),
            _ => panic!("model-derive: tuples and unit are not covered"),
        },
        _ => panic!("#[derive(Model)] can only be used with structs"),
    };
    // println!("FRAGMENTS {:?}", fragments);

    if let Some(lifetime) = lifetimes {
        quote! {
            impl<#lifetime> #name<#lifetime> {
                #(#fragments)*
            }
        }
    } else {
        quote! {
            impl #name {
                #(#fragments)*
            }
        }
    }
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use quote;
use syn::*;

use helpers::*;
// ----------------------------------------------------------------------------
#[derive(Default, Debug)]
struct Params {
    get_ref: bool,
    get_mut_ref: bool,
    get_mut_default: bool,
    get_iter: bool,
    set: bool,
    add: bool,
}
// ----------------------------------------------------------------------------
fn extract_attributes(attr: &[Attribute]) -> Option<Params> {
    let flags = extract_attribute_flags("model", attr, "model-derive");

    if !flags.is_empty() {
        let mut params = Params::default();
        for flag in flags {
            match flag {
                "getr" => params.get_ref = true,
                "getmr" => params.get_mut_ref = true,
                "getmrd" => params.get_mut_default = true,
                "iter" => params.get_iter = true,
                "set" => params.set = true,
                "add" => params.add = true,
                _ => panic!(
                    "model-derive: unsupported model attribute setting: {:?}",
                    flag
                ),
            }
        }
        Some(params)
    } else {
        None
    }
}
// ----------------------------------------------------------------------------
fn generate_field_fragments(struct_params: &Params, field: &Field) -> quote::Tokens {
    let field_params = extract_attributes(&field.attrs);
    let params = field_params.as_ref().unwrap_or(struct_params);

    let ident = &field
        .ident
        .as_ref()
        .expect("model-derive: expected field ident");
    let field_type = extract_fieldtype(&field.ty);

    let (getr, getmr, getmrd, set) = match field_type {
        FieldType::Option(ref ty) => (
            do_it!(params.get_ref, generate_opt_getr(ident, ty)),
            do_it!(params.get_mut_ref, generate_opt_getmr(ident, ty)),
            do_it!(params.get_mut_default, generate_opt_getmrd(ident, ty)),
            do_it!(params.set, generate_opt_set(ident, ty)),
        ),
        FieldType::Normal(ref ty) | FieldType::Vec(ref ty, _) | FieldType::Map(ref ty, _, _, _) => {
            (
                do_it!(params.get_ref, generate_getr(ident, ty)),
                do_it!(params.get_mut_ref, generate_getmr(ident, ty)),
                if params.get_mut_default {
                    panic!(
                        "model-derive: \"getmrd\" option only supported for Option fields {:?}",
                        field_type
                    )
                } else {
                    None
                },
                do_it!(params.set, generate_set(ident, ty)),
            )
        }
    };

    let (add, iter) = match field_type {
        FieldType::Vec(_, ref ty) => (
            do_it!(params.add, generate_add(ident, ty)),
            do_it!(params.get_iter, generate_iter(ident, ty)),
        ),
        _ if params.add => panic!(
            "model-derive: \"add\" option only supported for Vec fields {:?}",
            field_type
        ),
        _ if params.get_iter => panic!(
            "model-derive: \"iter\" option only supported for Vec fields {:?}",
            field_type
        ),
        _ => (None, None),
    };

    quote! {
        #getr
        #getmr
        #getmrd
        #set
        #add
        #iter
    }
}
// ----------------------------------------------------------------------------
// fragment generators
// ----------------------------------------------------------------------------
fn generate_getr(ident: &Ident, ty: &quote::Tokens) -> quote::Tokens {
    quote! {
        #[inline]
        pub fn #ident(&self) -> &#ty {
            &self.#ident
        }
    }
}
// ----------------------------------------------------------------------------
fn generate_getmr(ident: &Ident, ty: &quote::Tokens) -> quote::Tokens {
    let fn_ident = Ident::from(format!("{}_mut", ident.as_ref()));
    quote! {
        #[inline]
        pub fn #fn_ident(&mut self) ->&mut #ty {
            &mut self.#ident
        }
    }
}
// ----------------------------------------------------------------------------
fn generate_set(ident: &Ident, ty: &quote::Tokens) -> quote::Tokens {
    let fn_ident = Ident::from(format!("set_{}", ident.as_ref()));
    quote! {
        #[inline]
        pub fn #fn_ident<T: Into<#ty>>(&mut self, value: T) {
            self.#ident = value.into()
        }
    }
}
// ----------------------------------------------------------------------------
fn generate_add(ident: &Ident, ty: &quote::Tokens) -> quote::Tokens {
    let fn_ident = Ident::from(format!("add_{}", ident_as_singular(ident)));

    quote! {
        pub fn #fn_ident<T: Into<#ty>>(&mut self, value: T) {
            self.#ident.push(value.into())
        }
    }
}
// ----------------------------------------------------------------------------
fn generate_iter(ident: &Ident, value: &quote::Tokens) -> quote::Tokens {
    quote! {
        #[inline]
        pub fn #ident(&self) -> impl Iterator<Item = &#value> {
            self.#ident.iter()
        }
    }
}
// ----------------------------------------------------------------------------
// special version for options
// ----------------------------------------------------------------------------
fn generate_opt_getr(ident: &Ident, ty: &quote::Tokens) -> quote::Tokens {
    quote! {
        #[inline]
        pub fn #ident(&self) -> Option<&#ty> {
            self.#ident.as_ref()
        }
    }
}
// ----------------------------------------------------------------------------
fn generate_opt_getmr(ident: &Ident, ty: &quote::Tokens) -> quote::Tokens {
    let fn_ident = Ident::from(format!("{}_mut", ident.as_ref()));
    quote! {
        #[inline]
        pub fn #fn_ident(&mut self) -> Option<&mut #ty> {
            self.#ident.as_mut()
        }
    }
}
// ----------------------------------------------------------------------------
fn generate_opt_getmrd(ident: &Ident, ty: &quote::Tokens) -> quote::Tokens {
    let fn_ident = Ident::from(format!("{}_or_default", ident.as_ref()));
    quote! {
        #[inline]
        pub fn #fn_ident(&mut self) -> &mut #ty {
            self.#ident.get_or_insert(#ty::default())
        }
    }
}
// ----------------------------------------------------------------------------
fn generate_opt_set(ident: &Ident, ty: &quote::Tokens) -> quote::Tokens {
    let fn_ident = Ident::from(format!("set_{}", ident.as_ref()));
    let fn_ident2 = Ident::from(format!("set_opt_{}", ident.as_ref()));
    quote! {
        #[inline]
        pub fn #fn_ident<T: Into<#ty>>(&mut self, value: T) {
            self.#ident = Some(value.into())
        }
        #[inline]
        pub fn #fn_ident2(&mut self, value: Option<#ty>) {
            self.#ident = value
        }
    }
}
// ----------------------------------------------------------------------------

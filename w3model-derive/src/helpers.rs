//
// helpers
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
macro_rules! do_it {
    ($yes:expr, $do:expr) => {{
        if $yes {
            Some($do)
        } else {
            None
        }
    }};
}
// ----------------------------------------------------------------------------
#[derive(Debug)]
pub enum FieldType {
    Option(quote::Tokens),
    Vec(quote::Tokens, quote::Tokens),
    Normal(quote::Tokens),
    Map(quote::Tokens, quote::Tokens, quote::Tokens, quote::Tokens),
}
// ----------------------------------------------------------------------------
pub fn extract_fieldtype(ty: &Ty) -> FieldType {
    match *ty {
        Ty::Path(_, ref path) if !path.segments.is_empty() => {
            let segment = &path.segments[0];
            match segment.ident.as_ref() {
                "Option" => match segment.parameters {
                    PathParameters::AngleBracketed(ref params) => {
                        let params = &params.types[0];
                        FieldType::Option(quote! { #params })
                    }
                    ref params => panic!(
                        "w3model-derive: unexpected Option parameters: {:?}",
                        &params
                    ),
                },
                "Vec" => match segment.parameters {
                    PathParameters::AngleBracketed(ref params) => {
                        let params = &params.types[0];
                        FieldType::Vec(quote! { #ty }, quote! { #params })
                    }
                    ref params => {
                        panic!("w3model-derive: unexpected Vec parameters: {:?}", &params)
                    }
                },
                "BTreeMap" | "HashMap" | "IndexMap" => match segment.parameters {
                    PathParameters::AngleBracketed(ref params) => {
                        let ident = &segment.ident;
                        let key = &params.types[0];
                        let value = &params.types[1];
                        FieldType::Map(
                            quote! { #ty },
                            quote! { #ident },
                            quote! { #key },
                            quote! { #value },
                        )
                    }
                    ref params => {
                        panic!("w3model-derive: unexpected Map parameters: {:?}", &params)
                    }
                },
                _ => FieldType::Normal(quote! { #ty }),
            }
        }
        _ => FieldType::Normal(quote! { #ty }),
    }
}
// ----------------------------------------------------------------------------
pub fn extract_attribute_flags<'attr>(
    name: &str,
    attrs: &'attr [Attribute],
    derive: &str,
) -> Vec<&'attr str> {
    let mut flags = Vec::new();

    for attr in attrs.iter().filter(|a| a.name() == name) {
        match attr.value {
            MetaItem::List(_, ref list) => for flag in list {
                match *flag {
                    NestedMetaItem::MetaItem(MetaItem::Word(ref flag)) => {
                        flags.push(flag.as_ref());
                    }
                    _ => panic!(
                        "{}: unsupported {} attribute setting: {:?}",
                        derive, name, flag
                    ),
                }
            },
            _ => panic!(
                "{}: unsupported {} attribute setting: {:?}",
                derive, name, attr.value
            ),
        }
    }
    flags
}
// ----------------------------------------------------------------------------
pub fn extract_lifetimes<'lt>(lifetimes: &'lt[LifetimeDef], derive: &str) -> Option<&'lt Ident> {
    match lifetimes.len() {
        0 => None,
        1 => {
            Some(&lifetimes[0].lifetime.ident)
        },
        c => panic!("{}: only one lifetime supported. found: {}", derive, c)
    }
}
// ----------------------------------------------------------------------------
pub fn ident_as_singular(ident: &Ident) -> String {
    match ident.as_ref() {
        name if name.ends_with("ies") => format!("{}y", name.split_at(name.len() - 3).0),
        name if name.ends_with("hes") => String::from(name.split_at(name.len() - 2).0),
        name if name.ends_with('s') => String::from(name.split_at(name.len() - 1).0),
        _ => String::from(ident.as_ref()),
    }
}
// ----------------------------------------------------------------------------
pub fn map_string_to_str(v_type: &quote::Tokens) -> Ident {
    match v_type.as_str() {
        type_str if type_str == "String" => Ident::from("str"),
        type_str => Ident::from(type_str),
    }
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use quote;
use syn::*;
// ----------------------------------------------------------------------------

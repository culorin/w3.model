//
// modelrepo
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub fn model_repo(ast: &DeriveInput) -> quote::Tokens {
    let name = &ast.ident;
    let struct_params = extract_attributes(&ast.attrs).unwrap_or_default();
    let lifetimes = extract_lifetimes(&ast.generics.lifetimes, "modelrepo-derive");

    let fragments = match ast.body {
        Body::Struct(ref data) => match *data {
            VariantData::Struct(ref fields) => fields
                .iter()
                .map(|f| generate_field_fragments(&struct_params, f))
                .collect::<Vec<_>>(),
            _ => panic!("modelrepo-derive: tuples and unit are not covered"),
        },
        _ => panic!("#[derive(ModelRepo)] can only be used with structs"),
    };

    if let Some(lifetime) = lifetimes {
        quote! {
            impl<#lifetime> #name<#lifetime> {
                #(#fragments)*
            }
        }
    } else {
        quote! {
            impl #name {
                #(#fragments)*
            }
        }
    }
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use quote;
use syn::*;

use helpers::*;
// ----------------------------------------------------------------------------
#[derive(Default, Debug)]
struct Params {
    get_ref: bool,
    get_mut_ref: bool,
    add: bool,
    del: bool,
    has: bool,
}
// ----------------------------------------------------------------------------
impl Params {
    // ------------------------------------------------------------------------
    fn any_set(&self) -> bool {
        self.get_ref || self.get_mut_ref || self.add || self.del
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
fn extract_attributes(attr: &[Attribute]) -> Option<Params> {
    let flags = extract_attribute_flags("modelrepo", attr, "modelrepo-derive");

    if !flags.is_empty() {
        let mut params = Params::default();
        for flag in flags {
            match flag {
                "getr" => params.get_ref = true,
                "getmr" => params.get_mut_ref = true,
                "add" => params.add = true,
                "del" => params.del = true,
                "has" => params.has = true,
                _ => panic!(
                    "modelrepo-derive: unsupported model attribute setting: {:?}",
                    flag
                ),
            }
        }
        Some(params)
    } else {
        None
    }
}
// ----------------------------------------------------------------------------
fn generate_field_fragments(struct_params: &Params, field: &Field) -> quote::Tokens {
    let field_params = extract_attributes(&field.attrs);
    let params = field_params.as_ref().unwrap_or(struct_params);

    let ident = &field
        .ident
        .as_ref()
        .expect("modelrepo-derive: expected field ident");
    let field_type = extract_fieldtype(&field.ty);

    let (getr, getmr, add, del, has) = match field_type {
        FieldType::Map(_, ref map_type, ref key, ref value) => (
            do_it!(params.get_ref, generate_getr(ident, key, value)),
            do_it!(params.get_mut_ref, generate_getmr(ident, key, value)),
            do_it!(params.add, generate_add(ident, map_type, key, value)),
            do_it!(params.del, generate_del(ident, key)),
            do_it!(params.has, generate_has_elements(ident)),
        ),
        _ if params.any_set() => panic!(
            "modelrepo-derive: only map type fields are supported. invalid field: {}",
            ident
        ),
        _ => (None, None, None, None, None),
    };

    quote! {
        #getr
        #getmr
        #add
        #del
        #has
    }
}
// ----------------------------------------------------------------------------
// fragment generators
// ----------------------------------------------------------------------------
fn generate_getr(ident: &Ident, key: &quote::Tokens, value: &quote::Tokens) -> quote::Tokens {
    let key = map_string_to_str(key);
    let fn_single_ident = Ident::from(ident_as_singular(ident));

    quote! {
        #[allow(dead_code)]
        #[inline]
        pub fn #fn_single_ident(&self, id: &#key) -> Option<&#value> {
            self.#ident.get(id)
        }
        #[allow(dead_code)]
        #[inline]
        pub fn #ident(&self) -> impl Iterator<Item = &#value> {
            self.#ident.values()
        }
    }
}
// ----------------------------------------------------------------------------
fn generate_getmr(ident: &Ident, key: &quote::Tokens, value: &quote::Tokens) -> quote::Tokens {
    let key = map_string_to_str(key);
    let fn_ident = Ident::from(format!("{}_mut", ident.as_ref()));
    let fn_single_ident = Ident::from(format!("{}_mut", ident_as_singular(ident)));

    quote! {
        #[allow(dead_code)]
        #[inline]
        pub fn #fn_single_ident(&mut self, id: &#key) -> Option<&mut #value> {
            self.#ident.get_mut(id)
        }
        #[allow(dead_code)]
        #[inline]
        pub fn #fn_ident(&mut self) -> impl Iterator<Item = &mut #value> {
            self.#ident.values_mut()
        }
    }
}
// ----------------------------------------------------------------------------
fn generate_add(
    ident: &Ident,
    map_type: &quote::Tokens,
    key: &quote::Tokens,
    value: &quote::Tokens,
) -> quote::Tokens {
    let ident_name = ident_as_singular(ident);
    let fn_ident = Ident::from(format!("add_{}", ident_name));

    let map_type = match map_type.as_str() {
        "IndexMap" => Ident::from("indexmap::map"),
        "HashMap" => Ident::from("std::collections::hash_map"),
        "BTreeMap" => Ident::from("std::collections::btree_map"),
        _ => panic!("modelrepo-derive: found unknown map type {}", map_type),
    };

    quote! {
        pub fn #fn_ident<K: Into<#key>, V: Into<#value>>(&mut self, id: K, value: V) -> Result<()> {
            use #map_type::Entry;

            match self.#ident.entry(id.into()) {
                Entry::Occupied(entry) => {
                    Err(format!(
                        "{} name must be unique. found an existing definition for: {}",
                        #ident_name, entry.key()
                    ))
                }
                Entry::Vacant(entry) => {
                    entry.insert(value.into());
                    Ok(())
                }
            }
        }
    }
}
// ----------------------------------------------------------------------------
fn generate_del(ident: &Ident, key: &quote::Tokens) -> quote::Tokens {
    let key = map_string_to_str(key);
    let ident_name = ident_as_singular(ident);
    let fn_ident = Ident::from(format!("delete_{}", ident_name));

    quote! {
        pub fn #fn_ident(&mut self, id: &#key) -> Result<()> {
            self.#ident
                .remove(id)
                .map(|_| ())
                .ok_or_else(|| format!("{} {} not found", #ident_name, id))
        }
    }
}
// ----------------------------------------------------------------------------
fn generate_has_elements(ident: &Ident) -> quote::Tokens {
    let fn_ident = Ident::from(format!("has_{}", ident));

    quote! {
        pub fn #fn_ident(&self) -> bool {
            !self.#ident.is_empty()
        }
    }
}
// ----------------------------------------------------------------------------
